module.exports = {
	title: "Sonic Garden",
	url: "https://sonicgarden.kazhnuz.space/",
	language: "fr",
	description: "Un microsite sur Sonic le Hérisson",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
