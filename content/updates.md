---
layout: layouts/base.njk
eleventyNavigation:
  key: Mises à jour
  parent: Accueil
  order: 1
---

# Mises à jour

Le but de cette page est de présenter les différentes mises à jour qu'il y a eut sur Sonic Garden, ainsi que les projets futurs au fur et à mesure qu'elles arrivent.

Comme ça, cela permet de savoir les dernières évolutions, ainsi que celles plus anciennes. Il y aura aussi ici mes futurs projets pour le site.

## Historique

- (**2023-11-15**) Nouvelles fonctionnalités
  - Ajout de news propulsées par Press Garden
  - Ajout des listes de sites web dans des pages séparés
  - Ajout d'une page "à propos" et "mise à jour"
- (**2023-09-23**) Mise à jour du site
  - Ajout d'élément lié à Superstars et aux derniers comics
  - Ajout de mods qui améliorent l'ère 128-bits.
- (**2023-05-14**) Recréation des projets en un site Eleventy
  - Ajout d'une section "Comment jouer aux jeux ?"
  - Retravail de la Chronologie et de la Géographie pour être adaptés aux derniers médias
  - Ajout de théories basées sur Frontiers
- (**2018-07-24**) Version initiale de la page de la chronologie
- (**2018-03-26**) Extraction de ma liste de fangame en une awesome-list sur github
- (**2016-08-19**) Création de la page Sonic Xtreme
- (**2014-12-28**) Retravail de ma liste de fangame, intégration à project-mobius
- (**2014-11-13**) Version initiale de la geographie
- (**2014-11-10**) Version initiale du site, un dépot de page nommée "Project Mobius", ou je mettait en commun des trucs que j'avais fait dans des commus Sonic
- (**2013-05-31**) Premier essaie de créer une liste de fangame sur les forums de Planète-Sonic

## Projets futurs

- Ajoute une section dédiée à SRB2
- Ajoute une liste de jeux "Sonic-like" intéressant
- Ajouter des mods pour jouer à des version "définitive" de Forces et Lost World
- Ajouter les jeux Boom à la liste des jeux
- Ajouter des sections pour les personnages et organisations