
# Infinite

I think that ironically (as it's what people dislike the most) what I love the most with Infinite is how he have a shitty motivation for doing what he does, just wanting to prove how superior he is to everybody. In a way, he is basically an edgy asshole that support a dictatorial invasion just to feel superior to the people he see as "insects". In a way, it's kinda realistic. ( The only missing things would be him saying that it's because of weak people getting too much attention that strong people like him doesn't get what is their "right" or something like that.. )

I really think that Forces should have used that more. I feel that instead of Sonic making a speech on friendship, he should have dunked on how shallow the vision of "power" of Infinite is, and going a whole "reason you suck" speach to Infinite to really crush his shitty vision of the world. ( And because it would be nice to see Sonic be a little "meaner" against a villain )

And I feel that he would have needed to feel more "dangerous", and maybe be an actual final boss, maybe with the speech after his final defeat.


##

Well, in a way, he don't even really go for Sonic either, he just fight it because he find "fun" going for the hero and because it's his mission. But his dislike of Sonic only build up with each defeats.

I think that the idea behind that is that he wasn't especially angry at Shadow personally, more that he couldn't accept the idea that he could be "weak", because as he believe that only the strong have rights, if he is weak he doesn't have any rights. "Defeating Shadow" was done (by trapping him in an illusion), so he was happy.

( I might be wrong about the writers thought process tho, it's more what I understood from the game story )

​

In a way, it's something I think is interesting and should be used more if he reappared again, that he was entirely driven by his shitty ideology of "strong = good; weak = bad", and it could be really interesting in something like IDW.


I can totally see Infinite break the fourth wall, search ways to trenscend dimensions... just because he is angry at a criticism.

##

I really don't interpret Infinite that way. For me, being called weak by Shadow was worse than even loosing his gang, because of his "ideology". Because Infinite is show through the whole game as "a bad person". I'm not talking about a bad character here (it's not the notion that interest me), but a bad person.

​

Since his first chronological apparition (the Sonic Forces comic), he his shown as wanting destruction, even before becoming Infinite (that's what the Phantom Ruby show him as a vision). He work for Doctor Eggman because he is tired of this world, that he don't care about. In the whole game, we see a bit more of his way of thinking :

- He doesn't care about weaker people, see them as insect. He makes that kind of comparison a few time, showing that he doesn't seem them as anything valuable.

- He seems to prefer destroying hope and "savior". He likes being a destroyer, and see crushing the hopes of the "weaker" (worthless for him) as something that he have the right to do.

- He doesn't show real care about Shadow and Sonic, especially since he have the Phantom Ruby. He just crush once Shadow, dive him into a Phantom Ruby Illusion and never really go back again trying to destroy him. Even Sonic, he only attack him after Sonic come to him. He don't show anything personnal about them.

- The lyrics show a bit of his ideology : he is highly cynical ("the world is a illusion") and it's an immense badass boast about his all-mighty power. I don't quite agree with your interpretation of his lyrics, for me all those sentences are him trying to crunch the hopes of other, because in the intermediary sentance, he his show belittling his ennemy. For me, it's more something like saying to their ennemy that everything they know will be destroyed. For the pain, I always interpreted that as a sign of the power of the Phantom Ruby having some tradeback.

So that's why for me, the most important point of Infinite, what define him, is that he have an ideology where only the strongest have rights, and the weaker people doesn't deserve anything. So basically for me, he doesn't have a real "motivation", but more a way of thinking that is the condition of all his actions. He isn't mad because he was "called weak" by Shadow. He is crushed because he was obliterated like if he was nothing, and called "weak" by a ultimately superior being, that didn't even sweat a bit fighting him.

In Infinite ideology, or at least the way I see him, being called weak mean being called that he is what he despised, the "weak", the one that he see as deserving nothing. The kind of way of thinking that I see Infinite having only works if he is at the top of the food chain in order to have every rights and have his cozy position where he can mock other and see himself as superior… if he isn't, welp, he is the one that will suffer.

I also see a bit of that in how he describe the world as an illusion, that "You live a lie and that's the difference in you and me" : he have this kind of thinking of "I see the reality, you are all in a lie", which is often the case in "survival of the fittest" type of ideology.

​

And for me that's why he choose to use the Phantom Ruby (for me, Shadow isn't a motivation for him to be a villain, it's a motivation to get the ultimate power that is the phantom ruby). And in a way, that create what make Infinite pretty interesting to me, how he is a mirror with other characters, as basically three other characters suffer the same situation than him, but react differently

- Sonic is beaten by Infinite, maybe as violently than Shadow beat him (and in a pretty similar way : without being able to do anything). But instead of simply searching to get more power, he tried to discover the source of Infinite power, and continued to help people no matter what, not letting his ego going in the way of what was more important. In a way, Sonic was already stronger.

- The Avatar is in the same situation of having to face a vast superior power and having to flee. But he decided to team up with other to save/repair the world. In a way, he showed having a strong will like Infinite (hence his ability to use the Phantom Ruby), but also a better motive (for the other vs for himself), which made him strong, with the help of Sonic.

That's also what is interesting, is that it's both of them that surpassed Infinite. Both of them got a part of what started Infinite, but they had not his toxic vision of the world, and got a better power. In a way, it's a bit like something we can see in Hobbes state of nature : even if someone is "more powerful", several person can overpower them.

The parallel is also done with a third character : Tails. Basically Tails start in a bad shape, not being able to beat a more powerful being (here Chaos, I know, Chaos 4 blablabla but here that's not the point), and basically started showing his own strenght, becoming the strategist of the team.

​

So TL;DR : for me, it's pretty visible that Infinite is more thought as a toxic person. Maybe that his team death is also a part of what made him, maybe that having them dead spiraled his vision of needing to be strong. But for me, the idea is that basically he was had a toxic vision of the world to begin with, and got the power to be all powerful, and used it badly and thus suffered a karmic defeat from the Resistance.

In a way, for me Eggman is the dictator in Sonic Forces, but Infinite is all those person that follow a dictator to feel powerful, by ambition. All those persons that are ready to blame everybody for them not being on top, and seeing in an all-powerful authority a way to get the "place they deserve" above other. All those persons that are ready to belittle, crush or kill the one they see as "worthless".

##

Well, in a way, Infinite already have accomplished his goal (for him) : his goal was to be the most powerful (and for him, he got that by getting the Phantom Ruby). In a way, Infinite didn't have any goal anymore (which explain why he act so borred the entire game) and is just having his own twisted, sadistic vision of "fun" (and his ideology that "weak people" are just some sort of insects for him).

He doesn't even seems to care pretty much about Sonic, with how much he.

EDIT : if we take the comic into account, he also said that he is "bored of how the world is" or something like that, and help Eggman. Combine with his vision of the world (he act like "weaker" people have no value, etc), he doesn't really have a "goal/motivation" but more some kind of twisted ideology where the people that have power can do all they want to weaker people.

##

Kazhnuz

7 points

2 years ago

I honestly kinda prefer Infinite having an awful motive. Because Infinite isn't "trying to get a revenge on Shadow" for calling him weak, he is applying his ideology.

Infinite see the world as only fitted for the strongest (we see a lot of that in the scene where he kills resistance members, or before his battle with the Avatar). He took pride of being that strongest ("the ultimate mercenary"), and Shadow showed him that he wasn't the strongest and could be seen as pretty weak.

Which create the tantrum : his view of the world only works if he is at the top of the food chain. Meanwhile the Avatar will have the exact same experience (with Infinite) and will become stronger, because he isn't a shitass that see weak people as worthless.

For me, making Infinite yet another "person that want revenge" would here be worse than the actual problem with Infinite, because it's basically stripping everything that's interesting with him, to make him a pretty generic villain. Combined with all the other problems of his writing, and what he *have done* it would have made him even worse, it would have felt like "let's try to find excuse for the genocidal murderer".

I think its current motivation is a more interesting motivation, because… it's basically really common, people that thinks like that. The problem for me is that Forces should have been more explicit (Tails' story in Forces have the same problem) and should have added more moment when characters *react* to Infinite view of the world. Sadly, he only say it to the Avatar, that can't react. And he should have had a closure (final boss or not), Sonic giving him a final speech making everything explicit.

##

His vision of the world. The whole game he show that : he thinks that people without power (here it's raw power, but that's basically the same way of working than a lot of people that see themselves as superior to the "plebe" because they have some capacity) are worthless and that they can be crushed by his might.

What happened to him is simple : he have a "power = worth" vision of the world. He see someone more powerful than him - and by far - and thus his entire world break : if he isn't the most powerful, he is worthless. And thus he needs more power to become worth.

It's supposed to be an inverted mirror with the Avatar, which suffered the exact same thing than Infinite (seeing comrades being crushed by an all-powerful being and then feeling weak and powerless compared to him), but decided to do side with other people instead of just trying to egoistically gain more power.

He is a pathetic asshole.

He entirely is.

( That's why I'm frustrated that we didn't have a final "Super Sonic" boss where he get a good "reason you suck" speach from Sonic, that would have been really cathartic. )

##

Infinite isn't evil because Shadow called him weak : he was already working for Eggman to begin with. He was already an asshole (that wanted to see the world burn in the official prequel comics), what Shadow did is just to make him want even more power because of his already horrible world view.

##

Honestly, even if Forces is horribly written, I prefer the idea of a pathetic Infinite than the "epic dark Infinite with a understandable motivation" that many want. For the simple reason that that kind of pathetic nemesis is the perfect foil for both Sonic and the Avatar.

​

We got character with understandable reason. A lot of them. The rage of Chaos, the sorrow of Void, the grief of Shadow... I think that having for once someone that have the MOST SHITTIEST REASON is really cool... He went his way because he had an "ideology" (in quote marks because it's a bit "cartoon ideology") that works only if he think himself the strongest (it's difficult to say "only strong should survive" when you don't see yourself as the strongest)... and went even worse than he already was when he was showed he wasn't the best ever™

And it's even more interesting because he simply use a source of power that isn't his real power and try to go god-mode with it.

TBH, the reason Infinite is interesting is that because often people that act like shitty person (harass, etc) and attack in an edgy way (belittling everybody for instance) other people thinks they are people like Shadow/Void/Chaos (or the Joker, to go outside Sonic)... when they are more people like Infinite.

​

But they should have played with that. Infinite should have used that as an excuse. Be an hypocrite. Say how much the world never gave him what he deserved. Go on and try to make Shadow feel responsible for what he did. Sonic should have discovered the "reasons" of Infinite and gone angry about that.

Sonic should have made a "reason you sucks" speech to Infinite. Showing him that he wasn't just some kind of nuclear-powered bully. That in the end, he was "nothing", that was just the Phantom Ruby that got him the capacity to went with his dellusion.

Even if I can feel that an anti-climatic death can be interesting for such a character, it was horribly done imo. They should have either conveyed more of the patheticness of his death, making even Eggman sadisticly ironise on it, or give us a final boss where we go totally corrupted by the Phantom Ruby, and Super Sonic feeling a mixt of disgust and pity to him.

​

So : TL;DR, i prefer the concept of the backstory of Infinite, but I feel that the execution should have been WAY better to really use it as it should have been used.

##

I'm not sure that it's supposed to justify Infinite attitude, as the comic they co-wrote also show that Infinite was a shitty person even before being employed by Eggman (as the PR showed that he wanted destruction and stuff).

It caused his thirst for power, but not being an horrible person.

##

Yep, they are inversed mirror of each-other. They have lived the same scenario, but chosen different path.

I would add one more thing to what you are saying, it's that the way each have chosen is different.

Infinite have choosen the "easy" way. He unlocked strenght from a power-gem that give him the capacity to do what he want. (and like you said : his quest for power was vain and self-motivated). With how he was beaten by Sonic + Avatar, we could say that by choosing th

The Avatar on the other side, got good by working with other to save the world. We can say that he have choosen the "real" (according to the game stence) way to get powerfull going full american and bringing a weapon working through a goal. We can see that with how the game show the Avatar and Sonic doing incredible stuff, like going out of Null Space.

All this fit in the overall theme of the game : Illusion vs Reality. Infinite quest for power is seen an illusion (that eventually backfired, reducing him to some combustible for the Phantom Ruby), while Avatar quest for becoming stronger to help other is seen as the "reality".

I really would have loved to see what was the first script before Pontaff arrival, what ideas had the original writer.

##

For Shadow against Infinite, I feel that Shadow's insult were too much over the top. I feel that it would have been way more effective if he just went a simple "... weak" constatation, maybe more even talking to himself like "he looks weak no need to go full power".

I feel that if you want your antagonist to be a thin-skinned asshole, just go all the way.

I tend to say that a lot, but I really feel that they should have gone 100% the way of "Infinite is just a bully with thermonuclear power and don't deserve pity" BUT that see himself as a scarred, charismatic ennemy with a tragic past. A speech like Sonic's speech to Scourge in Archie would have been FIRE, and with all the "redemptable sad villain" that are in the Sonic universe, it would have been really interesting to have one that is just a piece of shit, but act like if he had any legitimacy.

It would have been a very effective foil for someone like Sonic, and could be the kind of ennemy that make Sonic really angry.

##

Well tbh, except of the "fallen comrade" (that shouldn't be there, Infinite shouldn't have any legitimate justification to really work), he basically have the same origin story than most of the real-life people like him : basic "let's post on unpopularopinion subreddit than only the strong should strive" asshole that jerk off on power.

In a way it's the most realistic antagonist in Sonic lol : the world contain a lot more of Infinites than Merlinas and Shadows. ( And there are a lot of Infinites that believe themselves to be Shadows )

( But to be honnest, the world could be seen as having an awfull characterisation and story too XD The scenario of IRL is kinda shit. )

##

They are actually even simpler than that. The Shadow situation motivated Infinite to gain power, but before that, he was already stated to be "tired of the state of the world", and the first time he touched the Phantom Ruby, he saw the world burning (in the official prequel comics).

Basically his real motivation is "I want everything to burn". Shadow just added a layer of beating him down.

( Combined with how he reacted to being called weak, we can assume that he already had his ideology of "only the strong deserve to survive". He certainly have a bodypillow of Senator Armstrong. )

##

That's not what's shown in the prequel comic. Infinite basically started working with Eggman after he proposed to hire the jackal. He stated his motivation as having "grown quite tired of this world as it is", despite having the Jackal Squad trying to warn him not to do that.

With immediately after the contact between Infinite's sword and the Phantom Ruby causing an illusion showing a utterly destroyed world, it seems that Infinite simply want to see the world burn. If we had his ideology from Forces (shown when he says that only the strong should survive and breaking down at the mention of being called "weak" by Shadow, we can infer that he want a world of pure war where the strong can strive and the weak would be crushed by the strong… He would have loved Senator Armstrong from MGR XD )

##

Infinite is an asshole even before he is "transformed" (well, Surge might have been one, but it's not an element in her story now), and show from the start belief that "weak" deserve less than the powerful, acting as if they're nothing. Infinite is more something that is constructed in opposition to the "Sonic-Avatar" relationship, and I feel they don't tell the same kind of story. There are a few moment where Sonic Forces act as if we're supposed to feel bad for Infinite, but it's not its whole story. He is more shown to live the consequence of his own action, his own quest of power.

But we can see similarities to him in both Surge (edgelord, great power, nihilism) and Starline (not being happy with the state of the world and wanting a change that is more destructive, some kind of irony against heroes).

( In a way, I would say that Infinite is more similar to Scourge than Surge is, as he is a bully with thermonuclear power. Basically your generic internet edgelord. )