
## Forces vs 2006

You are : both are quite easy and hard at the same times, on different point, with different challenges and stuff that wouldn't be covered on a viable project.

    Forces needs some content creation (making the level longer, adding the two missing boss, adding an epilogue/final story), so it needs a bit more "creative work".

    Code-wise, Forces need way less work, you can make the gameplay quite good by adding back the drift, removing the sudden acceleration and replacing it by an acceleration curve (IDK if it needs code work of if somehow by tweaking some values it's possible to recreate) and fixing the homming attack. Most of the rest can be improved via physics tweaks. 2006 needs more lower-level work (that's why recreating it from scratch in Unity is one of the best way to improve it), and some gameplay needs deeper changes.

    Story-wise, the simplicity of Forces helps it a lot in how to fix it : you can improve Forces mostly by writing better dialogs. The fact that it have parts of its storytelling in-level would be a godsend for someone fixing it. A big cutscene needs works : Tails vs Chaos, and the battle would really need to be remade by Marza to be as epic as it would need to be. Improving Infinite via script is kinda simple too, either by making him the sad edgelord some poeple want, or making him being a self-entitled bastard the point.

    Sonic 2006 have more structural issues. Some issues are simple : Mephiles and Silver can be improved by changing some script to make Silver know Mephiles, the kiss cutscene is simple script-wise to change, but need a cutscene redone. But improving Sonic's story needs more structural changes (removing some kidnapping, for instance, and making more "important", the time travel), which is harder.

    Graphic-wise, 2006 suffer a bit more, but it's a 15 y.o. game, so it benefits a lot from being on a better engine, it can be improved via good GFX. Meanwhile, Forces is already pretty good on that front, so making it "better" without making PC melts like the shader mod do is kinda harder.

And in the end, with all these correction, I feel that both game would become kinda good, but in different sense. 2006 would be more extensive, more generous in content and more "epic", while Forces would feel more tight and controled, and maybe more "intense" (especially if the fixed level are made more arcade-y in their difficulty, which would work with the lack of live).

And both would have So some would prefer 2006, other would prefer Forces… which seems to me quite normal XD Both would keep having some flaws, tho, Forces would still have some stuff some dislike (Classic, Zavok), 2006 would still be quite awkward on some points (for instance I didn't talk about reanimating all the cutscene, as IMO it wouldn't be one of the priority in a "fixing" task).


"having the mechanics tweaked" often mean more work than most seems to believe, it's not always just changing variables values. Improving the level design of Forces is already done with the actual current build of Forces, without needing to change the code.

It's still work, but it's honestly doable.

##

Giving more time to a project and "finding a proper director" are two relatively different things. How the developpement was a mess shows that "not rushing" isn't the full solution that would have been been needed (which is kinda easy to says with insight, 10 years later, and without any stakes, in a way). And TBH, I don't think that with more time, they would have chosen a "proper director" (especially as Nakamura had an hand in many parts of the project).

ChaosX did an excellent job, and is a really good reverse engineer and dev. It doesn't change that P-06 isn't really representative that what would have been a "completed 2006", for the reason I said and some other. It's an improved, fixed version of 2006 with ten years of insight done with a more modern engine. Which makes it the definitive version of 2006 (because when finished it'll be 2006 but better) and for me something that should be played, just that it's different that what would have been done at that time.

What I said doesn't change how good P-06 is, just that it's simply false to use it as an exemple of what could have been done if 2006 had got more time.

##

P-06 isn't "Sonic 2006 that have been completed" thought : it have been rebuild from the ground up in Unity, which is FAIR different from being "not rushed", because you can fix deeper issues (for instance having better physics and momentum in the engine) and benefits from being on a more modern engine (Sonic 2006 already had some optimisation issue with how bad it looked).

It also benefits from the insight of having been started more than 10 years after the previous game, where a lot of the issue have been discussed, disected by a lot of people that know pretty deep the game.

I know it certainly sound like a nitpicking, but as a developpement process, it change a lot of things in what you can do easily, and stuff like that (That's why sometimes we prefer to restart something from scratch instead of dealing with the whole technical debt of a project).

( And of course, it doesn't make less awesome the work of ChaosX, and of all the people that participated to make this project a reallity. It's just that it'sa pretty different situation than "giving 2006 a few more years". )
