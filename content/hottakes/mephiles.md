# How to resurect Infinite ?

I basically don't like Mephiles because they basically didn't use anything of what is interesting in him.

He come from a good idea : creating a manipulator-type villain for the universe of Sonic. A Mephistopheles character was something basically totally the contrary of the type of villain we had (that were basically power bombs). The problems with that were quite a few :

    He is supposed to be some kind of ennemy of Shadow's Story... But don't manage to use his best power (manipulation) on him. Shadow don't get a doubt against him, and that's pretty lame imho, as it's just throwing his potential (even if adding YET ANOTHER moment of "Shadow hesitate to go the right way").

    He is yet another "shadow of shadow". That's not a problem in itself (it's a concept with potential), but the very next game after the Shadow Android arc, welp that felt to me like too much. (and tbh I would have found more interesting as a "shadow of shadow" a surviving Shadow Android that start to develop resentment against Shadow)

    He is kinda bland in his "character". He doesn't really show any emotion or reason to do what he do, and this isn't quite well used. Basically he only act like a funciton to me, and never go with any deepness or motivation. For me in this point, he is basically one of the worse, which is really a shame.

    How he finally kill Sonic in the last story, after having made a lot of complicated plan seemed to me just... too easy. It looked really rushed.

Basically, Mephiles is an ennemy that could have had potential, if only they really had tried to do anything more with him than just a function.
