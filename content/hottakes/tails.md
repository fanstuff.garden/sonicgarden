# Is Tails a coward ?

Not really. I agree that Tails isn't well-written, but he contribute more to saving the world that basically most of the non-playable cast (except Silver, which is the most usefull non-playable character).

EDIT: Shadow might be arguably nearly as usefull as Tails ? Tho even if he give some important intel about the Phantom Ruby and "save" Sonic from Phantom Shadow, Tails give the plan that'll save everybody, get the info about what exactly is the PR, find Eggman's selfies and do all the plans that actually work. So Tails is between Silver and Shadow (Silver is more "useful" than Tails because he create the whole chain of event that lead to the destruction of the Phantom Sun)


##

Even if I disagree with the current Tails characterization, I wouldn't say that seeing him as a "powerful" character is better. We already have too much powerful characters, and seeing Tails as that and "defeating massive ARMIES" and stuff is just basically bypassing what make him interesting : that he IS a 8-years old kid that have to do adventure. And that even with his quality, he is a bullied kid that have some anxiety/shyness problems.

Because we could have the following counter-argument : 
- Despite all that, he have been shown to be afraid by mondane stuff (Afraid of ghosth in Sonic Heroes), and shown as the "kid" character in many Adventure-era game (being easily teased by character like Wave and Rouge, saying the fucking "I shouldn't talk to people I don't know" or something like that in Sonic Battle). 
- Tails Skypatrol is a "fun adventure" only shown in a japanese-only game that isn't even though as canon since 1998. For everything we know, Witchcart might even not be a really powerfull antagonist - she was in Sonic Archie, but she also was a litteral troll.
- Basically, most of the non-megadrive classic game where forgotten from 98 onward. Because if we are taking your way of thinking, making Tails afraid in Sonic Adventure even if he manage to battle was already changing that, as he was shown way "afraid". They basically reverted his characterization to Sonic 2. 
- Some of these feat are "Tails' version" of these event. He fought Chaos 4 with Sonic and Knuckles, and in his story, we see him saying sentences that are said by Sonic in his story and even battling Knuckles whereas in the cutscene only Sonic battle him. Tails' story show only a part of the event. Same is true with Tails' story in Sonic 2 and S3&K. It's alternate versions, it doesn't mean that he beat that himself.

I don't say that what they did with him was good (it was pretty bad... Not really Colors, because Sonic pushing him was basically the first time Sonic was really shown to want to protect Tails, and Tails wanted to help. Not to mention that Eggman tried to control Tails just before, IIRC, so Sonic's reaction is pretty much the mature one, for once). But basically, the way you depict him is for me really bad too (not worse, though). Because the whole stuff about Tails is that he is an anxious, shy and a bit afraid kid that happen to be very smart and live adventure.

TBH, for me, showing Tails afraid and more "fragile" than the other character isn't bad in itself, as he is a kid that should be protected. What they should show more is how with that he have some sort of courage, just not the same of the other character (who basically rush on everything as your friendly barbarian player in JDR that leroy jenkins all your friggin bosses).

For instance, I have no problem with him being afraid from Chaos 0 (as tbh, the franchise is pretty ambiguous about the raw power of Chaos 0, as he is shown as powerful in battle and other media). What they should have done is more making Chaos 0 in this cutscene a real threat. Show Chaos 0 as powerfull, make him difficult to get hit, make him hit hard Tails a bit to show us WHY Tails should be afraid of him. And then give us a boss against him (if possible at the end of the Adventure instead of just after that, in order to make us see how much he is powerfull).

The problem is that Tails' story is basically invisible in Sonic Forces. The goal of his story is to show him rises to go from "he was depressed and took down by Sonic being "dead"" to "becoming basically the main stratege of the Resistance". And for me, that, but better written would be a better story than nearly anything based on "he beat the Battle Kukku Empire single-handedly" xD

##



>  The idea of making him run alone and away from his friends after the loss of a loved one may have some sense, but not for this character specifically. He can't fight, he is in a Rampaging world, and he is sweet and loyal to his friends. His final speech is so hypocrite.

I think that on the contrary, this kind of story is more interesting when it make the character do something that they wouldn't do when they are "normal". We mostly in real life see that something is wrong with someone when they act "out of character".

And for Tails, seeing him alone and having him be separated from the rest of the group was a good idea - that they messed up by focusing on "WE NEED ONE SONIC" instead of focusing of how he suffered from the situation and how it messed up his well-being.

​

>    It clearly shows that Sega doesn't know what to do with the character. I would like to ask, how do you think Tails should be like ?

A genius, intelligent, sweet and capable **kid**. And the last part is the one that should be way more used. I would also make more proeminent the signs of anxiety, needs of approval and the tad of emmotionnal dependance that he have. Play with that while his intellectual maturation is quite high, his emotional maturation needs help, and an help that Sonic is sometime unable to give, having never felt as Tails feel.

In Sonic Forces, that would make been shown as quite the same events, but presented differently.

- Don't show Tails' fear as something cartoony. Show genuine fear from a lost child facing the god of destruction. Even make Chaos hit him a bit, to make him have a reason to do something as illogical as calling Sonic for help (and maybe with a different tone, a more hesitant, deeply panicked tone)

- Change the "WE NEED ONE SANIC" by a more conflicted feeling toward Classic Sonic. Basically having trouble at being happy seeing him, because if as the same time he feel that he is their best chance, he is for him a constant reminder that he lost his best friend.

- Add a feeling of guilt. Survivor is a common case when people loose someone, make Tails feeling that everything is his fault (maybe making his unability to repair Omega a consequence of that), and showing the vicious circle. Say that he run of because he felt worthless, or something like that. Thus, Tails's lesson (that obviously can't be to learn to cope with death, as Sonic isn't dead) would be that he wasn't guilty. And the basic theme of "you can't heal alone", and how we need people.

- Show more how he regain confidence while starting creating good strategies. Basically, make other people recognize his strategies, especially Sonic. Also : add a Classic Level in Metropolis, between Sonic and Avatar being sent into the Null Space and the stage Null Space. This time, showing that Tails' isn't alone, and would have been to cope with it. It would be the "end" of his arc.

​

Basically, make visible his arc, and make the writing more centered about characters. For me, a story should be more an occasion to explore how they react to the situation, what are their flaws. And to make them suffer for the glory of Satan, of course


##

The idea was really interesting though. A broken Tails, having to rebuild himself before becoming the one that made the strategy that helped to win the fight (because Tails had basically all the idea toward the end of the story). The problem with that is that it was so much badly written that his progression is nearly made invisible, and that the most important points are made too much cartoony. (tbh, being afraid of Chaos 0 seems pretty natural as he is supposed to be powerfull, and that Tails was never alone during this fight - and that most of his story in Adventure is difficult to tell how much we see is true, as some cutscene are even changed compared to Sonic story…)

The problem is that they made it ridiculous, instead of making it a "real-looking" fear. Like him over-analysing the situation, having a PTSD flashback, or anything like that that would have made his reaction more natural instead of just a cartoony reaction. Like most of Forces, the interesting story and theme are made invisible by the writing that is too much exagerated, without any ounce of subtility. And every other them are treated that way, the problem with Forces is that it's full of great concept that they are unable to really use.

##

It "fixed" that, but not in the way many think, as it mostly made the situation less "black or white".

For instance, it used Tails writing in those game to show that progress is not linear (which is the most intelligent way of using that, and show why just doing what fans want would be a missed opportunity) and both Eggman and Sonic have different interpretation of Tails situation, suited to their character : Sonic, being kinda emotionnally intelligent in this game (and in most Flynn's writing) is understanding that Tails is a child and that failure is normal, while Eggman analyse that Sonic stop Tails from reaching his full potential (and I think that Eggman and Tails have different vision of what should be Tails "full potential"… I'm pretty sure for instance that Eggman would be impressed by Nines).

It's interesting because Prime goes in kinda the same direction that what we could infer from Frontiers : without Sonic, Tails could be more "strong" in apparence, but be even more messed up emotionnally without any support.

In a way, those two story together show a pretty interesting vision of depression, the need for emotional support, and what life we kinda want.

I'm curious of how they'll handle "Tails having had his own little quest", what he'll discover from him. If they're intelligent, they'll do a bit like Antoine in Archie, making Tails learn to handle his fear and self-doubt, instead of the classical error of "erasing them" (the best would be him to understand that him needing some help isn't weakness, and be able from that to work better when Sonic isn't here, while still benefiting from Sonic's support)

##

Tails is emotionnaly somewhat dependant, but it's kinda natural for his character (a bullied child that look up to someone flawed-yet-gentle-and-impressive). In survival skills alone, he could do without Sonic, but for his mental health, it would be a catastrophe.

( And even in Frontiers where he tries to become "better" - it is somehow related to Sonic. )
