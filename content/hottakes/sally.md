
# Sally

I really feel that a retooled Sally could complement well the "mainline" cast in a new serie/the comics/the games/wherever she could be used, as she would add a type of character that could bounce easily. Adding a level-headed serious strategic character could really be fun, with nice interactions with other. Especially if they play add a comedic side to her. Being "bossy" would kinda make her sometimes too much annoying, I feel that nowadays, stuff like making her serious business even in "normal/casual" situation could be really fun (I really like characters that tend to overthink simple stuff, or take too much seriously for instance a simple sport match).

Not that it's for me the most needed thing for Sonic, but it could be nice if SEGA ever decided that. And it would allow Amy to be more fully herself which would be better IMO (I'm not too fond giving Amy Rose the role of being the "serious person" because her being a bit wacky is part of the fun).

##

Actually for this one this is not quite true : the deal only affected the comics and the reboot, and the rules on how characters can acts appeared before the whole Penders affair and are mostly unrelated (except that his character can't appear, but it's not really a "mandate").

( The lawsuit was even really launched by Archie in 2010, and the Penders against SEGA in 2011. So basically when this whole mess really caught fire, the less serious games already existed. )

​

What Penders killed is mostly the old ArchieSonic timeline, maybe by echoes Archie Sonic post-reboot, Sonic Chronicles and his own reputation. And before that the Sonic SatAM movie project.

But he haven't really had an effect on the games.

## Sonamy vs Sonally

It's not a question of "where it is made", but more a question that at one time, there where two vision of Sonic. If "USASonic"/"JpnSonic" aren't relevant for todays IP (but mostly because JpnSonic is the norm now), this dispute is needed to understand the whole shipping war.

About which ship will be canon… Well the answer is certainly none, so for me it's a moot point.

    With sonamy, they'll bully, harass and try to force you to see things the way they see, not to mention they themselves have a share of harassing Sega, i.e trueloveheart and so on

Yeah there is shitty Sonamy fans, and some that are really harasive (for instance when there was the Shadow x Sonic shipping during Chao Races and Badnik Bases), and they'll certainly be a lot of "Sonic X boomers".

But honestly… I really don't think that the "shitty harassers" of one side is better than the others.

And as someone that really liked the USASonic (and that advocate for more addition of that in the "Japanese Meta-Canon"), I don't want to cling to the false idea that "my side would have less issues".

Especially as it's not really an issue of "which side is the worse", it's an issue about how people are ready to take a side that much they'll go all abusive about it.

    Also you can't really blame Sally fans sometimes as sonamy have gotten what they want

I don't blame "Sally fans" : a lot of them are really nice persons that I got really good discussion and fun with.

But I'm 100% blaming the small group of Sally fans that sent death threat to Flynn when Sally and Monkey Khan were together, or that are STILL going completely rabbid against Jon Gray for "The Slap". Same for when they tried to make IDW lose the Sonic IP or Flynn be fired to "save the FF".

In a way, I think that this groupe have been as much or even more a cause than the anti-Sally haters to make fade out Sonally. And the drama in the Rally4Sally campain also was really detrimental to the Sally fans (this shit which was painfull to see, as I advocate for Sally being a "5th main" to the cast).

###

The Sonally and Sonamy dispute is strongly related to the "USA Sonic" vs "Jpn Sonic" debate, as many fan of Sonally will prefer often the "USA Sonic" while many fan of Sonamy will often prefer the "Jpn Sonic". In many case, you'll prefer Sonamy or Sonally depending of what you prefered between both ideas (note : it's not always the case, of course )

When Sonic X was released, a lot of fans of SatAM and Archie strongly disliked it, for several :

    A first one is that it was seen "for the kids" compared to Sonic X (heck, even in France where we got a more similar show to the japanese one we saw that !), especially with how some character like Eggman where handled.

    The second one was that it was another part of this dispute, between "what is the best Sonic" between the USA Sonic and the Japanese Sonic. Often, the first element is used as an argument.

And as shipping is something important for some fan, they fought each other because the two main side have a big Sonic shipping, either Sonamy or Sonally. And something that made the ressentiment worse for Sonamy fans is that Sonally fans where often older, at that time, so had some adventage on that front.

So all that is honestly a mess. And it's made worse as there is still people that are ready to attack other people for just these shipping dumbfest.

For a long time, there was a statut quo as Sally was part of the comics. Since IDWSonic, SEGA doesn't seems for the moment interested to see Sally, which makes happy some, angry some other. The fact that some Sonally fans have attacked Flynn and accused him of being the source of Sally not being here, and the stuff with the "futurists" and other groups also not helped her press. But honestly, I wouldn't say that they're the only one that acted like bucket of shit in this story, some Sonamy fans have been extremely toxic about that.

Now for the question of canonicity : Technically, for the moment, Sally isn't part of the mainline Sonic universe, but of other Sonic universe. (But technically a lot of classic character aren't canon either to Modern Sonic). And the new IDW character aren't canon either to the mainline universe.

Now for IDW, Flynn says he is waiting the "right time" to push again to make her appear, and that IDW Publishing know that they are wanted. It seems that SEGA aren't hating them, but that it's more a question of convincing them that it would be an interesting move for them.

###

Well, to be fair, this part happenned because of the shipping wars. Flynn got so much attacked from people from both ship that he simply didn't want to deal with that.

And the issues, aren't related only to Penders, it's a way more complex and old stuff, and kinda comes from the "USASonic vs JpnSonic" disputes from the end-90/early-2000 (especially the "Sonic X vs SatAM" dispute).

And honestly, I understand why Flynn is burned out by this shit, and don't want to deal with that.

###

Well, sadly Spinball is difficult to really say "canon". It's a sidegame from another team in the 90's, that haven't been really referenced since… There are a few game like that, where their canonicity is really up to the player. I think that if Sally went to appear in the game (something that I'm not against), it would be a "brand new Sally", and spinball wouldn't really be referenced ever again.

My problem is more saying that the character is unimportant because "not canon" to the game. Even if Sally isn't a game character, she is a part of Sonic's legacy. And that's why I would love if she appeared in the game (especially as she could cover a niche that the "big 4" characters doesn't really cover), she is a character that is a big part of Sonic's legacy, having appeared in 25 years of comics, novels, two cartoon, a theme park…

###

I've got nothing against both ship, but this shitty shipping war is fucking terrible :

- In the Sonamy side we have some people going "But Amy is C A N O N" (which by itself is for me a reason to canonise Sally) and trying to use every tiny bit of unrelated trivia to prove that they have the T R U T H, and attacking every people working on American Sonic canons or simply liking these cartoons. They also go harass some other ship, I remember one going under every sonadow post during the release of Chao Races and Badnic Bases to try to "prove them wrong".

- In the Sonally side, we got some people harassing Jon Gray, Ian Flynn and nearly every other people that worked on the comics since "The Slap". Post reboot they tried to attack as much as possible the comics because Sally wasn't with Sonic anymore, and now well, the "Ian Flynn conspiracy theories" come from this side…

And both side got ugly stuff as murder fics, harassment of fans that like the other ship. I like shipping, but oh god some people are annoying with that. Fun stuff is that Flynn got attacked and insulted by both side. There are also good people in both ship, but that's the rule of the internet, we don't hear them. The toxic shipper are just the noisy minority, but oh god how much they are annoying.

​

Let's just ship Sonic with Eggman and call it a day */joke*


##

TL:DR : it's a mixt between a reaction against the actions of some toxic SatAM fans, a rejection caused by the war between partisan of the japanese and USA side of the IP and the action of some loud annoying SEGASonic fans. And it just makes all that annoying for people that would just want to enjoy the IP in peace.

​

It's basically the result of a lot of things, and a good example of how the perceptions of the fans of something can influence our perception of the thing itself. Many people dislike the Freedom Fighter and the USA Sonic canon because they dislike the fans of theses things, and often because of some old actions, or some toxic fans.

Before, it was the "SEGASonic vs USASonic" war, the shipping war or everything about Sonic X (some SatAM fans hated Sonic X with heart). Nowadays, what hurted the freedom fighter is mostly the constants wars inside the comics fandom since the ArchieComics reboot (and sometimes since Flynn arrival as the comics writers).

- At first, it was the hartred against the reboot (basically if you said you liked the reboot, you had many people spawning in your mention saying how much it was shitty compared to pre-reboot), because it was seen as "too much like the game". I'm not talking here about people just angry about having lost characters they liked, but the people harassing people and stuff.

- Then it was the one against IDWSonic (fun things is that you have also thus some fans of both pre/postboot going in the melee xD) for the same reason. Again, I'm not talking here about people being angry about Shadow, GUN, money or stuff like that, but really about groups like the Udon4Sonic stuff (a movement that even got someone working for Udon saying that this should stop)

The action of a small really dedicated and toxic group of SatAM/FreedomFighter fans during those two eras also damaged a lot the reputation of the ancient occidental canons : the attacks and harassement against Ian Flynn (especially with the conspiracy theory video by people like DHT, or attacks from the futurists).

The drama surrounding the Sally4Rally events (basically an old admin being a really infamous member of the community under a false account, and having lied about having been told stuff by SEGA, or having annoyed/harassed voice actors) also helped create a really negative perception of these stuff (even if the movement seems to have gotten more healthy since). Another recent event that could affect is the harassing of the fan-artists Kitaro by some fans.

Basically, all those things created for many people an aversion to the stuff that the fans responsible for those harassements and toxic behaviour. It's especially sad, as it's just the action of a few fans, and that a lot of SatAM fans aren't like the few fans that are responsible for those actions (a bit like not any Sonic Adventure fan is as agressive and toxic as the people behind the SA3 facebook page)

​

Another part, is that these opposition have crystalized opinions about the FF, basically creating for many people a sense of identity in being a fan of the occidental or japanese canon. Thus a lot of people dislike the Freedom Fighter because they aren't part of the japanese canon, and that a lot of these dipsute make both things seemingly "irreconcilable".

( Even if for me, the post-reboot canon of ArchieSonic showed how much you could infuse some elements of the american canon inside japanese Sonic and I feel that in this two-world era, elements from SatAM and Archie would really help to flesh out the Animal World and create a strong distinction between it and the Earth, by having kingdom and magics in Sonic's world, versus the sci-fy Earth. )

But this (with the past elements) created that seeing people being annoying about stuff that some current fans doesn't even really know, especially with this aura of "it's so much better of what you like" made a lot of people angry. The shipping war make that a lot of people prefering SonAmy would thus hate Sally too.

​

It's a bit sad, in a way. But it's difficult to solve years of dispute, and I'm not sure that the Freedom Fighter will be solved one day. Basically, we tend to easily hate the stuff some people annoy us about how much it's better than what we love (it's a bit like why some people will tend to mock TTYD or Radiant Dawn/Path of Radiance fans when they like the newest Fire Emblem or Paper Mario).

And I says that as a big fan of Archie post-reboot, and I'm a vocal supporter of importing the Freedom Fighter in IDW (with an overhaul to adapt them to the IDW settings, of course).

And of course : some vocal anti-FF are also annoying pro-SEGASonic/SonAmy fans that are harassing people that doesn't think like them, there are also shitty people in this part of the fandom (like the few people attacking every person that liked interpreting some Sonaze or Sonadow shipping in the latests IDW comics, for instance…).

​

( Wow, this message is really too long. )

## 

>  she's obviously the precious writer﻿﻿ pet﻿ who steals the show, perfect in every way, with﻿ big responsibilities on her﻿ shoulder﻿﻿ 

 I totally disagree on that point: she wasn't much that anymore in the reboot, actually. She is a nearly shown as an emotional wreck at some moment, that failed some mission pretty hard and shown to be a bit too nervous and bossy, and that need the other to work well… And each time there were a "who is right and who is wrong debate", most character were arguing for the "right" thing, and not just her, so the isn't shown to be standing out to other character on that point. (even if I have a problem on that last point, I would have loved that at least once, Rotor "pragmatic vision" would have been right, in order to make it more interesting). TBH, she isn't more a "mary-sue" or "perfect" than many actual official characters. TBH, the biggest thing that are still "like that" is that she have big responsibilities" (that are way more shared than before) and a "neat title". Not unlike Knuckles, Shadow, Blaze…

The case of "precious writer pet" is especially interesting though, as in the case of the reboot era, we got a lot of people saying that Amy was that, and other saying that Sally was that. And both side accused Flynn of disadventaging the other character. And I already see some anti-IDW people accusing the new character of being that. There will always be people to accuse writers of disadvantaging their character or to "replacing them" with other (or new character to be either "mary-sue" either "unlikable"). It's a bit of a vision that exists in fandom - I don't say that any fans thinks like that - where the writers should do a story not to tell a story, but to create some kind of "justice" for characters, in a sort of "democratic" vision that doesn't really suit storytelling imho.

For the rest : eh, I don't really care who get the sunlight, not anybody can be in any story, and there will be always other stories. I don't need any character to appear in the game, but I would love a lot of them to appear even just once, just to see what game story can be told with them. As I said, any character can do, if the writer have a good idea to what to do with them. All of them have a lot of potential, and can be the source of thousand of interesting and fresh stories. The question is just to use them to actually tell story (or/and use them in a interesting way gameplay-wise) instead of just wanting to "please". That's why I said

But I think that the subject of "how could we make any character appear in the game" is pretty interesting, because it add to us another constraint. Mostly, a game character is designed often around what is needed for the game. Here, we have some constraint based on what was already here. And that's what can make us especially smart of creative. Writing is always more creative under constraint, because it force us to think more. To find solution to get most of what we want, while making it work with the constraints.

For Tangle and Whisper, that's especially interesting because we already have a blueprint. For Tangle, I can imagine something very aerial, with importance of grabing and air action. (and as a lemur, she could also be especially good to climb ?). For whisper, it would especially be usefull to have a more "action-y" gameplay (and for me it would solve one big weakness of the avatar, that we have to always change weapon outside of act, making it less interesting imho). But there are a lot of possibilities. 