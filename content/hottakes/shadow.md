
# Shadow

Shadow is a hero. But a darker one.

Yuji Naka said a while while ago that the main difference between Sonic and Shadow is their method, or something like that. He is definitely a hero. A tsundere-esque (that'll act like he doesn't care about other when he does), stoic, sometimes reluctant and even slightly misanthropic (which can be kinda normal when you lost everything you cared about) hero.

Shadow will often use darker method and will be more ruthless than Sonic. The best incarnation of this debate is in IDW5 : where Shadow want to kill Eggman, and Sonic doesn't agree with such a course of action. Shadow will go way bigger length than Sonic to get what he want, which mean that he'll attack the strongest threat instead of the smaller ones or have a way more "tunnel vision" than Sonic. It can be seen also in Sonic Battle, where his first course of action is "destroying Emerl", not caring about that it have feeling. Another good exemple is in Sonic X, where what is his solution to knowing the truth about Cosmo ? Trying to murder her, and nearly hurting most of the heroes doing that.

For me, the best example of where Shadow really stand is the Maria promise. Shadow never started to "loves" humanity like her. His heel-turn is basically understanding "she wanted me to save everyone not kill them" to find his purpose and not really "becoming better" (in a way, Shadow never got a "redemption" in the way he admit having done bad things). He doesn't act because it's good. He act because he made a promise to protect the planet, and that now it's his purpose. He show how he needs some help to stay in the light. (Also highlighted in IDW when Rouge talk him into making the good choice, which is interesting as she doesn't coerce him, she just make him snap out of tunnel vision and thing clearer. )

But with all that he'll act also by pure sympathy sometimes, there isn't many exemples in the games (the one I would see would be saving Rouge in SA2, complete with the Tsundere bit XD), but one in IDW, when he is (spoiler) utterly angry at chao being mistreated and directly decide to change his course of action to teach a lesson to Clutch

Shadow is basically the "good is not nice" tropes. He is part of the good ones, have important objectives to save people. But he'll always do things that the other character would not really approve, because he doesn't act "because it's good", but "because I *have* to protect this planet". In a way it join its Sonic Channel profile "he has a dangerous sense of purpose".

So is Shadow part of the "good guys" ? Yes, of course. But he is complicated. That what makes him one of my favourite characters of the saga, because he have struggles, he doesn't have a "clear" progression, it's not as simple of "I let the past/decided to be good behind me now all my problem are solved". Sure, it's a bit "by accident", it still makes him really compelling to me.

It's for me just a bit under Zuko and Catra in term of redemption story, mostly because it's not really a "true" redemption story (Shadow never show any shame about what he did in SA2, unlike Zuko and Catra), but he is still really interesting in his own right.


## Message 1

They aren't new, but they have been pushed up. Shadow had these trait before, but they were kinda subdued in the previous game. It's always a question of how it's shown more than what is shown.

In Forces for instance, the way he talked to Infinite can be "too much" for some, as he was before often more "don't make me loose my time" and leaving, more than "don't show your worthless face to me" : here he really continue attacking someone really on the ground xD (well, in a way, we can see that Infinite kinda deserved that). We also didn't see the introspective streak that Shadow can have in both Forces and TSR.

(The Chao gag in TSR:O also rubbed wrong some of his fans)

Shadow is made of the same ingredients than before, just some traits have been pushed up, and one is less present. A bit like in the Metal Virus Shadow, in a way : we lack the introspective moment that make Shadow "more that what he seems", which is what makes Shadow a bit "different". I don't think that SEGA forbid it, they might just like less it happenning with words (which happenned before, like in 06 or Battle).

He can still have the introspection, I mean, it's only really absent of IDW19, Forces and TSR. It was there in IDW5, IDW30, CR&BB, and Sonic Channel's Sonic × Shadow. It's more that before, he was kinda a bit more "vocal" about it (for instance in 06, Battle, etc. he vocalise a bit more what he feel), and the "edgy" streak was kinda subdued (except in Shadow).

So I think there is a small shift in presentation. He is kinda shown as edgier/angrier in latest games. But I agree that this isn't a complete 180 shift, and that I think that with the right dosage, it even can improve the character, like in Sonic's Channel story, or he is even better than pre-06 Shadow IMO (I don't like too much 06 Shadow).

( I can understand why some elements like Shadow not wanting to help repairing Omega can annoy some fans, tho. )

## Message 2

Even if it's not a "Shadow mandates", SEGA ask Shadow to be "in character" (like eveyr other Sonic characters). It's mostly that we both don't really like this "in character", and that there is some "fumble" about how to handle it.

( For instance, I kinda prefer Evan's interpretation of Shadow, where he is more tsundere-ish, even if still having stuff like him not wanting to help and needing to be helped to do the right things can Rouge can annoy some fans, especially compared to the evolution Shadow got. Personally I kinda like that, as it makes him less "perfect", but I can understand why other would be annoyed by that. )

## Messages 3

Basically the idea behind that moment was that Sonic reached for Shadow, and Shadow mistook that for Sonic laughing at him, and then left. And it's an interesting writing of the character, because it makes him more realistic : a lot of people that I know that have left hardship can be on the defensive and mistake those movements for dismissive action. For me, it makes the character far more interesting, and give us more insight of his trouble.

IMO, moving on doesn't mean "stop being sad". It's like when we loose a relative, even after we "moved on", we can still be sad/melancholic for the time where there are alive. It's that we don't let these tragic event define us, and I feel it's like that for Shadow : of course he well certainly still be somewhat sad forever for the tragedy. But he doesn't want to crush the world in revenge, or won't let this define who he is.

IMO, I would say that making Shadow an agent of GUN is a bad idea : even if the "people" who killed Maria are dead, the structural power of GUN have still deep issues, and Shadow's experience should make him warry of them, while not having hatred anymore. It would be far better writing, especially as it would allow Shadow to "move on", without "forgetting" (which are two different things).

And that's why I love the Sonic Channel story : he his focused on his mission, he can be somewhat jerky on Sonic and have this "tsundere-esque" attitude toward Sonic that makes their relationship fun, and have the most interesting vision of why he act (he says to Sonic "have you seen what people do to other ?" which is an interesting insight on how he can move on, while still being drived by his experience). He haves elements of every Shadow, turned into an interesting package.

I feel that it's the issue of most Shadow post-Battle : they are incomplete. Shadow is a lot of things : a victim of a tragedy, a proud rival, the "ultimate life form", a powerful heroes, someone hiding a lot of his emotions, a more soft, reflexive side... And I feel that except SA2 and Battle, all games and incarnations tends to miss some part of him, which is why I liked that much to see a nuanced, complete take on him, making him feel whole for the first time since 2003.

(Ian Flynn's Shadow also tends to have issues on that, with a very chatty Shadow that thus tends to blend a lot into his other characters that alls tends to do that, and I feel it also participate to difficulties with writing the current Shadow, that have as a rules "he tends to hide what he feels" - something that Evan Stanley somewhat handled better imo with her Shadow doubting more).

About him being an anti-hero, well it have been his definition post-SA2 : his difference with Sonic is that he his more ruthless, and his ready to use stronger mean to achieve often similar ends. The Sonic Teams said that in most interviews since that time, so I feel that even if we can prefer a more "full-hero" Shadow, him being an anti-hero is one of his reason to be after he have been resurrected by the ST.

## Messages 4



Well, the Shadow one is a bit more complex, because if anything, Shadow was shown as kinda complex in the latest Sonic Channel story : still a bit meaner like in the current comics, but with a melancholic strike, and some issues to talk with people (for instance he confuse a gesture of compassion of Sonic for mockery). So, it's difficult to know if SEGA really want that… and what they exactly want. Or maybe they WANTED that and that there have been some shift behind the scene. IDK.

( I might be biased about this one, because "Sonic Channel Shadow" was for me the best Shadow since SA2 and Battle, the first one since those that really for me fused well both side of Shadow.

2006 Shadow is too simple/"archetypal hero"/"jesus the hedgehog" for me and edgy!Shadow lack the moments where he is more likable which is necessary to write an antihero like that. )

##

The info about them comes mainly from Ian Flynn and Evan Stanley, the comics writers.

You can find it in some of the Bumblekast show by Comics writer Ian Flynn (I don't remember exactly the ones). Basically Ian says that he wanted to write Shadow one write, but SEGA mandated him to be like he is (without exactly telling us what was changed, I think ?), and that the Team Dark doesn't exists in-universe and aren't "friend". It's dispersed in different bumblekast so it's kinda difficult to find them exactly. I'll edit this post if I can find them.

Evan Stanley have a kinda different take of them on her tumblr :

- https://spiritsonic.tumblr.com/post/638318373742182400/how-do-you-feel-about-idw-shadows-personality-do

- https://spiritsonic.tumblr.com/post/638320963113664512/the-impression-ive-gotten-from-people-comparing

- https://spiritsonic.tumblr.com/post/645520536487772160/why-do-you-like-to-ship-shadow-with-rouge-is (the one is more about how she see Rouge and Shadow relationship, but there is a small bit about the restriction on the end)

​

Basically, both writer have a different vision of them, Flynn seems to feel more restricted by them ? (maybe because his vision is more different ?)

##

I don't think that it's a question of "human" or not, I think that it's mostly that he is misanthropic, and that saving the world because he made a promise doesn't make him an all-loving hero. He don't like people (which can be understandable with his past), but have a "strong sense of purpose", and his purpose is eliminating what put the world in danger.

I also kinda likes Evan Stanley's take about him and Rouge, that both makes the other a better person than if they didn't knew eachother.


## SHADOW AND GUN

### Message 1

I'll wait having more evidence of Rouge having been able to dismantle that. And honestly, if the organisation make it that easy that a personnal vendetta from their commander can compromise the whole world security, it's not that good.

The purpose of GUN is "being protectors", but IMO, we don't really see that in the game. GUN is most of the time either shady or useless.

Which make using that to show that he is "truly unbound by his past" (which is already an issue for other reasons) isn't really great.

### Message 2

I dislike it too, because the organisation is still shady in Sonic's present. The Commander was ready to act just because of an old grudge, in SA they're ready to emprison Sonic without any trial or anything. So having Shadow joining them is a tad annoying to me. I prefer him being more independant, and acting according to his own belief and goals.

( And I especially like how those are framed in the Shadow Sonic Channel story. )

And I don't even really like Rouge being an agent of GUN, I prefer her being a special agent from the president/gov, that even GUN don't really know what she does, like in SA2. IDK I think it suit more how she is unique.

###

For me it's not really character developpement, it's just a change. Because this "evolution" doesn't really evolve from his past history.

With the previous games, he have no reason to see GUN as reformed (they still do weapon experiment in Shadow, they still can be sketchy as hell in SA2, and even if the Commander apologize, the GUN organization still wasn't able to stop him basically make everything worse by having his own vendetta). Yes, they have helped to save the world in Shadow. At least they do their basic job I suppose ?

It's not that he *forgive* them that annoy me. He could forgive them and move on, without actively being part of it, without showing any trace of questionning what happened. Same, in Battle, we see him question his statuse as a living weapon, which is another issue that this entirely elude.

​

Some good stuff can be good with that (Flynn handled that great, even if his Shadow speak way too much, and suffered a bit of that by moment like in the end of the Master Emerald Arc), but I feel that it hinder the character, and miss the point about most of his big question, by giving the wrong answer to them.

That's why I really prefer SA2's and Battle's Shadow, and then the Sonic Channel one, especially with his motivation : "Have you seen what people do to other people" is really an interesting thing to make Shadow says.

But for me "Agent of GUN!Shadow" is kinda a bad idea.

###

To be fair, for me Shadow being part of GUN is kinda the same issue of negating what Shadow was than the current one can have (a bit less annoying, ofc). It's especially jarring compared when we compare with its story in Battle (but TBH, Shadow did the same thing of negating the interesting points of Battle).

Shadow works way better with a tad of loneliness, doing his own things and appearing on his own terms. The issue is more that they should be more subtle with him, a bit like in the Sonic Channel story (that is basically Shadow's best writing since SA2 and Battle), but removing him from GUN, and making Team Dark less formal also allow those character to breath more, and avoid the tendencie to being formulaic that Sonic have.

( A bit of the same for Rouge is true : she work better as an agent of the governement that isn't under GUN orders (like it's implied in SA2), as it makes her being able to be in conflict with them, and annoy them in more interesting ways)


##

I especially found interesting that he needed some help to "get to the light" in full tunnel-vision, a bit like what was Maria's Wishes. It show a not-linear path, and that sometimes you'll still need help to do the right thing (because often, progression isn't linear, and it's nice that some stories show that, even by accident).

And I liked that when Rouge made clear that she was not forcing him and that it was his own choice, it immediatly snapped him out of tunnel vision to help everybody. She didn't manipulate him like she often does with people, she simply gave him the choice. It shows that sensibilities that IDWSonic have about boundaries, and that just forcing people doesn't work (it's interesting because it's not even like Maria's Wishes, where Shadow was more coerced as it was a dying whish, here it's just "you're free to act, but you are responsible of your actions", which is interesting).

It makes him for me more "real" it give me more empathy for him as I see in him the kind of "how fuck" when you need someone to snap out of something stupid. A bit like he insist in not talking about what he feels or think really, I know so much people like that, that doesn't show anything because of either misplaced pride, either lack of trust with other or either a trauma (and Shadow is all that), that it makes me have a lot of empathy for him, and love that dumbass (I said that with all my affection xD) edgy tsundere. It remind me a lot of what he was in Sonic X, and a bit in Battle (where he keep being injured :')).

( That's why for me, Chaos Races and Badnik Bases might have got the most interesting Shadow since Sonic Battle, and I'm really impatient to see of how Evan Stanley will continue to characterize Shadow within the framework of "stoic and doesn't show what he think/feel", especially as she seems to mean making him evolve within the "stoic and doesn't really talk about what he thinks" framework. )

##

He is basically way more tsundere-like : he doesn't show his emotions, and he have contradictions between his words (pretty harsh and jerky, kinda rude, doesn't talk to people) and actions (listening to Sonic about Eggman/Tinker, getting with a truck to evacuate people, saving Sonic several times, saving all the Chao in the latest arc) and showing a softer side when Sonic is not watching, or having several moments of "I didn't do it for you" like in SA2.

Evan Stanley (new main writer of the IDW comics) described him : SEGA want a strong emphasis on his independance and stoicism, with some room to do depper characterisation (what she did with the shadow chao). But he doesn't show his emotions (and won't say what he think or feel), which makes him a bit harder to write deeper because either you have to ressort to showing his thought (like in the latest issues) or to really show the differences between his actions and words (when most other character, they focus on their words for that).

He also have a way bigger ego, basically they gave more importance to all the small phrases he says during gameplay (behold my ultimate power, or when he boast about being the ultimate life form). Now this "title" seems to have some meaning to him, and he seems to be more perfectionist (he doesn't handle well having messed up) and searching to attack the strongest threat in the area.

Like in Archie-Sonic he also have a characteristic "the character that think about the big pictures and is ready use means that other wouldn't like", like shown in the Tinker arc or that he is more interested by stopping a robot part traffic than repairing Omega.

TL;DR : he is an edgy kinda-jerky tsundere that try to hide his softer side and emotions, trying to save the whole world by eliminating the biggest threat because he is the ultimate life form. He really need some vacation (and maybe a therapist), too.

---

I'm a big fan of Flynn (and don't think he is as bad as the comment you responding say, especially aboud edgyness as he is able to write pretty different type of stories, and especially his Megaman were really good) that he might be one of the best Sonic writers, but not everything is "SEGA's fault", especially with Shadow.

SEGA are the cause of him being a jerk. But that was Flynn's stories that made him fail/make something wrong in every of his apparition.

- In the first story he is wrong to be a foil to Sonic, and because the story is centered on that (and honestly, Flynn did that a lot in the past)

- In the NMS arc, we even got the cliché of "oopsie doopsie the rules have changed" to make him wrong and be the cause of Master Overlord.

- And even if Shadow's send off was bad because of SEGA, that Flynn that decided to keep him out of its story (and honestly : him being too strong isn't really that problematic when Silver was kept) for the third time, which is annoying especially as Zombot Shadow wasn't really interesting.

In these three cases, Shadow didn't fail because SEGA mandated it, as the very story needed that. ( And to be fair, keeping Shadow mainly out of the main story isn't something really new with him. ) Again, I said that as a huge Flynn fan (it's just that as any writer, he isn't perfect and have his weak points, even if on other points, he is fantastic)

​

I agree that a big part of the problem is SEGA. But I feel that Flynn have been more creative with constraint before, and that he have a bit used Shadow only as a foil for the moment. I hope that his apparition in the next arc will give him the opportunity to do something useful, and not make him fail just because the story need that, and be mostly out of the story.

But I think that he have heard the remarks (and see the flamewar), so I think that the few next issues he will write during Evan's time as the main writer might be better if they have Shadow in it. SEGA's limitation would be still there (and still bad), but I feel that he would at least make something a bit better with them this time. And I'm pretty curious to see how Evan Stanley will work on that.

##

Well, I find pretty logical that he antagonize that much Sonic in this page, ESPECIALLY the "you deserved it" : Sonic just came talking with a bad joke to someone that would OF COURSE be annoyed at him for being in his eyes responsible of a world-destroying. And the "I'm dealing with it too, I'm infected", when he is able to run it off… yeah Sonic, act like you are one of the victims when you are the only one that can survive this shit. I can totally understand why Shadow's anger should be legit.

Sure, saying to someone that they "deserved" an infection by a deadly virus isn't nice. But it's something that someone angry could totally say in this context, and I can understand Shadow not being nice at all toward Sonic in this moment.

( I don't deny that Sonic suffer in this arc of course, and I know that he was trying to hide also his guilt. And we shoudln't act like if being infected even with being able to run it off is nothing. It's just that this asshole-ish attack of Shadow was kinda understandable in this context. )

(the "cowards run. I win" is kinda derpy though, even if it's kinda in-line but exagerated with his SA2-Battle personnality, basically before 2006 showed him just 100% serious and nearly emotionless).


##

I honestly don't really like 06 Shadow, and would prefer something more between Battle Shadow and current Shadow. Shadow works better form me if he is a bit of a jerk (while in 06 he is too much of a "perfect tragic figure") - just he would need a bit more of a "hidden heart of gold" in stuff like IDW.

I really feel that the "Sonic Channel Shadow" we got a few month ago was the best incarnation of Shadow. Annoyed by Sonic, a bit competitive and jerk-y, thinking that Sonic reaching out was him mocking him (which is a simple scene that add a lot of depth to the character), but with a good handling of his "mysterious aura" and still affected by the tragic events of its life. And with maybe the best reason of why he fight : "have you seen what people do to other".

##

It depends which one, for me :

- Forces : He was a tad harsh against Infinite, but the rest was fine, and Team Dark interaction where fun.

- The Tinker Battle : He was good ! Its motivation where clear, and his debate with Sonic where fine.

- Battle for Angel Island : I just dislike that he got a bit stolen. I mean, his action saved Sonic & Knux, but where shown as "bad" (and not just having "bad consequences", especially as defeating Master Overlord seemed easier than Super Neo Metal Sonic) only because of an info nobody had, so it felt kinda cheap.

- The IDW19 wasn't something I like. I feel that him being angry against Sonic (and even the "you deserve it") was kinda understandable - even if harsh. The reason for making him a zombot where somewhat convoluted (it would have been more impactfull if everybody thought he was immune, due to resisting to it a bit). It would have been interesting if before the issue, Shadow had already attacked some without having any symptoms, and that it was either slow, either needed stronger exposition. Zombot!Shadow was kinda disappointing too, I hoped for him to be a strong threat.

- End of MV : Having him reflecting and nearly thinking that him being the "ultimate life form" means he don't have the right to makes mistakes have interesting potential (it's a bit Vegeta-ish, but could work well) ! I liked the attack on Eggman Shuttle and the vs Metal Sonic

- Chao Races & Badnik Bases : I loved unironically him here. His interaction with Sonic were gold, him going out of his mission only to save all the chao was really nice, him being tunnel vision against Starline but Rouge being able to steer him in the right path was interesting, and I liked the tsundere-ish bits. The arc also show some difference between how he thinks and what he says, and that's interesting too. And the apple moment at the end was my jam. I'm really down for more Evan Stanley's shadow writing. (I can see why some people wouldn't love it tho)
