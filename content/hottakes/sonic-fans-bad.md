# Sonic fan bad

I'm in a lot of fandom, and I can say : most of them can be that in some way. When you have a lot of people liking something, and that thing will often have release that doesn't please the fandom, a small percent of the fandom will start being toxic with other people.

Each fandom have their own way of being toxic. But often, it all come to an effect of number. Imagine that 0,1% of fans are toxic. We have 115k people on this subreddit, so it would mean 115 person here in this subreddit history that have been like that. And as toxic people are often waaaay louder than other people, it create a lot of noise.

But all in all... I've seen shit in nearly all fandoms. And I've seen nice stuff too. But always, we remember more negative shit than positive stuff. Be careful to the negativity bias ( https://en.wikipedia.org/wiki/Negativity_bias ).

​

And TBH… Look at the IRL world. War, assault, bigotry, etc. Toxic behaviour are everywhere. Of course, a lot of people are nice, but we remember and see more the 5 persons acting like shithead more than people that act normal. A bit like in the television you'll see way more what is "bad" than what is good.

##

 I agree on that point, we weren't united at all during that time.

I started really going on Sonic forum around 2005 and a bit before, and the fanbase wasn't united xD And the Classic/Adventure divide was especially strong and honestly I've seen during this era more problematic behaviour than now. I feel that nowadays there is a bit less of elitism in the Classic/Modern era, while in the forum I knew, I kinda remember several members being full-on elitist on that point. Sure, it wasn't "everybody", like always, but it was enough to ruin a bit the mood for some people (and I've got friend that left the fanbase because of that, during this era). And even not going to that kind of territory, there were many people that were diappointed with the 3D games and thought that the franchise needed a Sonic 4 to return to its root (and that's where come the "back to its root" tendencies that the franchise got). It was then amplified by Unleashed with the boost gameplay that pleased some, but was hated by some other, but even before, there where a lot of that.

During the first few time of the Adventure era though, it wasn't as bad, the separation wasn't totally made (I kinda remember a lot of mixt between Classic and Modern stuff in the really early 00's fangames), but it really arrived fast.

The separation isn't a problem thought, it's mostly when the behavior become too much problematic for something that is just a game. And I think that we got better on that point : there is still a divide, but I've seen less people attacking other about that. And that's a really good thing. I really don't want people to prefer one thing or another to feel bad or attacked because of that post : there is nothing wrong with prefering something to the other. What was problematic was that sometimes during this era, there were some people that added to that a lot of elitism and attack toward people that had different tastes.

And that's not even talking about another divide that existed way before that and that continued for a long time : the "SEGASonic vs American Sonic".

You can even see some trace of it, with people getting angry when Robotnik was being called Eggman, or when Sonic X arrived many fan of SatAM where especially angry against it (and calling it and even sometimes the game - often sans SA2 though - "kiddy") and its fans, and some of them went especially angry when Flynn started to make the first Archie continuity (but here we are going post-2006) more like the games. On the other side, there have been a lot of over-hartred of SEGASonic fans (sometimes with a biiiit too much of pride of being fans of the "real Sonic") toward everything not japanese-made.

As again, the problem wasn't that people didn't agree, it was that it was full of stupid debate that went on for hours and just made everybody angry for nothing xD

But all that doesn't mean that it wasn't a good time on other aspect. 