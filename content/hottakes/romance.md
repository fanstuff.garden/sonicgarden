Because different people like different kind of story, it's as simple as that. Some people likes the idea of Sonic in couple, some other do not. Not every character in fiction have to want a relationship, or anything like that. It can be that they aren't interested in these story (or see them as being already too much everywhere).

And for some people, they see Sonic as working better without romance. The "tying it down" is mostly related with the idea that Sonic doesn't need to be interested by that, and they see that focusing him on adventure, fun at life, and his "I do what I have to do" philosophy makes him more compelling. (and most aren't interested in seeing Sonic in couple, due to how it would remove focus in what they see interesting in him). Some also the idea of having a character they can see as aro-ace (even if it's not stated canonically) which isn't written with boring annoying cliché, but as a fun-loving social character which is friend with nearly everybody and that is simply cool.

( Also, you don't need to prefer loneliness to not be interested by romantic relationship. )

Now of course, the final say will be with what the writers says. Maybe they'll add open romance. Maybe they won't and will be still on the "we don't talk about romance here".

( For the moment, the official memo on official content seems to be that Sonic see Amy as a "really good friend", outside some "word of god", but it might change depending of what SEGA want for their IP. )

​

    Which is a criticism I NEVER heard when he was written as dating Sally

I've seen that A LOT of time XD

"Too much romance" is one of the biggest criticism of Archie I've seen (with "weird Penders writing"), and Sally "tying too much Sonic down" was also a common discourse, especially around "The Slap", or by seeing him in Mobius 25 YL.

Cricism of romance was really one of the biggest criticism of Archie before it became focused about Penders XD

​

Also, all the bullshit fights between a toxic portion of Sonamy and Sonally fans REALLY made a lot of people simply dislike romance with Sonic xD Death threat and stuff like that for fictionnal couple kinda ruin it for everybody.

##

Flynn stopped doing SonicXSally in the comics because of the toxic fans fighting each other (and him) over which ship is the best, and not hesitating to insult other in vile manner because of that. How you reacted to the previous poster simply saying they like Rouge x Shadow was exactly the kind of behaviour that made romance well less proeminent in the only media that really had them.

Insults and actions like yours simply suck all the fun from shipping and romance in the series. Because knowing that the subject will just attract some stupid flaming and downright vile insults like that simply makes it not worth it.

