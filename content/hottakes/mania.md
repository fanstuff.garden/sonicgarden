## A disclaimer about the "SEGA should hire these fans"

If you want to makes comments on a fangame about how SEGA should hire them… Please don't do that. And please, please, **FUCKING PLEASE** don't annoy SEGA by sending them "LOOK THIS FANGAME IS BETTER FIRE ALL YOUR PEOPLE AND HIRE THEM".

- First, well, it's useless. It won't help the game become better, it won't suddenly make SEGA says "oh wow we didn't know that using more money to delay again an already delayed game will make it better, thanks for this incredible insight". They *know* rushing a game isn't a good idea for the quality of the game. And certainly a lot of people at SEGA and in the Sonic Team would prefer to have more time to release finished game. Sadly, we won't stop the rush by saying "don't rush". So it's not really usefull feedback, as it's noise.

- Secondly, it's stressfull for fancreator. First, we kinda all fear that some day SEGA will stop letting us do our things. And even if it's not a random noise that'll change that, I'm not sure I wouldn't be kinda anxious thinking "maybe it's because of my project that it'll happens" if I were in the creator place when something like that happens. And even like that, when our praise is just "it's better than the shit SEGA did", it's kinda stress-inducing, because we might get dunked on by doing something that "fans" don't like and it create a bigger injonction to be good (as we know are supposed to "fix Sonic" for some) for something that is just an hobby project. I'm not saying that everybody will be anxious about all that. Just that we should be careful about that.

- Thirdly, you're just annoying some PR person that can't really do anything about all that. Which make you looks like that person that yell "I WANT TO TALK TO THE MANAGER" at one employee that have nothing to do with their issue. I'm pretty sure that's not what you want, but it's kinda similar things.

You can be angry at Blind Squirrel Game and even at SEGA (they have to manage their products) for the state of SCU (just please don't attack "the devs" you often don't know the condition of working of devs - and some rumors seems to show that at BSG it's kinda difficult). You can think that P-06 is an excellent remaster (and it certainly do a fantastic job) too, and even that it's one of the best remaster.

Just be careful to devs, and that not attacking people that have nothing to do with the issue. If you want to rant, be sure to not attack people that haven't done anything to you, if you want to provide feedback search to proper mean to do it (or help project that will be positive for Sonic fans, like explaing why you think a fan creation is good, or some exemple of what you liked in Sonic, etc). And please make sure if you're not putting too much pressure on some fandevs.

I know I'm a bit preachy there, and i'm not saying that everybody that does that are bad persons or anything. Just I'm saying that being careful would be nice for everybody.

( Also, a lot of people working on Sonic were originally fans. )

### About Sonic Mania

> But that one time, Sonic hired fans TheTaxman and Stealth to create Sonic Mania and it was the best Sonic game, so ur wrong and they should hire the fans
>
> - A real argument I have to deal with a lot

The Mania team got to make their own game because they weren't just hired to make Sonic game, and proposed a pro-quality project to SEGA. What happend was that :

- Christian Whitehead and Stealth came to SEGA with handled, good project that they knew how to put and the specific knowledge usable for that.

- They were able to put on a complete working engine from scratch and reusable tech (something than most fangame creator aren't able to do or just never did) making that the game WAS publishable by SEGA without much burden (like having to pay Unity/Unreal).

- They came back with a small-scale project with a complete team, a working prototype and Iizuka got hyped and scaled up the project.

All that is completely different than just a fan making a good fangame being hired ! Christian Whitehead and Stealth aren't just "Sonic fans that made good fangames/hack", they had when the Mania project started professionnal experience, and where able to go see SEGA with a working and interesting project.
