
# Archie/IDW

I really love both of them. And even the three of them if we count pre-252 Flynn stuff

    Pre-reboot Flynn Era was wild, with really fantastic rework of the old Archie lore, and with some of the best storyline written for Sonic (for me Silver Saga or the Iron Dominion are simply a few of the best Sonic stories).

    Post-reboot Archie was really solid, and I really liked its universe. It had basically what I would call the perfect Sonic universe depiction, by using the whole game universe with drops of custom elements, SatAM and AoStH. It was intriguing and really funs.

    IDWSonic benefits a lot of having less character and focusing on Modern Sonic : they're were more present and memorable to me. Stanley arriving post-Metal Virus also helped it getting some fresh her, as she interpret differently the character (her Amy, her Sonic and even her Shadow are a few of my favourites). The Metal Virus was also a really great storyline, and it was really fantastic to see something original like that.

So I've got trouble to really say which one is the best, as it'll depend on my mood xD When I'm full lore it'll be more archie post-reboot, when I want really fun character it'll be more IDW, when I want epic drama it'll be Archie...

( I can't talk about Fleetway, I disliked what I read so I didn't read everything, so I don't know enough to have an opinion, especially as some people says that some of the things that I dislike bring interesting stuff. )

---

If I say it's "The Game Canon, but extended with element from other canon" or a "fusion between older timeline and the game canon", it's kinda obvious that it means that it's not canon to the game, but most of the game that are canon to it. ^^

I even called it a Sonic Universe, so I think I made enough elements without having to add a giant disclaimer :p

##

 like both, for different reason :

- IDW seems like the exact spot I like with Sonic. Can be fun and lighthearted and dark when needed. Most character are spot on, the B6 and Gemerl are here, and the universe manage to be fun without being too overwhelming. And I love the character design, they are fewer but more interesting IMO (even some background characters are excellent, and I want a name for the Orang-Utan xD).

- Archie was WILD. The reboot was the biggest rationalisation of Sonic's universe, it was fun and unique, a gigantic world. It had some pretty epic storyline too, pre-reboot. I'm not a big fans of stuff pre-160 (except some Karl Bollers storyline), but after it's really good. The problem is mostly that it was really reserved to the most hardcore fan and was sometimes really messy.

So for me, both are really good. I would say that Archie have the adventage of being wild, IDW have the adventage of being accessible.


Canonicity in Sonic games is more fluid than we tend to think in fandoms, they are a lot of stuff that can be "canon in some sort".