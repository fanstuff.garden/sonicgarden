# Mandates

## Message 1 

Sonic can't loose "in the end". He can suffer temporary loss, but will have to win at one point. TBH maybe that SEGA also didn't want a Sonic zombot, but I don't feel that having a Sonic zombot would have helped the pacing either (and tbh, I don't feel that the pacing of the Metal Virus was really that bad : it's mostly for me the delays that caused issues, when reading in a row the issue, it works pretty well).

The Shadow mandates are... more complexe. Especially as we got recently yet another interpretation of Shadow that seems a bit IDW-compatible and game-compatible at the same time with the Sonic Channel Shadow : he is more gruff and less open like IDWShadow, even a bit jerky, but we can read his though process and is more than just an edgy hedgy… he also have something quite knew, misunderstanding Sonic's gesture of empathy as mockery, which is something I don't remember Shadow doing, but that is kinda intereting. And that have some new way of formulating his.

Sooo it's really difficult to know exactly what SEGA/The Sonic Team want for Shadow. Does it have changed since when we learned about them, maybe due to the reactions ? Is there some miscommunication between the teams ? Is it mostly an effect that Shadow doesn't say what he think exactly, and that we would need more of his thought process about things to have the "real version" ? Is it simply differences of vision about how is the character ? Do SEGA thinks that different publics will like a different Shadow ?

The only thing we can suppose is that it's seemingly not simply a story of "different people" : the review of the comics and the Sonic Channel story seems to be done by the same person, Eitaro Toyoda.

## Message 2

As I'm not the person you responded too, I don't have to answer your whole post, as I'm not necesserally saying that it's a "mandates", as I don't really like that word (as Flynn himself, that prefer to talk about guidelines/or just that SEGA asked for something since several years already).

But I suppose it all depends of what we calls the "mandates". Here, the classic/modern split is a guidelines of SEGA that affect both the game and the comics. So I suppose that for some it's a "mandates". So if we take mandates as that, yep, some rules from the comics also affect the games.

( But most of the time, fans doesn't understand what the mandates are, like with the "Sonic can't loose" one where they loose their mind over a quite common rules, even if it have been explained again and again what it is. But eh. Fandoms are like that, people are often young, and plenty. )

## Message 3

There is no "list" of mandate, because it's not a set list found in Mount Sinaï. It's just that like every company, they have rules about how their character have to be shown. Most of the things we see as "mandates" are just SEGA's that weren't really pleased with something or that asked for something to be more "on brand".

## Messages 4

Why would he put stuff just to make his own life more difficult ? XD And what mandates would be from Flynn ? Because it seems more conspiracy theory than anything without any proof or things that go in that direction really (or just "look at it the mandates can change"… which isn't surprising). Especially as several people have said that yes, SEGA have guidelines, and demand things.

The "mandates" are mostly SEGA saying their guidelines, and nearly all of them are quite commons, to be frank. It's mostly us saying "it's a M A N D A T E" everytime that SEGA "didn't want" something.

## Messages 5

Well, even if there is mandates on how he must present some things, they aren't "keeping a tight leash on what or can't happen". There are thing he can't do, but he also have some freedom on what he can do.

Some are frustrating for us, like that the "on character" of Shadow for them is out of character for us. But for instance, if they saw Shadow in a way that we like, we would like that they forbid for instance to show Shadow (and basically every character) in OoC ways. The issue is more that their vision of Shadow isn't what we want from Shadow, so we see the mandate as "too strict".

## Messages 6

Not really. A lot of them existed before the legal bullshit. For instance Sonic had to be modified in Sonic 25YL (the Flynn one) to "cry less". Omega and Cream where forbidden to appear at one time, etc. A lot of what we call the "mandates" are simply SEGA taking more care about the comics with time.

Penders mostly "helped" remove the "legacy argument" that made some of them less present. Take for instance the parent one, even before when the "Pendercalpyse", Flynn had to tell less "in the script" that they were the official character parents (a bit like Uncle Chuck could post-reboot still be there, but not be called Sonic's uncle). Same for the "Sonic can't loose". Ian Flynn talked about it for Sonic 175, which happenned before all those case really begun.

Flynn himself said several time that "mandates" where basically just guidelines/rules from SEGA that came and go a lot, and a lot of them existed before.

Penders did a lot of harm, but more "localized" (to mostly Chronicles, old Archie and his own career). About mandates, I'll agree one thing tho : that Penders gave them is that it removed the reason of "it always have been like that" (with the reboot) which removed some compromise (like having the parents and not calling them parents, even if Chuck survived).

( A bit like Archie loosing the licence did the same in making Modern-only the IDW Comics, as it created a new relationship between comics and the ST )

## Messages 7

Well, the "male hedgehog mandate" I feel isn't a strong rule, more an effect of only Sonic/Shadow/Silver can do it. It's mostly some kind of "only the biggest most powerfull character do it", and it was phrased weirdly to englobe only them and avoid someone saying "but maybe <other character>" could do it. But I agree that they should add at least a female character able to do it (Amy or a new "S-tiers" character). Basically it's for me some kind of weird description of the status-quo by trying to make it looks like a rule. Which doesn't mean it wasn't a bad idea to describe it like that.

About economical/political ramification, it's a complex matter, I honnestly don't know what I would do in their place XD (especially as I tend to add a bit of political fiction in all my writings).

## Messages 8

Penders and the mandates are two different question. Most of them existed before Penders.

Penders is shit, but his influence was mostly on loosing Chronicles and the pre-reboot stuff.

## Messages 9

The mandates are for the whole IP, they're simply the Sonic Team rules. The mandates are basically "make the comics according to our vision of the IP".

## Messages 10

Well, mentionning/referencing money isn't really forbidden. Latest IDW issues is basically. They don't talk directly about the exact "money/currency" (basically it doesn't answer with what people buy stuff), but talks a lot about money and stuff. Basically even without talking about the actual currency, money is kinda still present.

( In spoiler for people that haven't read it :The chaotix talk several times about the money (and the potential reward/payment), and all the situation of Clutch is how he lost his property and wealth and he is searching for people to buy his stuff. )

## Messages 11

Honestly, most aren't that strict (there are a few "strict" one, but most are kinda common in the world of executives mandates), it's what is kinda weird with SEGA. Because many of their mandates have some workaround, which participate to the issue IMO.

The two world is the most difficult to know, because it was never shown where it lasted (it's believed to be gone from now, as Ian said to "not worry about it", and than lately the Sonic Channel stories seems to retcon the two-world). According to Flynn, the two world is from Naka's era, so it's difficult to really know what it means. But this one isn't a "mandates" : it was their lore, for them.

So it's difficult to know if it's related to how much of Sonic X have been reused in the last few years, related to what people make what in the Sonic Team/change of power.

Now about the mandates one, it might also be that they didn't even send an answer that was meant to be "what is true for the universe", they might have simply told that as a way to say "money isn't a subject of the Sonic the Hedgehog IP". But branding could in fact be a part of issues : there is a long unresolved issue in Sonic with the rings, in what they are in-universe.

( And then there is Shadow, which is the most complicated, because it's difficult to know even what the Sonic Team wants with him, because there are contradicary elements on that one, between the comics, the Sonic Channel works, etc )

## Messages 12

In a later bumblekast (would have to find exactly which) he rectified by saying that it was mostly allowed only in the context on some character (The Chaotix, for instance). And the latest IDW talk about money several times, even in the latest issues where there are talk about the rent and stuff.

Either it have changed or either it was somewhat non-exact. It's mostly just that currency and stuff aren't talked about (certainly because they don't want to commit to a specific kind of currency). Seems to be mostly than they don't really want to talk about realistic power structures in Sonic.

IMO, it would be more efficient to really talk about the mandates/"what SEGA ask" without jumping into every rabbit hole, because basically going toward every rumors and stuff simply make weaker its criticism.

##

Long post incoming.

For most cited in wikis, no they didn't. Penders affair put a lot of stuff, but there is a lot of confusion about everything. The "mandates" aren't an homogenous set of rules, and weren't all mades for the same reason. And in wikis, they have a LOT of confusion about the matters. Especially as some wikis did a pretty bad job about the matter.

The rules that we know that are for sure related to Penders is the "no Penders character rules". It also certainly influenced Chronicles treatment, even if the game failure and problems with Bioware certainly didn't help.

But many aren't related to this whole affair.

    The "parent" one is one of them, in that it have started in some sort before that, as Flynn had progressively to talk less and less directly about how they were "parents", even when Penders character where allowed to appear. A big instance of that is when we see Espio's mother/master, where he only use some sort of hesitation to show us the information. This rules is more some sort of "licencing"/"coherence" rule more than anything, and have started before Penders. Basically, what the Penders affair allowed is that as the reboot happenned, they basically screw most of the current parent character and didn't have to think about that.

    Same for the "Metal Sonic" one, as Flynn already says that he had to call Shard (which is derivated from a Penders character) a "Mecha Sonic" to work around the mandate. This one is a good exemple, as Shard was scrapped after that because of Pender's whole affair. Basically, think that every mandates that existed BEFORE that they removed Penders character isn't made to protect themselves from Penders, as the first step to protect themselves from Penders was.

    An even bigger exemple is "Sonic is not allowed to lose". The biggest instance where Flynn had to work with it was for Archie Sonic 175. In 2007. Two years before the first instance of the whole process. Same for the "strong emotion" that was also shown before this whole affair.

Some stuff on the list aren't even really mandates :

    The memory stuff for instance wasn't really asked for SEGA (they didn't care about that), it's the editor that wanted them to exists to create a link between the two continuity, while Flynn prefered a fresh start as no plot point of the first continuity to be. So one of the "mandates" isn't even a mandates.

TL;DR: Yes, some stuff have been removed because of Penders, mainly his characters and Chronicles. But the "mandates" are a collection of rules that not all have the same source, and most shown in the wiki page about that aren't related to the whole Penders affair, as they existed before and where part of simply how SEGA see their own brand.

##

This mandates is NOT related to the lawsuit. Familial bond where less showed even before the reboot (with a lot of "we show it but don't say it outright"), and it's just that the reboot made that they didn't have to show them anymore (they made an exception to Uncle Chuck with the "everybody call him like that").

The idea isn't that Sonic can't have parents, it's that "if someone have to create parents for Sonic, it'll be us (SEGA)". Basically, the idea is that if someone else create familly bond, it'll create some annoyance laters (as you can't create hundred of parents for Sonic and have all of them).

Basically, for the moment, they see that Sonic doesn't need parents (and to be fair, I kinda understand why, as Sonic as a character is often more "symbolic" than "grounded of reality", which made the strenght of Maekawa's story for instance), but they want to be able to create them without annoyance later if they want.

##

The wiki page is kinda wrong on a lot of points.

For instance a lot of them are from BEFORE the lawsuit (for instance the Nega, emotion or family ones… or Sonic characters not being able to be killed off). And saying "some rules might have been here before" after that doesn't negate that, and is bad article writing.

Some other are not mandates from SEGA : characters from the older universe not appearing isn't a mandate from SEGA, it's most certainly a demand from ACP. Same from the memories, it's actually the opposite ! Ian Flynn didn't want them (not wanting to promise stuff, he talked about that on his old forum), but his editor wanted them to ease the transition. So removing the memories weren't an editorial mandate from SEGA, having them was an editorial mandates from his editor.

##

There are "evidence" that :

    have been refuted/criticized by mostly EVERY other people that worked on Sonic says (for instance that yes, Sonic have rules of what they can and can't do)

    take the extreme vision of the "mandates" to better said "they are false lol" (for instance by mentioning that they change, while Flynn explained that it's not immuable mandates, but mostly rules related to what they want at a time), which is kinda a straw man argument.

    Are mostly the result of a small group of people that do that because if they believe that a single man is the cause of the loss of what they liked (the FF) the solution would be simple. A common bias.

Most of the video that talk about that show a gigantic confirmation bias, explaining things with the already made conclusion of "Ian Flynn is evil" THEN saying that it confirm than he is evil and conspiracing against the "American Canon".

So nope, not convinced. ( Not convinced that SEGA is evil for having brands demand too )


##

He have made a lot of dumb stuff, but he isn't the reason of that. He started copyrighting his character in 2009, and Colors was certainly already in development at that moment. He started his lawsuit against SEGA only in 2011.

So basically, they had already started going to more humorous stories at that time (and they tried having a serious story in Forces even if they failed, which mean imo that it's not related).

I agree that there are a lot of problem related to Penders, though. But he doesn't have that much effect nor importance.

##

I love IDW and I think that this is one of the greatest moment of "Sonic being of the verge of collapsing", but I feel this isn't totally exact.

They only forbidden him to cry, but he was allowed to get a lot of strong emotion. In the Metal Sally arc (after Antoine's coma and Bunnie's de-roboticization), he was allowed to be on the verge of abandonning, to Amy and Tails IIRC making him. He was also WAY harsher than usal with Silver when he proposed that Antoine might be the traitor (well, it was really understandable). They even made him remember the old line of Scourge "just one bad day and you could be just like me".

And in the second continuity they even allowed him to be corrupted by Dark Gaïa, while the game stated that it was impossible xD. They also showed him trying to hide the problem the whole time, making everything worse.

​

So I don't think this rule have changed : he can have strong negative emotion and weakness, but can't show outburst of these emotions because it would be seen as OoC by SEGA. So Flynn can make that work - and did several time, by being a bit more subtle about it. Here, I think that the impression that this is stronger than usual is because it's the whole focus of the arc. The arc focus mostly on Sonic trying to save everyone, and each issue show the strain to him. And as the whole arc is saying "All this are a consequence of Sonic saving Eggman", they show even more how this affect him (and the Mecha Sally arc, the last "Sonic saved Eggman and suffered the price of it" from Flynn didn't really went in this area... Which is especially strange as Archie!Eggman is far worse than Game!Eggman. Though they HAD locked him, while in this arc they didn't even had left people to monitor him, while they had an organization that could have done the job - the resistance/restauration).