I didn't say that they wrote the story. But that they ADAPTED the script and that the script is a big part of what Sonic Forces is. Moreover, I only said that Eitaro Toyoda wasn't responsible for the story as he only wrote the japanese final script, not the story.

Since Colors, cutscene are tailored for the English Script, not the Japanese Script, which is visible for instance with how the mouth movement adapt to the script. Basically : the English version is the original version since Colors. Which mean that the English script must be considered finished enough to work.

In the case of Forces, here is what Nakamura said about that

Fast Google Translation about that :

    This time, for the story, I made the story elements and flow in Japan, wrote the whole thing firmly, and then rewritten the lines and spaces in the United States. The Japanese version is rewritten from there. It takes a lot of time, and it causes a lot of trouble for the event team ...

There is some problem about the translation (for instance it's not Nakamura that have written the english script), but we see the movement :

Initial japanese story and script (the "story" motion in the credit) -> English script (English Story in the credit), rewritten by Pontaff -> The final japanese script, written by Eitaro Toyoda

That's why you have Makoto Goya that is credited for the story and Toyoda that is credited to the "japanese story".

I'm not saying that Tails being afraid isn't related to the original japanese story (written by Nakamura and Goya), nor that they have choosen story elements... But that Eitaro Toyoda isn't the writer of the Sonic Forces story. He made the final japanese rewriting (for instance he removed some stuff like the "Sonic have been tortured for month", toned done some jokes like the transpiration smell joke, etc).
