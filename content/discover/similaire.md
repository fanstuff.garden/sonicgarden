---
layout: layouts/base.njk
eleventyNavigation:
  key: Jeux similaires
  parent: Autour de Sonic
  order: 100
---

# Jeux similaires à Sonic

Le but de cette page est de présenter et de rendre plus connus quelques jeux similaires à Sonic, afin de permettre aux gens d'avoir d'autres expériences proches si iels sont intéressés. Les jeux présentés ici ne sont pas forcément complètement des Sonic avec un autre skins, mais sont des jeux ayant pour effet de ramener quelques aspects du fun qu'on a dans un Sonic. Ces jeux viennent souvent de la communauté Sonic, et montrent l'influence qu'à eu la série sur le monde du jeu vidéo.

## Freedom Planet

Freedom Planet est une série d'action-plateformer rapide, ayant été à l'origine des fangames Sonic. Ils se différencient des Sonic par un système de combat plus poussé (et globalement une plus grande complexité). Parmis les aspects intéressant, l'univers d'Avalice est un univers fortement inspiré de la chine, et les personnages y sont haut en couleurs.

- [Freedom Planet](https://store.steampowered.com/app/248310/Freedom_Planet/)
- [Freedom Planet 2](https://store.steampowered.com/app/595500/Freedom_Planet_2/)

## Spark the Electric Jester

La série Spark the Electric Jester est une série de jeux vidéo de plateforme à haute vitesse, créé par Lakefeperd, connu pour être le créateur de la série des Before/After the Sequel ainsi que Sonic Chrono Adventure. La série à commencé avec un titre 2D, puis est passée en 3D à partir du second. Comme Freemdom Planet, il y a une plus grande emphase sur le combat, et à partir du 2, la série part dans une direction qui tire pas mal d'inspiration des premiers titres Sonic 3D (Adventure, Adveture 2, et un peu de Heroes dans le level design je trouve).

- [Spark the Eletric Jester](https://store.steampowered.com/app/601810/Spark_the_Electric_Jester/)
- [Spark the Eletric Jester 2](https://store.steampowered.com/app/1079210/Spark_the_Electric_Jester_2/)
- [Spark the Eletric Jester 3](https://store.steampowered.com/app/601810/Spark_the_Electric_Jester/)

## Jeux de Spicy Gyro Games

Le studio Spicy Gyro Games	à sorti deux jeu faisant penser à du Sonic dans ses mécaniques : Pollyroll et Panic Porcupine. Les deux sont des jeux de plateforme un peu die and retry (Meat Boy, etc) mélangé à du Sonic. Le combo marche plutôt bien, mais les jeux ne sont pas facile.

- [Pollyroll](https://store.steampowered.com/app/743850/Polyroll/)
- [Panic Porcupine](https://store.steampowered.com/app/1820570/Panic_Porcupine/)

## Autres jeux

- [Pizza Tower](https://store.steampowered.com/app/2231450/Pizza_Tower/) - Mélange un Wario-like avec le flair pour l'inertie d'un Sonic sur certains aspects, ce qui donne un mélange super fun ! Peut-être l'un de mes plateformer préféré de 2023. Apparu régulièrement à des SAGE.