---
layout: layouts/base.njk
eleventyNavigation:
  key: Autour de Sonic
  order: 1
---

# Autour de Sonic

La franchise Sonic est une franchise qui a eut beaucoup de contenus à travers les années. Il est souvent difficile d'en avoir véritablement une version simple. Le but de cette catégorie et de donner un petit aperçus d'éléments qui ont existé dans l'univers de Sonic, pour pouvoir s'intéresser aux différentes histoires qui ont été faites.

Cette catégorie permet aussi de découvrir quelques univers similaires.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Canon et fanon

Cette liste ne différencie pas trop ce qui est canon et fanon. Vous pouvez retrouvez plus sur ce qui est (à l'heure actuelle) considéré par canon par la Sonic Team dans la catégorie [Univers](/univers).

Cependant, même quand ces différents contenus ne sont pas canon, il est tout a fait possible d'imaginer à votre manière ce qui fait partie de votre fanon. Comme le dit souvent le compte officiel Sonic "Everything is Canon" : au fond, la notion de "canon" n'est qu'une notion inventée, le monde de Sonic n'existant pas (wow, impressionnant, qui l'aurait cru !).

Même sans être "canon", ces différents univers et personnages qui ont existé dans Sonic restent intéressant et des éléments sur lesquels vous pouvez vous appuyez pour construire vos histoires et tout.