---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Les jeux gba et après
  parent: Jouer aux jeux
  order: 3
---

# Les jeux gba et après

Aucun des jeux Sonic "moderne" console portable n'a encore été réédité. Votre meilleure option sera l'émulation. Il n'y a pas non plus d'amélioration des hacks, ce sera donc l'expérience normale de l'époque. Pour les jeux en 3D, vous pouvez augmenter la résolution interne du jeu via l'émulateur. Vous aurez besoin des émulateurs suivants pour les jeux suivants :

## Jeux GBA

Ces jeux peuvent être joué avec de très nombreux émulateurs. mGBA et différente variation de VBA (genre VBA-next) feront parfaitement l'affaire, et vous pourrez jouez à ces jeux sur la plupars des consoles à émulation. RetroArch contient de nombreux émulateurs seront efficace.

- Sonic Advance
- Sonic Advance 2 - Peut être amélioré via le hack [Sonic Advance 2 SP](https://www.youtube.com/watch?v=548JeUEAOCo) (Lien [romhacking.net](https://www.romhacking.net/hacks/8001/))
- Sonic Advance 3
- Sonic Pinball Party
- Sonic Battle

En plus de cela, vous pourrez jouez à une version standalone du Tiny Chaos Garden, qui était fourni avec Sonic Adventure DX, et qui contenait un mini-jeu de cache-cache chao.

## Jeux DS

Ces jeux peuvent être joué via melonDS ou DeSmuME. RetroArch contient également des core pour jouer à la DS, basé sur DeSmuME.

- Sonic Rush
- Sonic Rush Adventure
- Sonic Colors
- Sonic Chronicles

## Jeux PSP

Ces jeux peuvent être joué via PPSSPP, disponible sur Windows, MacOS, Linux, Android, etc. Il peut être utilisé comme faisant partie de RetroArch. Il est possible d'augmenter la résolution interne pour avoir les jeux de manière moins pixelisées.

- Sonic Rivals
- Sonic Rivals 2

## Jeux 3DS

Ces jeux peuvent être joué via Citra, disponible sur Windows, MacOS, Linux, Android, etc. Il est possible d'augmenter la résolution interne pour avoir les jeux de manière moins pixelisées.


- Sonic Generation (3DS)
- Sonic Lost World (3DS)
- Sonic Boom : Shattered Crystal
- Sonic Boom : Crystal and Ice