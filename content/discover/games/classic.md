---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Les jeux classiques
  parent: Jouer aux jeux
  order: 0
---

# Les jeux classiques

De nombreux jeux classiques sont disponibles en émulation ou d'autres moyens. Voici ce que je considère comme les meilleurs moyens de jouer à certains d'entre eux.

## La série originelle

Ces jeux sont disponibles sur **Sonic Origins**. Avec Sonic Origins Plus, ils ajouteront Amy comme jouable ! Ces portages ne sont pas parfaits (il y a quelques glitchs et autres), mais ils permettent de jouer aux jeux avec plus de fonctionnalités (drop dash, pas de vies, etc). Vous pouvez modder Sonic Origins avec [HedgeMod Manager](https://github.com/thesupersonic16/HedgeModManager/releases)

Vous pouvez restaurer les musiques de S3&K avec [ce mod](https://gamebanana.com/sounds/61823).

### Sonic 1 et Sonic 2

Une autre alternative pour jouer à Sonic 1 et Sonic 2 est d'extraire les fichiers de la version Android et de les jouer avec la [décompilation](https://github.com/Rubberduckycooly/Sonic-1-2-2013-Decompilation). Notez que vous pouvez également utiliser les fichiers de Sonic Origins, mais vous devrez faire [quelques manipulations](https://github.com/Rubberduckycooly/Sonic-1-2-2013-Decompilation/blob/main/FAQ.md#using-origins-rsdk-files) pour que cela fonctionne correctement. De nombreux mods sont désormais disponibles pour cette décompilation.

### Sonic CD

Une autre alternative pour jouer à Sonic CD est d'extraire les fichiers de la version Android (ou de la version PC originale si vous l'aviez) et de la jouer avec la [Décompilation](https://github.com/Rubberduckycooly/Sonic-CD-11-Decompilation). Notez que vous pouvez également utiliser les fichiers de Sonic Origins, mais vous devrez faire [quelques manipulations](https://github.com/Rubberduckycooly/Sonic-CD-11-Decompilation/blob/master/FAQ.md#using-origins-rsdk-files) pour que cela fonctionne correctement.

Le portage original de Sonic CD sur PC n'est plus disponible à la vente, et nécessite le [SEGA PC Reloaded](https://forums.sonicretro.org/index.php?threads/sega-pc-reloaded.34181/) pour fonctionner correctement ([miroir disponible ici](https://forums.sonicretro.org/index.php?threads/sega-pc-reloaded.34181/page-7#post-1003615)).

### Sonic 3&K

Pour Sonic 3&K vous avez deux autres options pour obtenir une "édition définitive" :
- Jouer avec une rom (que vous pouvez obtenir si vous avez la version steam de S3&K, ou par d'autres moyens :3) à [Sonic 3 A.I.R](https://sonic3air.org/).
- Jouer au hackrom [Sonic 3 Complete](https://www.s3complete.org/) si vous êtes sur une plateforme qui gère mieux l'émulation.

Sonic & Knuckles Collections n'est plus disponible à la vente, et nécessite le [SEGA PC Reloaded](https://forums.sonicretro.org/index.php?threads/sega-pc-reloaded.34181/) pour fonctionner correctement ([miroir disponible ici](https://forums.sonicretro.org/index.php?threads/sega-pc-reloaded.34181/page-7#post-1003615)).

## Autres jeux MD

Vous pouvez obtenir les jeux suivants dans [SEGA Mega Drive and Genesis Classics] (https://store.steampowered.com/app/34270/SEGA_Mega_Drive_and_Genesis_Classics/) et les extraire en les récupérant depuis le dossier `steamapps/common/Sega Classics/uncompressed ROMs/` dans votre installation Steam. À noter qu'elles utilise l'extension .68K, mais ce sont des roms md classiques.

- Sonic 3D Blast (SONIC3D_UE.68K)
- Sonic Spinball (SONICSPI_U.68K)
- Dr. Robotnik's Mean Bean Machine (ROBOTNIK_U.68K)

(Si vous l'aviez acheté avant Sonic Origins, vous aurez aussi Sonic 1, 2, 3&K)

### Non-disponibles légalement

Les jeux suivants ne sont pas disponibles et nécessitent malheureusement d'être récupéré illégalement pour être joués :

- Knuckles Chaotix
- Wacky Worlds Creativity Studio
- Sonic Eraser

Ils peuvent fonctionner sur n'importe quel émulateur mégadrive (et 32x pour Knuckles Chaotix)
