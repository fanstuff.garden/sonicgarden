---
layout: layouts/base-toppages.njk
eleventyNavigation:
  key: Jouer aux jeux
  parent: Autour de Sonic
  order: 1
---

# Jouer aux jeux

Cette page a pour but de vous donner quelques conseils pour jouer aux jeux Sonic de la meilleure façon possible. Elle vous donnera une liste de projets de décompilation, de fan-remake, de ports, de mods d'amélioration, etc. qui permettent de rendre certains jeux jouables ou même meilleurs, en particulier sur du matériel moderne. Ce guide indiquera également quels jeux ont ou n'ont pas de moyens légaux d'être accessibles aujourd'hui, et quand l'émulation ou le fan-remake donnent de meilleurs résultats. Il permettra également de découvrir des projets de restauration et d'amélioration.

Le but n'est pas d'obtenir la meilleurs façon d'y jouer, la façon ultime, ni la façon la plus *authentique*, mais plusieurs manière de pouvoir y jouer.

Ce guide est quelque peu subjectif, il s'agit donc d'une ressource parmi d'autres sur le sujet !

## Catégories

Pour cette section, j'ai décidé de diviser les jeux en différentes catégories :

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

Je sais que ça a tendance à créer beaucoup de débats, mais j'ai décidé d'utiliser la même division que la Sonic Team pour ce guide, avec une ère classique comprenant tout ce qui précède Sonic Adventure et une moderne comprenant tout les jeux à partir de Sonic Adventure.

## Autres guides

- [Un document sur comment jouer aux jeux sur Steam Deck](https://docs.google.com/document/d/1FsjXbyYQTIK9f9P_NYHoLGnv8_ZqHjgLPf9Qm0VMcVo/edit#)

## TODO

- Jeux arcades
- Jeux Saturn/PC
- Jeux mobiles