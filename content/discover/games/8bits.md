---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Les jeux 8 bits
  parent: Jouer aux jeux
  order: 1
---

# Les jeux Sonic 8bits

Sonic a eut sur Master System et Game Gear de nombreux jeu, autant des version alternatives de jeux 16 bits que des jeux totalement originaux. Si ces jeux sont souvent moins apprécié que ceux sorti sur consoles de salon (en partie du à leurs archaïsmes et lags), ils sont cependant des jeux intéressants à jouer qui peuvent permettre de passer un bon moment. 

Les titres Game Gear sont disponibles dans **Sonic Origins Plus**, mais pour l'instant aucun moyen de les extraires depuis ce jeu. A noter qu'il permet de jouer en multijoueur aux jeux le supportant.

## Extraires les roms

Les roms sont disponibles dans Sonic Adventure DX, et sont même présent dans la copie du jeu disponibles sur Steam. Vous pouvez restaurer l'accès au jeu à l'aide du [SADX Mod Installer](https://sadxmodinstaller.unreliable.network/). Ils peuvent également être extraits du jeu avec la [Game Extraction Toolbox](https://github.com/shawngmc/game-extraction-toolbox).

- Sonic the Hedgehog 1 (8 bit)
- Sonic the Hedgehog 2 (8 bit)
- Dr. Robotnik Mean Bean Machine (8 bit)
- Sonic Spinball (8 bit)
- Sonic Chaos / Sonic & Tails
- Sonic Triple Trouble / Sonic & Tails 2
- Sonic Labyrinth
- Tails Adventure
- Tails Skypatrol
- Sonic Drift
- Sonic Drift 2

## Version Master System

Pour certains jeux, vous pouvez préférer les versions Master System (disponible qu'en les récupérant sur internet ou depuis une cartouche Master System), qui ont une taille d'écran plus grande, et parfois quelques différences (de musique ou de détails dans le gameplay). Les jeux suivants ont été porté à l'origine sur Master System. Elles sont souvent considéré comme plus simple.

- Sonic the Hedgehog 1 (8 bit)
- Sonic the Hedgehog 2 (8 bit)
- Dr. Robotnik Mean Bean Machine (8 bit)
- Sonic Spinball (8 bit)
- Sonic Chaos / Sonic & Tails
- Sonic Blast (Cette version est sortie seulement en Amérique du Sud, et a été produite par TecToy)

Les jeux suivant ont également eut des portages non-officiels fait par des fans :

- [Sonic Triple Trouble](https://info.sonicretro.org/Sonic_Triple_Trouble_SMS)
- [Sonic Drift 2](https://info.sonicretro.org/Sonic_Drift_2_SMS)

## Améliorer l'expérience

Pour améliorer (sur RetroArch), l'expérience de jouer aux jeux 8bits, il y a quelques paramètres que vous pouvez modifier dans l'émulateur afin de pouvoir avoir un plus grand confort de jeu. Vous trouverez ses paramètres en lançant un jeu en émulation (pour l'émulateur **Genesis Plus GX**), appuyant sur `F1`, puis allant dans `Option du Coeur`. Ces options sont

- CPU Speed : Montez le à 150%, cela devrait supprimer les lags des jeux, tout en ayant pour seul soucis que j'ai remarqué une accélération du son "SEGA" au début, pour l'instant
- Remove Per-Line Sprite Limit : Passez le à `true` pour vous débarasser d'une partie des scintiellement

Vous pouvez aussi tentez de voir si le paramètre "Game Gear Extended Screen" fonctionne correctement, mais dans mes test je trouvais que cela avait trop d'inconvéniant (sprites n'apparaissant qu'au dernier moment, glitch) par rapport à utiliser l'écran Game Gear. Si vous voulez un écran plus large, utilisez plutôt les portages Master System.

## Fan-remakes

Pour Sonic Triple Trouble, vous pouvez aussi jouer au [remake 16 bits](https://www.sonicfangameshq.com/forums/showcase/sonic-triple-trouble-16-bit.1130/). Un projet de fan-remake est également en cours pour Tails Adventure.

