---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Les jeux modernes
  parent: Jouer aux jeux
  order: 2
---

# Les jeux modernes

Tous les jeux sortis après l'ère Saturn.

## Ère 128-bit

Les jeux Sonic sorti jusqu'à Sonic 2006.

### Sonic Adventure et Sonic Adventure 2

Pour ces jeux, la solution la plus simple (et pour moi la meilleure) est d'acquérir la version Steam et d'utiliser des mods pour améliorer l'expérience. Avec des mods, Sonic Adventure peut être rendu assez similaire aux versions Dreamcast.

- Pour Sonic Adventure, installez le [SADX Mod Installer](https://sadxmodinstaller.unreliable.network/) qui contient de nombreux moyen d'améliorer l'expérience de jeu.
- Pour Sonic Adventure 2, installez le [SA2 Mod Loader](https://info.sonicretro.org/SA2_Mod_Loader), ou vous pourrez accéder à des tas de mods améliorant l'expérience.

### Autres jeux 128 bits (Gamecube et Wii)

Pour ces autres jeux, votre meilleure option sera l'émulation, avec Dolphin pour émuler les versions gamecube ou Wii. Ces versions sont souvent les meilleures versions disponibles, même lorsqu'une version PC existe (pour Sonic Heroes et Sonic Riders). Les jeux suivants fonctionnent très bien sur Dolphin. Pour certains d'entre eux, vous pouvez utiliser des mods pour les améliorer.

- Sonic Heroes
- Shadow the Hedgehog - Peut être amélioré avec [Shadow the Hedgehog Reloaded](https://github.com/ShadowTheHedgehogHacking/ShdTH-Reloaded)
- Sonic Riders - Peut être amélioré avec [Sonic Riders Tournament](https://www.sonicriders.org/) ou [Sonic Riders DX](https://www.exgearlabs.org/downloads/sonic-riders-dx)
- Sonic Riders Zero Gravity - Peut être amélioré avec [Sonic Riders Regravitified](https://www.exgearlabs.org/downloads/sonic-riders-regravitified)
- Sonic and the Secret Rings
- Sonic and the Black Knight

A noter que ces mods d'améliorations ne contiennent pas que des bugfixes, mais des modifications du gameplay aussi. Ce sera une expérience différente.

## Jeux pré Steam

Avant Generations, de nombreux jeux Sonic plus modernes sont sortis sans version Steam. Deux de ces jeux (2006 et Unleashed) sont parmi les plus difficiles à jouer de nos jours.

### Sonic 2006

Pour Sonic 2006, les manières d'y jouer seront différentes suivants les préférence d'y jouer :

- Selon moi, une des manières les plus agréable d'y jouer est [Sonic Project P-06](https://www.youtube.com/watch?v=SCD5aQLlCD4), même s'il ne contient que les niveaux. *Cependant*, ce sera un jeu relativement différent, le gameplay ayant été transformé pour être plus proche des Adventures. Cependant, j'estime que cela fait du jeu une meilleurs expérience.
- Si vous voulez jouer à l'original, il vous faudra recourir à l'émulation, et à une branche alternative de [RPCS3](https://github.com/elad335/rpcs3/tree/sonic-race) pour y jouer (voici un [special build for Steam Deck](https://drive.google.com/file/d/1ztkvU5vOIAChJ9JIbQs7HGRVwUm8bKeh/view)).

Vous pouvez également utiliser le mod [Legacy of Solaris](https://github.com/lost-legacy-team/LoS-Mod_Files_X) avec la version XB360, qui contient quelques fix et améliorations QoL.

### Sonic Unleashed

Pour l'instant, la seule bonne solution pour jouer à Sonic Unleashed est de l'acquérir sur la XBox Series X/S, où vous pourrez le jouer en 60FPS, ce qui améliore BEAUCOUP les sensations du jeu. L'émulation n'est pas vraiment une bonne option pour l'instant.

### Sonic Colors

Sonic Colors a été réédité sur une plateforme moderne via Sonic Colors Ultimate. Cette version contient beaucoup de problèmes par rapport à la version originale. Vous pouvez essayer d'améliorer SCU avec un mod, ou jouer à la version originale sur Dolphin, selon que vous préférez la légère amélioration de la qualité de vie de SCU, ou l'expérience plus vierge de SC.

Je conseille à titre personnel plutôt la version originelle sur Dolphin, je trouve l'expérience plus agréable.

## Jeux Steam

Les jeux suivants sont disponibles sur Steam sans aucun problème. Pour certains d'entre eux, vous pouvez également améliorer l'expérience avec des mods pour obtenir une meilleurs expérience.

- Sonic Generations
- Sonic Lost World
- Sonic Forces
- Sonic Frontiers
- Sonic Mania
- Sonic 4 Episode 1
- Sonic 4 Episode 2
- Sonic All Star Racing
- Sonic All Star Racing Transformed
- Team Sonic Racing
- The Murder of Sonic the Hedgehog - Avec [une traduction en français par Chaos Trad](https://www.youtube.com/watch?v=hsD1IWEMQfc)

Pour Sonic Mania, vous pouvez y jouer sur d'autres plateformes grâce à la [décompilation](https://github.com/Rubberduckycooly/Sonic-Mania-Decompilation).