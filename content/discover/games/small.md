---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Les petits jeux
  parent: Jouer aux jeux
  order: 4
---


# Les petits jeux

La série Sonic a donné lieu à de nombreux jeux de moindre envergure, disponibles sur flash ou sur d'anciens systèmes mobiles. La plupart de ces jeux ont été réalisés durant la première moitié des années 2000, lors d'un pic de popularité de la série Sonic, dû à Sonic X.

## Les jeux flash

Il y a beaucoup de petits jeux flash qui ont été créés pour chaque jeu au début des années 2000. Vous pouvez les trouver [dans un pack ici](https://archive.org/details/sonic-the-hedgehog-flash-games). Pour y jouer, vous pouvez utiliser soit [Ruffle](https://ruffle.rs/), soit sous Linux [flash depuis flathub](https://flathub.org/fr/apps/com.adobe.Flash-Player-Projector). La liste contient les jeux flash suivants :

- SEGA Superstars Flash
- Shadow the Hedgehog Flash
- Sonic Advance 2 Jigsaw
- Sonic Adventure 2 Jigsaw
- Sonic Boom Link 'n Smash
- Sonic Heroes Puzzle
- Sonic Maze Craze
- Sonic Mega Collection Plus Mini
- Sonic Rivals Dash
- Sonic Rush Adventure Flash
- Sonic Speedway
- Sonic: The Broad Jump
- Sonic the Hedgehog Chaos Crush
- Sonic Xs

## Les jeux Sonic Cafe

Les jeux Sonic Cafe sont un ensemble de jeux publiés par SEGA pour les téléphones pré-Android utilisant Symbian. Ces jeux sont écrits en Java ME, et peuvent être téléchargés dans [cette archive](https://archive.org/details/all-available-sonic-java-games).

L'archive contient les jeux suivants :

- Shadow Shoots
- Sonic & SEGA All-Stars Racing
- Sonic at the Olympic Games
- Sonic Cricket
- Sonic Darts
- Sonic Evolution (un portage mobile de Sonic Advance)
- Sonic Fishing
- Sonic Hopping
- Sonic Jump
- Sonic Racing Kart
- Sonic Runners Adventure
- Sonic Spinball
- Sonic Tennis
- Sonic the Hedgehog - Part 1
- Sonic the Hedgehog - Part 2
- Sonic the Hedgehog 2 - Dash!
- Sonic the Hedgehog 2 - Crash!
- Sonic the Hedgehog Golf
- Sonic Unleashed (la version mobile de Sonic Unleashed, ne contenant que quelques niveaux)

A noter que beaucoup de jeux sont également perdus pour le moment, comme par exemple Sonic Timing of the Train, ou on poursuivait le train d'Eggman avec Sonic.

Pour les jouer, vous pouvez utiliser un émulateur JavaME comme [SquirrelJME](https://multiphasicapps.net/doc/ckout/readme.mkd) qui est également disponible dans le multi-émulateur RetroArch.

## Les jeux McDonald

McDonald a sorti entre 2003 et 2006 des petits jeux portable Sonic façon G&W. [Sonic McOrigins Plus](https://tvc-16.science/mcorigins-plus.html) est une recréation de tous ces jeux, avec les manuels et tout. Le jeu est disponible

## Jeux mobiles modernes

Les jeux mobiles modernes sont en grande partie des petis jeux avec plus ou moins de rejouabilités. Je ne vais ici parler que des exclusivités mobiles. Attention, nombre de ces jeux comporte des microtransaction.

Pour y jouer hors Google, il faut passer par des émulateurs android. Ces jeux nécessite les API Google Play de présente sur l'appareil.

- Sonic Dash : Gratuit, présent sur le Google & Apple Store
- Sonic Dash 2 - Sonic Boom : Gratuit, présent sur le Google & Apple Store
- Sonic Prime Dash - Réservé aux utilisateur Netflix
- Sonic Forces - Running Battle : Gratuit & Apple, présent sur le Google Store
- Sonic Runners Adventure - ~3$, présent sur le Google & Apple Store
- Sonic Jump Pro : ~ 3$, présent sur le Google & Apple Store
- Sonic at the Olympic Games - ~1$, présent sur le Google & Apple Store
- Sonic Jump Fever - A été délisté, doit être récupéré via un APK. A priori il marchait bien sur mon vieil Android, mais y'a toujours un risque parfois avec les évolutions d'API.

### Sonic Runners

Sonic Runners est souvent considéré comme l'un des meilleurs jeu mobile Sonic. Directement créé par la Sonic Team, sont plus gros soucis était sa roulette, souvent vue comme injuste. Le jeu a été supprimé en 2016, et remplacé par Sonic Runners Adventure (souvent vu comme bien inférieur) en 2017.

Cependant, des fans ont réussi à créé un fan serveur et ont relancé le jeu sous la forme d'un jeu gratuit, [Sonic Runners Revival](https://sonicrunners.com/#/) qui continue à faire vivre le jeu.

## Jeux Apple Arcade

Les jeux suivants ne sont disponible que sur Apple Arcade :

- Sonic Racing (une version simplifiée de Team Sonic Racing)
- Sonic Dream Team - Un plateformer 3D avec Sonic, Tails, Knuckles, Amy, Cream & Rouge

Il n'y a a ce jour pas encore de remake ou de moyen de jouer à ces jeux. Le Apple Arcade à également une version spéciale de Sonic Dash.