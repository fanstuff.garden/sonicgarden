---
layout: layouts/base.njk
eleventyNavigation:
  key: Sonic Robo Blast 2
  parent: La fan-area
  order: "2"
---

# Sonic Robo Blast 2

[Sonic Robo Blast 2](https://srb2.org) est un fangame Sonic démarré en 1998, par une équipe nommée la Sonic Team Junior (dont la composition à évoluée avec le temps). Ce jeu utilise un moteur dérivé de celui de Doom, et sont code est open-source. La combinaison du code open-source et de la modabilité déjà forte du moteur Doom à produit un jeu qui possède sa propre communauté de mods, réalisations, et projets.

Le jeu peut être joué en multi, soit via des modes de compétitions (course, batailles de tirs d'anneaux, catch the flag), mais aussi via un mode coopération permettant de faire l'aventure à plusieurs, le tout de manière super fun.

Le jeu contient de base Sonic, Tails, Knuckles en personnage, et Amy, Fang et Metal Sonic y sont déblocable.

Pour les utilisateurs de Linux, le jeu est disponible sur [flathub](https://flathub.org/apps/org.srb2.SRB2).

**Note :** tout les mods ici ne viendront que du [forum officiel](https://mb.srb2.org/), et ne comporteront pas de portage de vieux mods fait sans l'accort de leur propriétaires, pour des raisons de respect.

**Note 2 :** L'absence d'un mod ici ne veut pas dire qu'il manquerait de qualité. Cette liste n'est pas exhaustive, et notamment j'y ai pas encore mis les fan-character, ne les ayant pas encore beaucoup testés.

## Jeux dérivés

Plusieurs jeux ont été dérivé de SRB2, en particulier deux, créé tout les deux par le "Kart Krew", une équipe de moddeurs de SRB2 (certains participant même au jeu de base).

- [SRB2 Top-Down](https://mb.srb2.org/threads/srb2-top-down.24786/) est un exe mod transformant SRB2 en un jeu mélangeant des aspects des
- [SRB2 Kart](https://mb.srb2.org/addons/srb2kart.2435/) est un jeu de karting basé sur le moteur de SRB2. Comportant énormément de niveaux, et ayant sa propre catégorie de mod sur le forum, c'est devenu un jeu ayant sa propre popularité et sa propre partie du fandom de Sonic. Le jeu est disponible sur [flathub](https://flathub.org/apps/org.srb2.SRB2Kart), ainsi que dans une version [moddée](https://flathub.org/apps/org.srb2.SRB2Kart-galaxy) ajoutant quelques features voir même le [AZERTY](https://flathub.org/apps/org.srb2.SRB2Kart-galaxy_azerty).
- [Dr Robotnik's Ring Racers](https://www.kartkrew.org/) - La suite de SRB2 Kart, un jeu de karting technique avec des tonnes de fonctionnalités et de contenus, mais moins accessible.

## Mods Gameplays

Des mods modifiants le gameplay de SRB2.

### Total Conversion

Des mods transformants SRB2 en un jeu complet plus ou moins entier.

- [SRB2 Dungeon Crawler](https://mb.srb2.org/addons/srb2-dungeon-crawler-v2-0-2-a-rogue-like-srb2-experience.153/) - Un Rogue-like dans SRB2
- [SRB2Gold](https://mb.srb2.org/addons/srb2-golf.2867/) - Un jeu de golf dans SRB2
- [Metroid Vanguard](https://mb.srb2.org/addons/metroid-vanguard-samus-aran-in-srb2-v1-4.2666/) - Un jeu Metroid 3D dans les niveaux de SRB2.
- [SRB2infinity](https://mb.srb2.org/addons/srb2infinity-a-phantasy-star-knockoff.5373/) - un clone de Phantasy Star Online dans SRB2
- [Minecart Racing](https://mb.srb2.org/addons/minecart-racing.5740/) - Des course de minecarts

### Modifications gameplay

Des modifications plus légères, remixant un peu le jeu.

- [Encore Mode](https://mb.srb2.org/addons/encore-mode.3968/) - Un mode encore similaire à celui de Mania Plus
- [Pizza Time](https://mb.srb2.org/addons/nicks-pizza-time-pizza-time-v2.5456/) - Ajoute un Pizza Time à SRB2, vous forçant à foncer vers le départ.
- [Adventure Mode](https://mb.srb2.org/addons/final-demo-adventure-mode.2886/) - Restore le Adventure Mode de SBR2 Final Demo
- [SRB2 Underwater](https://mb.srb2.org/addons/srb2-underwater-a-hedgehogs-greatest-nightmare.3028/) - SRB2 mais tout est sous l'eau
- [Momentum+](https://mb.srb2.org/addons/momentum.5706/) - Modifie la gestion du momentum et modifie le gameplay de tout les personnages
- [CrossMomentum](https://mb.srb2.org/addons/v1-3-crossmomentum-a-complete-gameplay-overhaul.1371/) - Une refonte complète du gameplay de SRB2
- [SRB2 Randomizer](https://mb.srb2.org/addons/srb2randomizer.1334/) - Un randomizer pour SRB2

### Multijoueurs

Des mods qui ajoutent des mods de jeux au mode multijoueur de SRB2, offrant des nouvelles manières de s'affronter ou coopérer.

- [Emblem Hunt](https://mb.srb2.org/addons/emblem-hunt.4603/) - Un mod dedier à la chasse aux emblemes.
- [Tron SRB2](https://mb.srb2.org/addons/srb2-tron-srb2t-v2-3-1-tron-meets-srb2.152/) - Un mode de jeu à la Tron
- [King of the Hill](https://mb.srb2.org/addons/king-of-the-hill.2879/) - Restez en haut de la colline !
- [Chaos Mode](https://mb.srb2.org/addons/chaos-mode.3010/) - Affrontez des vagues d'ennemis
- [SRB2 Battle](https://mb.srb2.org/addons/srb2-battle-multiplayer-gameplay-mod.171/) - Un mode bataille plus approfondis, avec de nouvelles mécaniques. De nombreux mods personnages sont compatibles.
- [RingSlinger NEO](https://mb.srb2.org/addons/ringslinger-neo.3864/) - Une amélioration du mode de duel FPS du jeu.
- [Sonic.exe](https://mb.srb2.org/addons/sonic-exe-v4-1-5a-compatibility.480/) - Apporte deux types de modes de jeux : Exe Survival (façon jeu d'horreur assymétrique, ou seule les Exe peuvent tuer) et Exe Terrorisme (ou les Exe et les Innocents peuvent se tuer les uns les autres, mais tout le monde semble innocent)

## Niveaux

### Aventures

- [Sonic and the Gunslinger](https://mb.srb2.org/addons/sonic-and-the-gunslinger.5760/)
- [Tailwind](https://metalharbor.itch.io/tailwinds) - Tails × Pilotwings
- [Sol Sestancia](https://mb.srb2.org/addons/sol-sestancia.116/)
- [Sol Sestancia 2](https://mb.srb2.org/addons/sol-sestancia-2.7136/)
- [Sonic Adventure Genisys](https://mb.srb2.org/addons/sonic-adventure-genisys.2646/) - Un mod SRB2 parodique
- [Angel Island Tour](https://mb.srb2.org/addons/angel-island-tour.3131/)
- [Sonic 2006 - Extended Port](https://mb.srb2.org/addons/sonic-2006-extended-port.157/) - Sonic 2006 dans SRB2
- [Chaos Domain](https://mb.srb2.org/addons/srb2-chaos-domain.2720/) - Deux niveaux de trois acts, et deux de deux acts.
- [Tortured Planet](https://mb.srb2.org/addons/tortured-planet.5074/) - Un ancien mod, créé pour la version 2.0
- [Mystic Realm](https://mb.srb2.org/addons/v5-2-the-mystic-realm.119/) - Un autre ancien mod, de l'époque Final Demo. Un remake est en cours de création.
- [SA2B 2.2 Port](https://mb.srb2.org/addons/sonic-adventure-2-battle-2-2-port.4767/) - Un ancien mod portant SA2 dans SRB2 2.0. Les maps datent d'avant les pentes et ont pas très bien vieillis.

### Level pack

Des packs de niveaux, souvent fait par différents créateur⋅ices de manière collaborative.

#### OLDC : Official Level Design Contest / Collab

- [OLDC Summer 2020](https://mb.srb2.org/addons/official-level-design-contest-summer-2020.1054/)
- [OLDC Autumn 2020](https://mb.srb2.org/addons/official-level-design-contest-autumn-2020.1056/)
- [OLDC 2021 : Round 1](https://mb.srb2.org/addons/official-level-design-contest-2021-round-1.2961/)
- [OLDC 2021 : Round 2](https://mb.srb2.org/addons/official-level-design-contest-2021-round-2.3579/)
- [OLDC 2022 : Round 1](https://mb.srb2.org/addons/official-level-design-contest-2022-round-1.4156/)
- [OLDC 2022 : Round 2](https://mb.srb2.org/addons/official-level-design-collab-2022-round-2.4802/) - Premier à être devenu une collab
- [OLDC 2023 : Round 1](https://mb.srb2.org/addons/official-level-design-collab-2023-round-1.5578/)
- [OLDC 2023 : Round 2](https://mb.srb2.org/addons/official-level-design-collab-2023-round-2.6133/)
- [OLDC 2024 : Round 1](https://mb.srb2.org/addons/official-level-design-collab-2024-round-1.7474/)

#### ULDC : Unofficial Level Design Collab

- [ULDC Spring 2020](https://mb.srb2.org/addons/v3-0-unofficial-level-design-collab-spring-2020.122/)
- [ULDC Summer 2021](https://mb.srb2.org/addons/v1-6-unofficial-level-design-collab-summer-2021.3136/)
- [ULDC Autumn 2021](https://mb.srb2.org/addons/v1-6-unofficial-level-design-collab-autumn-2021.3423/)

#### SUGOI

- [Shut Up and Get On It](https://mb.srb2.org/addons/sugoi.6427/) - La fusion de trois pack de niveau, SUGOI, SUBARASHII et KIMOKAWAII (oui) fait pour la version 2.1 de SRB2. Ce pack contient en tout plus de 140 niveaux pour SRB2 par plus de 80 contributeurs.

### Portage anciennes versions

*Note:* Même si SRB2 The Past contient toute les maps, d'autres portages sont aussi inclus, ça peut être utile je trouve d'avoir aussi des version ne contenant qu'un élément précis.

- [SRB1 Remake](https://mb.srb2.org/addons/srb1-remake.148/) - Remake des niveaux de SRB1
- [2.1 levels](https://mb.srb2.org/addons/2-1-levels.4228/) - Rejouez à la campagne de SRB2 2.1 dans SRB2 2.1
- [Final Demo Zone](https://mb.srb2.org/addons/final-demo-zone-vsl_finaldemo_v1_0-pk3.133/) - Tout les niveaux de la final demo en un
- [SRB2 The Past](https://mb.srb2.org/addons/sonic-robo-blast-2-the-past-version-2-0.4613/) - Une collection de plein d'ancienne version des maps de SRB2

### Packs multi-joueurs

- [Ringslinger Re-Match](https://mb.srb2.org/addons/ringslinger-rematch.5415/) - Un set de maps pour les modes ringslingers/Catch the Flag
- [Chaos Circuit](https://mb.srb2.org/addons/chaos-circuit-map-pack-rl_chaoscircuit2-v1-5-wad.197/) - Un set de maps pour faire la course.
- [SADX Race](https://mb.srb2.org/addons/sonic-adventure-dx-race.215/) - Des niveaux de course basé sur SADX
- [Chaos Pack](https://mb.srb2.org/addons/1-1-1-update-chaos-pack.3604/) - Des niveaux pour le Chaos Modes
- [Super Smash Bros 64 Stages](https://mb.srb2.org/addons/battle-super-smash-bros-64-stages-v1-1.265/) - Des niveaux de Smash 64 pour le mod Battle
- [Sonic Battle Collection](https://mb.srb2.org/addons/battle-sonic-battle-collection.114/) - Des niveaux de Sonic Battle pour le mod Battle (fair enough)

### Niveaux individuels

- [Seaside Hill](https://mb.srb2.org/addons/seaside-hill-zone.4826/) - Une recréation de Seaside Hill
- [SRB2Hub](https://mb.srb2.org/addons/srb2hub-for-2-2.186/) - Un hub world pour SRB2
- [Jade Gully](https://mb.srb2.org/addons/srb2xtreme-jade-gully-demo.200/) - Jade Gully de Sonic Xtreme dans SRB2

### Divers

- [SRB Dream Emulator](https://mb.srb2.org/addons/srb-dream-emulator.2509/)

## Personnages

Parmis les mods de SRB2, beaucoup permettent de jouer à d'autres personnages, parfois avec des gameplays complètements différents. Dû à ça, SRB2 est peut-être le jeu ou vous pouvez jouer le PLUS de personnages jouables.

### Sonic the Hedgehog

Sonic à le droit au plus de mods dans la communauté, offrant pas mal de variation de son gameplay, inspirée souvent des différentes ères de son personnages.

- [ChrispyChars](https://mb.srb2.org/addons/chrispychars-pointy-sonic-and-fluffy-tails.485/) - Contient Pointy Sonic et Fluffy Tails
- [Xtreme Sonic](https://mb.srb2.org/addons/xtreme-sonic.511/) - Le Sonic de Sonic Xtreme, avec le Spin Slash
- [Adventure Sonic](https://mb.srb2.org/addons/adventure-sonic-v1-6b.3589/) - Un Sonic basé sur celui de SA et SA2
- [Neo Sonic](https://mb.srb2.org/addons/neo-sonic.504/) - Un Sonic inspiré des Sonic Advance
- [Modern Sonic](https://mb.srb2.org/addons/modern-sonic-v5-12.501/) - Un Sonic inspiré de l'ère Boost
- [Bandages Reborn](https://mb.srb2.org/addons/bandages-reborn-v-1-2.625/) - Un Sonic basé sur Sonic Boom
- [Sonc](https://mb.srb2.org/addons/sonc.5480/) - Sonc
- [X Sonic](https://mb.srb2.org/addons/x-sonic.7372/) - Le Sonic de Sonic X

### Personnages classiques

- [EggPack](https://mb.srb2.org/addons/eggpack-remake-v2-6.502/) - Eggman dans SRB2
- [The Chaotix](https://mb.srb2.org/addons/the-chaotix.5734/) - Ajoute Espio, Vector, Charmy, Mighty, Heavy et Bomb à SRB2
- [EggRobo](https://mb.srb2.org/addons/egg-robo.549/)
- [Bean the Dynamite](https://mb.srb2.org/addons/bean-the-dynamite.2816/) - Bombs
- [Honey the Cat](https://mb.srb2.org/addons/honey-the-cat.2592/)
- [Metal Knuckles & Tails Doll](https://mb.srb2.org/addons/metal-knuckles-tails-doll-sonic-r-characters.587/) - Deux rivaux de Sonic R dans SRB2
- [Ray the Flying Squirrel](https://mb.srb2.org/addons/ray-the-flying-squirrel.6348/)

### Personnages modernes

- [E-102 Gamma](https://mb.srb2.org/addons/e-102-gamma.5259/) - Gamma avec son gameplay d'Adventure
- [Shadow the Hedgehog](https://mb.srb2.org/addons/shadow-the-hedgehog.517/) - Le seul et l'unique.
- [Shadow Androids](https://mb.srb2.org/addons/shadow-android-v1-3-can-now-speen-under-certain-circumstances.698/)
- [RushChars](https://mb.srb2.org/addons/rushchars-blaze-and-marine.3831/) - Ajoute Blaze et Marine.
- [Jet the Hawk](https://mb.srb2.org/addons/jet-the-hawk-v1-1-now-slightly-less-broken.752/) - Ajoute Jet, avec un gameplay un peu Riders-esque
- [Silver the Hedgehog](https://mb.srb2.org/addons/silver-the-hedgehog.503/) - Ajoute Silver (et sa télékinesie)
- [Le Werehog](https://mb.srb2.org/addons/the-werehog-v2-3.505/) - Oui techniquement c'est une version de Sonic, chut.

### Personnages IDW

- [Tangle the Lemur](https://mb.srb2.org/addons/tangle-the-lemur.2670/) - Weeeeeeeee
- [Whisper the Wolf](https://mb.srb2.org/addons/whisper-the-wolf.4325/) - Avec même un wispon fonctionnel, et une vue FPS.
- [Surge the Tenrec](https://mb.srb2.org/addons/surge-the-tenrec.3605/) - La rivale survoltée de Sonic dans IDW.

### Autres franchises

- [Pac Man](https://mb.srb2.org/addons/pac-man.3337/)
- [Super Mario Bros](https://mb.srb2.org/addons/super-mario-bros.3420/) - Mario et Luigi jouable dans SRB2
- [Flicky](https://mb.srb2.org/addons/flicky-tm.497/) - Un Flicky
- [Samus Aran](https://mb.srb2.org/addons/metroid-vanguard-samus-aran-in-srb2-v1-4.2666/) - Fait partie du mod Metroid Vanguard
- [Kirby](https://mb.srb2.org/addons/kirby.510/)
- [Yoshi](https://mb.srb2.org/addons/yoshi.687/) - Un yoshi, complet avec la mécanique de manger et les œufs.
- [Kamek](https://mb.srb2.org/addons/kamek.3789/) - Qui vole. Oui.
- [Meta Knight](https://mb.srb2.org/addons/meta-knight.2780/)
- [Doom Slayer](https://mb.srb2.org/addons/the-doom-slayer.4819/) - Le perso de Doom dans le jeu dérivé de Doom.
- [N64 Mario](https://mb.srb2.org/addons/n64-mario.3251/) - Mario avec son moveset de Mario 64
- [Spyro le Dragon](https://mb.srb2.org/addons/spyro-the-dragon.512/) - La chèvre violette qui crache du feu.
- [Kiryu Kazuma](https://mb.srb2.org/addons/kiryu-kazuma.3882/) - Yakuza dans Sonic.
- [Plague Knight](https://mb.srb2.org/addons/plague-knight.3667/)
- [DeltaChars](https://mb.srb2.org/addons/deltachars.5139/) - Kris, Susie et Ralsei de Deltarune dans SRB2.
- [Elfilin](https://mb.srb2.org/addons/elfilin-friend-from-the-forgotten-land.5382/)

### Divers

- [Sonic.exe](https://mb.srb2.org/addons/sonic-exe-v4-1-5a-compatibility.480/) - Présent aussi dans la section "multijoueurs"
- [Super Mystic Sonic](https://mb.srb2.org/addons/super-mystic-sonic-smsreborn-v3-1-polished-to-a-shine.477/) - Une version extrèmement puissante et versatile de Sonic.
- [Eggette/Omelette](https://mb.srb2.org/addons/eggette-or-omelette-if-you-prefer.3385/)

## Autres mods

Pour les autres mods, ils seront classé en groupes suivants des buts spécifiques

### Recréer Sonic Adventure

Tout les mods pouvant être utile pour avoir une expérience à la Sonic Adventure, conseillé par le mod Sonic Adventure directement.

- [Adventure HUD](https://mb.srb2.org/threads/sonic-adventure-style-hud.27294/) - Un HUD à la Adventure 1
- [Adventure Emblems](https://mb.srb2.org/addons/adventure-style-emblems.3592/) - Des emblems à la Adventure 1/2
- [Adventure Monitors](https://mb.srb2.org/threads/modern-monitors-for-2-2-v3-3.27615/) - Les moniteurs modernes
- [Goal Ring Mod](https://mb.srb2.org/threads/goal-ring.28845/) - Le Goal Ring de SA2
- [SA-style ennemi explosion](https://mb.srb2.org/addons/adventure-styled-explosions.3571/) - Fait exploser les ennemis à la SA2
- [SA 1-UP](https://drive.google.com/file/d/1_564YPjMhYcouS7L0mTtkIIzgAGdx0MW/view?usp=drive_link) - Remplace le jingle de vie pour être plus proched de SA1. (Ce mod provient du topique du Sonic de Sonic Adventure)