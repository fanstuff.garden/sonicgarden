---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Mods
  parent: Fangames et mods
  order: 3
---

# Mods

Les mods sont des modifications de jeux non-émulés, qui nécessite souvent le jeu originel pour être joué.

## Sonic Adventure

### Niveaux et packs de niveaux

- [Prototype Windy Valley](http://sonichacking.org/entry/120) - Par ItsEasyActually.

## Sonic Adventure 2

### Niveaux et packs de niveaux

- [Emerald Coast](http://sonichacking.org/entry/68) - Par MainMemory.
- [Dreamcast Chao Gardens](http://sonichacking.org/entry/104) - Par Exant64.

## Sonic Heroes

### Niveaux et packs de niveaux

- [Radical Highway](https://www.youtube.com/watch?v=MdcqrSudtmo) - Par Shadowth117.

## Sonic Generations

### Conversions totales

- [Sonic Adventure Generations](https://www.moddb.com/mods/sonic-adventure-generations) - Un pack de niveaux des Sonic Adventure, par JCorvinus, Steven Grayson et S0lv0.

### Amélioration et nouvelles fonctionnalités

- [Cell Shading](https://www.youtube.com/watch?v=UNso3wn7T0w) - Par Dario.

### Niveaux et packs de niveaux

- [EggmanLand Mod](https://www.youtube.com/watch?v=KUdd9nq8DX8) - Par N69vid.
- [Aquarium Park](https://www.youtube.com/watch?v=IGOv9RRhy9k) - Par Hyuga.
- [Aquarium Park](http://sonichacking.org/entry/98) - Un autre, par Skyth.
- [Final Rush](http://lostgenerations.freeforums.net/thread/6/final-level-sonic-world-version) - Par Flash Dash.
- [Camelot Castle](https://www.youtube.com/watch?v=AHBdZhRzkBE) - Par Melpontro.
- [Sweet Mountain Act 2](http://sonichacking.org/entry/114) - Par JoeyLaw.
- [Tropical Resort ReBoosted](http://sonichacking.org/entry/97) - Par SKmaric.

### Characters Mods

- [Super Sonic Generations](https://www.moddb.com/mods/super-sonic-generations/news/super-sonic-generations-2016-edition) - Joue à Sonic Generations avec Super Sonic, par JoeTE.
- [Real Tails Mod](https://www.moddb.com/games/sonic-generations/downloads/real-tails-mod-ver20) - Jouez Tails dans Sonic Generations, par UltimateDarkman.

## Sonic Forces

### Amélioration et nouvelles fonctionnalités

- [Classic Sonic Improvement](http://sonichacking.org/entry/91) - Physics fixes for Classic Sonic, par Showin.

### Niveaux et packs de niveaux

- [Lost Valley Overclocked](http://sonichacking.org/entry/111) - Par Infinite Force.
- [Mortar Canyon Remastered](http://sonichacking.org/entry/113) - Par Infinite Force.
- [Network Terminal Overclocked](http://sonichacking.org/entry/112) - Par Infinite Force.
- [Real Infinite Boss](http://sonichacking.org/entry/110) - Par Infinite Force.
- [Grand Metropolis](https://gamebanana.com/maps/202631) - Par Infinite Force.
- [Westopolis](https://gamebanana.com/maps/201020) - Par Knuxfan24.
- [Glyphic Canyon](http://sonichacking.org/entry/89) - Par Knuxfan24.
- [Lava Shelter](http://sonichacking.org/entry/84) - Par Knuxfan24.
- [Weapon Bed](https://www.youtube.com/watch?v=pKnlsS6sSFQ) - Par ĐeäTh.

## Sonic Robo Blast 2

This list contain only mods for SRB2 2.2.

### Conversions totales

- [SRB2 Bomberman](https://mb.srb2.org/showthread.php?t=46510) - Un mod bomberman pour SRB2, par 

### Improvement and Feature mods

- [SRB2NetPlus](https://mb.srb2.org/showthread.php?t=46342) - Un netgame amélioré, par LXShadow.
- [Jeck Jims' 2.2 3D Models](https://mb.srb2.org/showthread.php?t=46560) - Des models 3D low-poly, par Jeck Jims.
- [Modern Monitors](https://mb.srb2.org/showthread.php?t=46622) - Des itembox modernes, par Demnyx Onyxwing.
- [Tails' Super Flickies](https://mb.srb2.org/showthread.php?t=46444) - Une superform avec des Flicky pour Tails, par birbhorse et gregory\_house.
- [Everyone likes Tails](https://mb.srb2.org/showthread.php?t=46345) - Avoir Tails qui suis n'importe quel personnage, par Zippy_Zolton.

### Levels and Level Pack mods

- [SRB2 Apologue Pack](https://mb.srb2.org/showthread.php?t=46028) - Un set de niveaux Singleplayer / Co-op levels, turned into a neat short little campaign-style experience, par RomioTheBadass.

### Mods de personnages

- [Tails Guy](https://mb.srb2.org/showthread.php?t=45726) - Tails mais c'est le Doom Guy, par Tripel the fox et Maximus Universal.
- [Blaze The Cat](https://mb.srb2.org/showthread.php?t=46017) - Par TDRR, Lilly, Naluigi et .Luke.
- [Flicky](https://mb.srb2.org/showthread.php?t=45666) - Un Flicky, par MotorRoach, HellHawkX, Lach et VAdaPEGA.
- [Egg Robo](https://mb.srb2.org/showthread.php?t=46518) - Par CobaltBW, DrTapeworm, SuperPhanto et toaster.
- [Neo Sonic](https://mb.srb2.org/showthread.php?t=46505) - An Adventure/Advance-y Sonic, par Chengi, Crum, Dimpsuu, Lach, Lat' et TehRealSalt
- [Metal Knuckles](https://mb.srb2.org/showthread.php?t=46047) - Par Lloydirving7736, BENJITA9000, Chaos Zero 64, corneliab et senku.
- [Mecha Sonic](https://mb.srb2.org/showthread.php?t=46737) - Par CANINE.
- [Modern Sonic](https://mb.srb2.org/showthread.php?t=46476) - Sonic façon Unleashed, par MotdSpork, Badnik, Chrispy, Golden Shine, HitCoder, K.S., Lach, ManicTH14, Rumia1, TrickyTex, Zero\_the\_artificial, Zipper.
- [Eggpack](https://mb.srb2.org/showthread.php?t=46482) - Eggman avec un jetpack, par Zero\_the\_artificial.
- [Silver the Hedgehog](https://mb.srb2.org/showthread.php?t=46496) - Par Lat', Inazuma, MotorRoach, QuikSilver-TH, Snu, zxyspku.
- [Sonic the Werehog](https://mb.srb2.org/showthread.php?t=46770) - Par Zero\_the\_artificial ApocHedgie, DaOrangeBoi, Ikkarou Tatsuru, Inazuma, Lach, MotdSpork et Rumia1
- [Yoshi](https://mb.srb2.org/showthread.php?t=46743) - Oui oui, c'est lui, par SMS Alfredo.
- [Mighty the Armadillo](https://mb.srb2.org/showthread.php?t=46840) - Mighty avec son moveset de Mania, par Man553, Emong, Monster Iestyn, Tripel the fox.