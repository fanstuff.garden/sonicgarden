---
layout: layouts/base-toppages.njk
eleventyNavigation:
  key: Fangames et mods
  parent: La fan-area
  order: "1"
---

# Fangames et mods

Un domaine dans lequels on retrouve énormément de création est évidemment le domaine des *fangames*, ce qui est logique pour une série originellement de jeux vidéos. Les fangames Sonic ont même obtenus pas mal de réputations à travers les années.

Le but est ici de retrouver tout ce qui concernes les jeux, que ce soit des jeux créé from scratch, des mods ou des hackroms de jeux rétro Sonic.

( À noter que SRB2, au vu de sa taille, à sa propre section en dessous )

## Types de jeux

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Note sur la toxicité

Une réaction assez courante quand on voit un jeu très cool Sonic est le fameux « SEGA devrait vous engager », que ce soit dit avec une toute innocence, ou avec une volonté plus désagréable de "vous faites mieux que ces nuls".

S'ils vous plait, évitez ce genre de comportement : ils peuvent être stressant pour les créateurs, et ne prennent souvent pas compte de beaucoup d'aspects.

Et d'un autre côté, n'oubliez pas que tout ces jeux ont été fait sur le temps libre des créateurs. Rien que réussir à terminer un jeu est extrèmement difficile, la majorité des projets finissent abandonné. Il est important de garder une athmosphère de positivité (tout en étant capable de voir les défauts de projets), et de ne pas descendre ni d'attaquer personnellement des créatifs.

## Classification

- Fangames
  - Genesis-like
  - Advance-like
  - 2D Autre
  - 3D Classique
  - 3D Moderne
  - 3D Autre
  - Autre style *(Tout les jeux avec des gameplay non-standard)*
    - Chao
    - Humour
    - Styles alternatifs
- Hackroms
  - Conversions totales (Les hacks transformant en un tout nouveau style)
    - Petit jeu
    - Autre style
  - Amélioration et nouvelle fonctionnalité (le même jeu avec de nouvelles fonctionnalités)
    - (une catégorie par jeu)
  - Hack de Gameplay
    - (une catégorie par jeu)
  - Hack de personnage
    - (une catégorie par jeu)
- Mods
  - *une catégorie par jeu*
    - Conversion totale
    - Niveau et pack de niveaux
    - Hack de personnage
    - Autre