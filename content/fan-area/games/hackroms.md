---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Hackroms
  parent: Fangames et mods
  order: 2
---

# Hackroms

Des modifications de jeux retro, qui peuvnet pour la plupars être joué sur émulateur. La majorité sont des hackroms Megadrive. Contrairement à d'autres communautés, la communauté de hacking Sonic n'upload pas des patchs mais directement des roms modifiés, ce qui fait que la plupars d'être elles peuvent être joué directement sur émulateur ou votre plateforme de choix.

## Conversions totales

Des hacks qui transforment le jeu en des jeux complètement ou fortement différents (nouveaux niveaux, graphismes, etc).


- [Mobius Evolution](http://info.sonicretro.org/Mobius_Evolution) - Une conversions totale de Sonic 1, avec Sonic et Blaze, par Abyssal Leopard.
- [Metal Sonic Hyperdrive](http://info.sonicretro.org/Metal_Sonic_Hyperdrive) - Un jeu complet avec Metal Sonic, par Lone Devil.
- [Metal Sonic Rebooted](http://info.sonicretro.org/Metal_Sonic_Rebooted) - Un autre jeu complet avec Metal Sonic, par Lone Devil.
- [Sonic 2 Retro Remix](http://info.sonicretro.org/Sonic_2_Retro_Remix) - Un hack 2D mettant en avant l'exploration, par Thorn et DNXDelta.
- [Sonic 2 Advanced Edit](http://info.sonicretro.org/Sonic_2:_Advanced_Edit) - Une transformation complète de Sonic 2, par rika_chou et la Team Advanced Edit
- [Sonic Hellfire Saga](https://www.youtube.com/watch?v=SiEohopUkWc) - Sonic contre des démons, par Red Miso Studio.

### Petits jeux

Des hacks avec moins de niveaux, pouvant être fini dans de petits sessions.

- [Pana Der Hejhog](https://info.sonicretro.org/Pana_Der_Hejhog) - Un petit hacks très détaillés, par MarkeyJester.
- [Sonic : The Next Level](https://forums.sonicretro.org/index.php?showtopic=34835) - Sonic sur une autre planète, par MarkeyJester.
- [Sonic Boom](https://info.sonicretro.org/Sonic_Boom_(hack)) - Un hack court mais intense, par le SÆGA collective.
- [Mr. Cat's 16 day challenge!](http://sonicresearch.org/community/index.php?threads/mr-cats-16-day-challenge-released.5029/) - Un petit hack créé en 16 jours.

### Styles différents

Transformation du jeu original en quelque chose de complètement différent


- [Big's Fishing Derby](http://info.sonicretro.org/Big%27s_Fishing_Derby) - Un jeu de pêche avec Big, avec Flamewing.
- [Doctor Robotnik's Creature Capture](http://info.sonicretro.org/Doctor_Robotnik%27s_Creature_Capture) - Capturez autant de créature que possible avec Eggman, par MarkeyJeser.

## Améliorations et nouvelles fonctionnalités

### Sonic the Hedgehog

- [Sonic the Hedgehog 1 @ SAGE 2010](http://info.sonicretro.org/Sonic_the_Hedgehog_1_@_SAGE_2010) - Une amélioration de Sonic 1 basé sur le scoring, par Cinossu.

### Sonic the Hedgehog 2

- [Sonic 2 Long Version](http://info.sonicretro.org/Sonic_2_Long_Version) - Sonic 2 étendu avec des niveaux abandonnés, par Sonic Hachelle-Bee.
- [Drop Dash in Sonic 2](http://sonichacking.org/entry/69) - Le Drop Dash dans Sonic 2, par MainMemory.

### Sonic the Hedgehog CD

- [Sonic CD++](http://info.sonicretro.org/Sonic_CD_Plus_Plus) - Un hack ajoutant le jump dash/homming attack et un spindash façon Sonic 2 dans Sonic CD, par qiuu et snkenjoi.

### Sonic the Hedgehog 1 et 2

- [Sonic 2 Delta](http://info.sonicretro.org/Sonic_2_Delta) - Sonic 2 fusionné avec Sonic 1, avec des niveaux inutilisés de Sonic 2, par Esrael.

### Sonic the Hedgehog 3 and Knuckles

- [Sonic 3 Complete](http://info.sonicretro.org/Sonic_3_Complete) - La version définitive de S3&K, par Tiddles, Neo, That One Jig, ValleyBell, flamewing et Hayate.
- [Sonic 3 Hadoken](http://www.hacking-cult.org/?hacks/61/x) - Sonic 3&K. Avec le Hadoken.
- [Drop Dash in Sonic 3&K](http://sonichacking.org/entry/78) - Le Drop Dash dans Sonic 3&K, par MainMemory.

### Sonic 3D Blast

- [Sonic 3D: Director's Cut](http://info.sonicretro.org/Sonic_3D:_Director%27s_Cut) - Une director's cut de Sonic 3D Blast, par le dev originel de Sonic 3D.

## Hacks de gameplay

Des hacks étant le jeu de base, avec des missions, style de gameplay, etc… différents. Les Boss Rush sont rajouté dans cette catégorie.

### Sonic the Hedgehog

- [An Ordinary Sonic ROM Hack](http://shc.sonicresearch.org/history13/1) - Une romhack façon creepypastas, par Cinossu.

### Sonic the Hedgehog 2

- [Sonic 2 Adventure Edition](http://info.sonicretro.org/Sonic_2_Adventure_Edition) - Sonic 2 avec plusieurs gameplay et élément façon Adventure, par MainMemory.
- [Sonic 2 Secret Ring Edition](http://info.sonicretro.org/Sonic_2_Secret_Rings_Edition) - Des missions façon Secret Rings dans Sonic 2, par MainMemory.
- [Knuckles' Emerald Hunt](http://info.sonicretro.org/Knuckles%27_Emerald_Hunt) - Un jeu de chasse au trésor inspiré par les Sonic Adventure, par MainMemory.

### Sonic the Hedgehog 1 et 2

- [Sonic Classic Heroes](http://info.sonicretro.org/Sonic_Classic_Heroes) - S1 et S2 façon Sonic Heroes, par Flamewing.
- [Robotnik's Revenge](http://info.sonicretro.org/Robotnik%27s_Revenge) - Un Boss Rush Sonic 1 et 2, par ColinC10.

## Hacks de Personnages

Hacks dans lequel vous jouez d'autres personnages

### Sonic the Hedgehog 1

- [Tails in Sonic the Hedgehog](http://info.sonicretro.org/Tails_in_Sonic_the_Hedgehog) - Jouez Tails dans Sonic 1, par drx (v1) et Puto (v2).
- [Knuckles the Echidna in Sonic the Hedgehog](http://info.sonicretro.org/Knuckles_the_Echidna_in_Sonic_the_Hedgehog) - Jouez Knuckles dans Sonic 1, par Stealth.
- [Amy Rose in Sonic the Hedgehog](http://info.sonicretro.org/Amy_Rose_in_Sonic_the_Hedgehog) - Jouez Amy dans Sonic 1, par E-122 Psi.
- [Sally Acorn in Sonic the Hedgehog](http://info.sonicretro.org/Sally_Acorn_in_Sonic_the_Hedgehog) - Jouez Sally dans Sonic 1, par E-122 Psi.
- [Bunnie Rabbot in Sonic the Hedgehog](http://forums.sonicretro.org/index.php?showtopic=34542) - Jouez Bunnie dans Sonic 1, par E-122 Psi.
- [Kirby in Sonic the Hedgehog](http://info.sonicretro.org/Kirby_in_Sonic_the_Hedgehog) - Jouez Kirby dans Sonic 1, par Lone Devil.
- [Metal Sonic in Sonic the Hedgehog](http://info.sonicretro.org/Metal_Sonic_in_Sonic_the_Hedgehog) - Jouez Metal Sonic dans Sonic 1, par Lone Devil.
- [Charmy Bee in Sonic the Hedgehog](http://info.sonicretro.org/Charmy_Bee_in_Sonic_the_Hedgehog) - Jouez Charmy Bee dans Sonic 1, par E-122 Psi.
- [Mighty the Armadillo in Sonic the Hedgehog](http://info.sonicretro.org/Mighty_the_Armadillo_in_Sonic_the_Hedgehog) - Jouez Mighty dans Sonic 1, par E-122 Psi.
- [Ray in Sonic the Hedgehog](https://www.sonichacking.org/entries/expo2019/65) - Jouez Ray, avec son gameplay Mania, par Iso Kilo.
- [Somari the Adventurer](http://info.sonicretro.org/Somari_the_Adventurer) - Jouez Mario dans Sonic 1, par Lone Devil.
- [Motobug the Badnik in Sonic the Hedgehog](http://info.sonicretro.org/Motobug_the_Badnik_in_Sonic_the_Hedgehog) - Jouez a Motobug dans Sonic 1, par Polygon Jim.
- [Shadow the Hedgehog in Sonic the Hedgehog](http://info.sonicretro.org/Shadow_the_Hedgehog_in_Sonic_the_Hedgehog) - Jouez Shadow dans Sonic the Hedgehog, par Falavia ! (Façon Modern)
- [(Another) Shadow the Hedgehog in Sonic the Hedgehog](https://sonichacking.org/entries/contest2019/22) - Jouez Shadow dans Sonic the Hedgehog, par AsuharaMoon ! (Mania-like)
- [Rouge the Bat in Sonic the Hedgehog](https://www.sonichacking.org/entries/expo2019/24) - Rouge dans Sonic 1, par AsuharaMoon.

### Sonic the Hedgehog 2

- [Amy Rose in Sonic the Hedgehog 2](http://info.sonicretro.org/Amy_Rose_in_Sonic_the_Hedgehog_2) - Jouez Amy (Classique) dans Sonic 2, par E-122 Psi.
- [Sonic the Hedgehog 2 : Pink Edition](http://info.sonicretro.org/Sonic_the_Hedgehog_2:_Pink_Edition) - Jouez Amy and Cream (Modernes) dans Sonic 2, par E-122 Psi.
- [Sally Acorn in Sonic the Hedgehog 2](http://info.sonicretro.org/Sally_Acorn_in_Sonic_the_Hedgehog_2) - Jouez Sally dans Sonic 2, par E-122 Psi.
- [Kirby in Sonic the Hedgehog 2](http://forums.sonicretro.org/index.php?showtopic=34887) - Jouez Kirby dans Sonic 2, par LoneDevil.
- [Metal Sonic in Sonic the Hedgehog 2](http://info.sonicretro.org/Metal_Sonic_in_Sonic_the_Hedgehog_2) - Jouez Metal Sonic dans Sonic 2, par Lone Devil.


### Sonic the Hedgehog 3 & Knuckles

- [Sonic the Hedgehog 3 and Amy Rose](http://forums.sonicretro.org/index.php?showtopic=28820) - Jouez Amy dans Sonic 3&K, par E-122 Psi.
- [Sonic 3 Cz](http://info.sonicretro.org/Sonic_3_Cz#History) - Jouez Amy dans Sonic 3&K, par Hayate.
- [Sonic the Hedgehog 3 and Sally Acorn](http://forums.sonicretro.org/index.php?showtopic=34300) - Jouez Sally dans Sonic 3&k, par E-122 Psi.
- [Metal Sonic in Sonic the Hedgehog 3 & Knuckles](http://info.sonicretro.org/Metal_Sonic_in_Sonic_the_Hedgehog_3_%26_Knuckles) - Jouez Metal Sonic dans Sonic 3&K, par Lone Devil.

## Autres

- [Sonic the Hedgehog Vol.2](https://www.romhacking.net/reviews/5626/) - Une version améliorée de Somari
- [Cream in Mario 64](https://sm64pc.info/forum/viewtopic.php?t=132&i=1)