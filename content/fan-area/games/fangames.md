---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Fangames
  parent: Fangames et mods
  order: 1
---

# Fangames

Voici une liste de fangames sympa basé sur Sonic. Le but est d'avoir des fangames qui sont "complet" ou "assez complet", ce qui veut dire qu'ici vous trouverez que des expériences considéré comme suffisamment complète pour être jouée. Si ce sont principalement des titres 2D, vous trouverez ici plusieurs jeux intéressant 3D !

La plupars de ces jeux nécessite Windows ou une plateforme de compatibilité Windows comme Wine (directement ou via un launcher comme Bottles) pour être joué. Certains sont aussi compatible avec Linux.

## Megadrive-like

- [Sonic Axiom](https://forums.sonicretro.org/index.php?showtopic=26434) - Un fangame inspiré de CD, par Vexer.
- [Sonic Classic](https://info.sonicretro.org/Sonic_the_Hedgehog_Classic) - Un fangame style retro, par Hez.
- [Sonic Classic 2](https://sonicfangameshq.com/forums/showcase/sonic-classic-2.964/) - La suite de Sonic Classic, par Hez.
- [Sonic Before the Sequel](https://info.sonicretro.org/Sonic_Before_the_Sequel) - Un préquel de Sonic 2, par Lakefeperd.
- [Sonic BtS Aftermath](https://sites.google.com/site/sonicbts/aftermath) - Une suite de Sonic BtS, et prédécesseur spirituel de Sonic Chrono Adventure, par Lakefeperd.
- [Sonic After the Sequel / AtS DX](https://info.sonicretro.org/Sonic_After_the_Sequel) - Une suite de Sonic 2, par Lakefeperd.
- [Sonic Chrono Adventure](https://sites.google.com/site/sonicchronoadventure/) - Un fangame Metronic, par Lakefeperd.
- [Sonic Time Twisted](https://info.sonicretro.org/Sonic_Time_Twisted) - Une suite spirituelle de CD, par Overbound et la Time Twisted Team.
- [Sonic 3D in 2D](https://sotaknuck.itch.io/sonic3d2d) - Une reimagination de Sonic 3D Blast avec une structure similaire à S3&K, par Sotaknuck.
- [Sonic and the Fallen Star](https://stardropsmh.github.io/sonic-and-the-fallen-star/) - Un jeu Sonic avec une esthétique rêve/mid-90, par Stardrop.
- [Sonic Triple Trouble 16-bits](https://gamejolt.com/games/sonictripletrouble16bit/322794) - Un remake 16-bit de Sonic Triple Trouble façon suite de S3&K, par NoahNCopeland.
- [Sonic UltraSaturn](https://sonicfangameshq.com/forums/showcase/sonic-ultrasaturn.2059/) - Un jeu court typé Saturn, par Un Sonic Ryu.

## Jeux modernes façon MD

- [Sonic Frenzy Adventure](https://gamejolt.com/games/sfa/533938) - Un fangame Adventure-like, par Frenzy-kun.

## 8-bit

- [Sonic Blasting Adventure](https://gamejolt.com/games/sonic_blasting_adventure/252769) - Un remake des fangames bootleg gameboy, par GustavoCazonato.
- [Sonic SMS Remake](https://gamejolt.com/games/sonicsmsremake/639432) - Un remake de Sonic 1 8bits, avec des niveaux et personnages supplémentaires, par CreativeAraya.
- [Sonic SMS Remake 2](https://gamejolt.com/games/sonicsmsremake2/639487) - Un remake de Sonic 2 8bits, ou Tails n'est plus capturé, en équipe de 2 avec de nouveaux personnages en plus, par CreativeAraya.
- [Sonic SMS Remake 3](https://gamejolt.com/games/sonicsmsremake3/766150) - Un jeu mélangeant des éléments de Sonic Chaos et Sonic Triple Trouble, formant un tout nouveau jeu. Se joue avec des équipes de trois personnages, par CreativeAraya.

## Advance-like

- [Sonic Fusion](http://forums.sonicretro.org/index.php?showtopic=20641&st=0) - Sonic Adventure-inspired fangame en 2D, par Felik.
- [Neo Sonic Universe](http://soahcity.com/forums/index.php?/topic/104-neo-sonic-universe/) - Un retrofangame inspiré Sonic Advance, par Alexandre Martins.
- [Ultimate Flash Sonic](http://www.allsonicgames.net/ultimate-flash-sonic.php) - Un petit fangame Sonic en flash, par dennis_g.
- [Sonic 2K6 2D](https://www.youtube.com/watch?v=bX2bgVWzHCs) - Un Sonic 2006 en 2D de 3 niveaux, par BlazeHedgehog.
- [Knuckles’ Halloween Tale](https://nefault1st.wordpress.com/downloads/knuckles-halloween-tale-halloween-special/) - Un fangame de chasse au trésor style Halloween, par Nefault1st.

## 3D Classic

- [Classic Sonic Adventure](http://shad-unit.webs.com/sonic3dadventure.htm) - Un fangame 3D classique, par Chishado Games.
- [Sonic Hexacide](https://phpbb.mfgg.net/viewtopic.php?f=8&t=17186) - Un mélange entre Xtreme et Lost World, par DJ Coco.
- [Sonic Robo Blast 2](http://srb2.org/) - Un Fangame 3D basé sur Doom, par la Sonic Team Junior.
- [SRB2 Top Down](https://mb.srb2.org/showthread.php?t=42317) - SRB2 × Super Mario 3D Land, par le Kart Krew.
- [SRB2 Kart](https://mb.srb2.org/showthread.php?t=43708) - SRB2 × Mario Kart, par le Kart Krew.
- [Dr Robotnik's Ring Racers](https://www.kartkrew.org/) - La suite de SRB2 Kart, par le Kart Krew.
- [Sonic Revert](https://taldius.itch.io/sonic) - Un fangame de course multijoueur, par Taldius.

## 3D Modern

- [Sonic Souls](https://www.youtube.com/watch?v=b6KdeXxMOrA) - Un fangame 3D avec Sonic GDK.
- [Sonic World](https://sonicworldfangame.com/) - Un fangame 3D avec pleins de niveaux, par Ozcrash et l'équipe Sonic World.
- [Sonic Incursion](https://forums.sonicretro.org/index.php?threads/sonic-incursion-released.32866/) - Un fangame 2.5D ou vous affrontez l'Empire Metallix, par Ell678.
- [Sonic GT](https://gamejolt.com/games/Sonic-GT/533291) - Un fangame 3D de vitesse et exploration, par [NSGreedy](https://twitter.com/NSGreedy)
- [Project P-06](https://www.youtube.com/watch?v=SCD5aQLlCD4) - Une recréation améliorée de 2006, par ChaosX
- [Sonic Adventure Blast 2](https://sonicfangameshq.com/forums/showcase/sonic-adventure-blast-2.2157/) - Jeu utilisant le gameplay Adventure, par ChubbaBubba.

### Openworld Fangames

- [Green Hill Paradise](https://www.youtube.com/watch?v=C0IsSeE_ckI) - Un jeu d'un niveau openworld, par SuperSonic68.
- [Green Hill Paradise - Act 2](https://www.youtube.com/watch?v=6nmokhPUCQ0) - La suite de Green Hill Paradise, par SuperSonic68.

## Other Playstyles

All games with non-standard gameplay

- [Eggman hates furries](https://gamejolt.com/games/eggman-hates-furries/4110) - Un boss-rush avec une ambiance unique, par Oddwarg.
- [Sonic & Knuckles Flicky Panic](https://gamejolt.com/games/sonic-knuckles-flicky-panic/147734) - Un puzzle-plateformer à écran fixe, par Lionheart.
- [Sonic & Knuckles Double Panic](https://gamejolt.com/games/sonic-knuckles-double-panic/148224) - Un autre puzzle-plateformer à écran fixe, par Lionheart.
- [Cheese Megadventure](https://gamejolt.com/games/cheese-megadventure/147783) - Un petit puzzle-plateformer avec Cheese, par Lionheart.
- [Chao World 2](https://gamejolt.com/games/chao-world-2/147721) - Un petit puzzle-plateformer de Chao, par Lionheart.

### Chao

- [Chao Resort Island](https://nefault1st.wordpress.com/downloads/chao-resort-island/) - Un fangame d'élevage de chao, par Nefault1st.

### Meme et Hmour

- [Sunky.MPEG](https://gamejolt.com/games/sunky-mpeg/78243) - Une parodie funkie de Sonic.exe, par LooneyDude.
- [Silly.TIFF](https://gamejolt.com/games/silly-tiff/78244) - Une suite de Sunky.mpeg et parodie de sally.exe, par LooneyDude.
- Sunky the Game [Part 1](https://gamejolt.com/games/sunky-the-game-part-1/78242), [Part 2](https://gamejolt.com/games/sunky-the-game-part-2/78246), [Part 3](https://gamejolt.com/games/sunky-the-game-part-3/115474) - Une parodie de Sonic en trois partie, par LooneyDude.
- [Sunky the Game 2](https://gamejolt.com/games/sunky-the-game-2-april-fools-2016/137699) - Suite à Sunky the Game, par LooneyDude.
- [Sonic Boom and the Smash Crew](https://sonicfangameshq.com/forums/showcase/sonic-boom-and-the-smash-crew.85/) - Un clone de Smash Bros avec plus ou moins des personnages de Sonic Boom, par MetalSonic3.