---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Sonisphère
  key: Sites anglophones
  order: 1
---

# Sites anglophones

La plupars des sites Sonic sont anglophones. Pour celleux qui peuvent parler anglais, vous pourrez trouver de nombreux contenus intéressants dans ces différents sites.

## Generalist communities

- [Tails Channel](https://tailschannel.com/)
- [Sonic Stadium](https://www.sonicstadium.org/)
- [Soah City](https://soahcity.com/)
- [Sonic News Network](https://sonic.fandom.com/wiki/Sonic_News_Network)
- [Sonic City](https://sonic-city.net/)

## Specific subjects

- [Chao Island](https://chao-island.com/)
- [Fans United for SatAM](https://www.sonicsatam.com/)
- [Secrets of Sonic Team](http://sost.emulationzone.org/)
- [Sonic Stuff Research Group](https://sonicresearch.org/community/index.php)

## Fangames & Hacks

- [Sonic Retro](https://sonicretro.org/)
- [SFGHQ](https://www.sonicfangameshq.com/)
- [SAGExpo](https://sagexpo.org/)
- [Sonic Hacking Contest](https://shc.zone/)
- [SRB2 Community](https://www.srb2.org/)

## Subreddits

- [/r/SonicTheHedgehog](https://reddit.com/r/SonicTheHedgehog/)
- [/r/MoonPissing](https://reddit.com/r/MoonPissing/)

## SEGA websites

- [SMS Power](https://www.smspower.org/)
- [SEGABits](http://segabits.com/)
- [SEGA Retro](https://www.segaretro.org/)
- [SEGA-16](http://www.sega-16.com/)
