---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Sonisphère
  key: Sites francophones
  order: 0
---

# Sites francophones

Quelques communautés et websites Sonic intéressant en langue française. Le but de cette page est notamment de permettre à la communauté francophone d'être plus connecté, et potentiellement à terme de dépasser les différentes division qui peut y exister.

## Communautés

- [Planète-Sonic](https://planete-sonic.com) - La plus grande est la plus ancienne communauté francophone Sonic. Elle privilégie souvent la qualité de l'information, évitant les rumeurs quand elle peut.
  - [La radio Planète-Sonic](https://radio.planete-sonic.com/) - Une radio 24/24 Sonic, diffusant des musiques de la série
  - [Le Discord Planete-Sonic](https://discord.gg/8ZdCkWXgNc) - Discord de la communauté Planète-Sonic.
  - [Le Discord Chaos Dimension](https://discord.gg/ytpsAGt) - Un Discord de RolePlay Sonic, animé par la communauté Planète-Sonic
- [Sonic Community FR](https://twitter.com/Sonic_Community) - Une communauté plus orienté multi-media
  - [Discord SC-FR](https://t.co/SfZclHXv5B) - Le Discord de Sonic Community FR. Ce Discord possède des règles plutôt stricte, exigeant notamment (à ma connaissance) d'être actif pour pouvoir rester dessus.
  - [Youtube SC-FR](https://www.youtube.com/watch?v=yLh0NWnCf-Y) - Chaine youtube de SCFR
- [Sonic Online](https://www.soniconline.fr/) - Autre communauté francophone Sonic.

## Youtubeurs Sonic

- [Veyle](https://www.youtube.com/@Veyle_)

## Traduction

- [ChaosTrad](https://chaostrad.fr/) - Équipe de traduction de projets Sonic
- [Sonic Mag 2](https://sonicmag2.planete-sonic.com/) - Site de scanlation ArchieSonic

## Communautés SEGA

- [SEGA Mag](https://www.sega-mag.com/)