---
layout: layouts/base-toppages.njk
eleventyNavigation:
  key: Sonisphère
  parent: La fan-area
  order: "5"
---

# Sonicsphère

La sonisphère est l'ensemble des sites lié à la série Sonic.

La fanbase Sonic est pleine de fansites et communauté cool, qui peuvent être sympa à linker et faire découvrir. Mon but dans ces pages est de permettre d'en découvrir.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Critères de choix

- Ces sites/communautés doivent contribuer ou fournir un espace pour une communauté Sonic.
- Les sites web qui ne sont plus actif sont quand même les bienvenues. Parce qu'ils peuvent avoir des informations et intéressantes.
- J'essaie de ne pas rentrer dans les disputes et les dramas de communautés et de "qui apprécie qui"
  - Cela dit, j'essaie d'éviter les communautés mettant d'autres personnes en danger ou faisant preuve de point de vues oppressifs
- J'ai pas pu mettre les sites si je connais pas leur existence. C'est un peu évident, mais j'ai loupé pleins de sites web cool !

