---
layout: layouts/base.njk
eleventySubNavigation:
  key: Webcomics
  parent: Autres médias
  order: 4
---

# Webcomics

Quelques comics publiés sur le web parlant de notre hérisson bleu préféré. Contrairement aux autres listes, ici on peut trouver des éléments trèèès loin d'être "finaux", puisqu'ils ont pour but d'être une série continue.


## Fan-continuations

- [ArchieSonic Online](http://archiesoniconline.com/) - Une fan-continuation de la première continuité du comics.
- [Sonic the Comic Online](http://www.stconline.co.uk/) - Une fan-continuation de Sonic the Comic.
- [Sonic SatAM S3ason](http://www.sonicsatam.com/sea3on/) - Une fan-continuation de Sonic SatAM en webcomic.

## Ongoing comics

- [Sonic Legacy](https://tapas.io/series/Sonic-Legacy) - Une série de comics par Sonic Paradoxe.
- [Gosth of the Future](https://www.deviantart.com/evanstanley/gallery/) - Une série de comics ayant un focus sur Silver, par Evan Stanley.

## Game Adaptation

- [Sonic Adventure 2](https://board.sonicstadium.org/topic/20105-sonic-adventure-2-comic/) - Une adaptation de Sonic Adventure 2 en comics (un seul numéro fini).
