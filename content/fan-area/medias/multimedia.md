---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Multimedia
  parent: Autres médias
  order: 6
---

# Multimedia

Tout un tas de média Sonic, avec de la vidéo et même un drama audio !

## Movies

- [Sonic](https://www.youtube.com/watch?v=JtCtQpclpY8) - Un fanfilm avec live-action et CGI, par BlueCore Studios
- [Sonic Prologue](https://www.youtube.com/watch?v=9C1ryUfXnR4) - Une préquel en animation à Sonic 2, par NECOMIX et Chris Wilcots Animation.
- [Nazo Unleashed DX](https://www.youtube.com/watch?v=g-etGCr7SM4) - Une fan-animation réutilisant le personnage de Nazo de Sonic X.

## Humorous videos

- [Sonic Shorts](https://www.youtube.com/playlist?list=PLPefo9JATa-uoXMO4mf4uZTBBCNfrvh-C) - Une série de petite animations courtes, par le collectif Sonic Paradoxe.
- [Sonic Seconds](https://www.youtube.com/playlist?list=PLPefo9JATa-tSp8mlSb2Vvfn-TtpUUEo-) - Une autre série de petite animations (plus) courtes, par le collectif Sonic Paradoxe.
- [Seaside Denied](https://www.youtube.com/watch?v=veLTMMiDjh8) and [Metropolis Mayhem](https://www.youtube.com/watch?v=bSj7yRyPWaQ) - Une parodie musicale des deux premiers niveaux de Sonic Heroes, par le collectif Sonic Paradoxe.

## Drama radio

- [Sonic and Tails R](https://www.youtube.com/playlist?list=PLVucl6B8OcBPCfQdgCMGUUFxv2Y7jl5_s) - A Sonic Radia Drama, by Emi Jones and Doryan Nelson, with several cast members from past and present Sonic games !
