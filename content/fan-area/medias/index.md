---
layout: layouts/base-toppages.njk
eleventyNavigation:
  parent: La fan-area
  key: Autres médias
  order: "3"
---

# Autres médias

Si les projets Sonic tournent surtout autour des jeux, bien d'autres médias sont présents dedans. Les fans ont créé un peu de tout, et le but est de montrer tout ce qu'on peut trouver en terme de multimedia créé par des fans autour de Sonic.

Comme toujours, ces pages ne sont pas exhaustive, et bien des autres créations cool existent !

## Types de medias

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Classification

- Webcomics
  - Fan-continations (continuation d'une continuité ou d'une série Sonic)
  - Multiples numéros
  - One-shot
  - Humour
- Videos
  - Fan-continuations
  - Film (Projet qui se comporte comme un court/moyen/long métrage)
  - Anime (projet inspiré par l'animation japonaise)
  - Cartoon (projet inspiré par l'animation occidentale)
  - Humour