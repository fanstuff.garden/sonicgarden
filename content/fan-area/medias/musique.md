---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Fan-musiques
  parent: Autres médias
  order: 4
---

# Fan-musiques

La musique de Sonic est connue et réputée, et celle des fans n'est pas en reste ! Le but de cette page est de vous faire découvrir des artistes cool qui créés de la musique pour notre rat bleu national.

A noter que les musiques listées ici ne contiennent que des albums complet et des musiques originales (que ce soit des lyricisations ou des toutes nouvelles musiques). Il y a tellement de bonnes cover que je ne peut pas vraiment toute les ajouter. Il y aura peut-être quelques *artistes* produisant des fan-cover Sonic, mais pas de listing individuels des musiques.

## Chansons originales

- [Blink of an Eye](https://www.youtube.com/watch?v=E-5GSBussjc) - Par Victor McKnight, Chi-chi, & SquigglyDigg.
- [All my Fear](https://www.youtube.com/watch?v=xUXQYltmW0g) - Par Victor McKnight, BillyTheBard11th, & Plexsy.
- [The End of All Things](https://youtube.com/watch?v=oqCmGUzItu4) - Par LongestSoloEver & LaceyJohnsonMusic.

## Lyricisation

- [Mania](https://www.youtube.com/watch?v=pxiWVTH0Jdk&t=17) - Par Hyper Potion & Skye Rocket.
- [SONIC MANIA Opening](https://invidious.fdn.fr/watch?v=HHill_kR-FQ) - Par Hyper Potion & Chi-Chi.
- [Les aventure de Sonic](https://invidious.fdn.fr/watch?v=cfUCD2tT6A4) - Par Kickban & Alexis Tomassian.

## Album de cover

- [S&K Project Chaos](https://ocremix.org/album/9/sonic-3-and-knuckles-project-chaos)
- [Sonic CD : Temporal Duality](https://soniccd.ocremix.org/)
- [Sonic Stadium Music Album 2011](https://projects.sonicstadium.org/music-album-2011/)
- [Sonic Stadium Music Adventure 2012](https://projects.sonicstadium.org/music-album-2012/)
- [The Sound of TSS '14](https://projects.sonicstadium.org/sound-of-tss-2014/)
- [The Sound of TSS '15](https://projects.sonicstadium.org/sound-of-tss-2015/)
- [Sonic & Chill](https://www.youtube.com/watch?v=k6oEC4fpBhQ)

## Fan-soundtracks

- [Sonic Before the Sequel OST](https://www.youtube.com/channel/UCTHhYeeRUKWZ90Z7ngwjgFg?disable_polymer=1)
- [Sonic After the Sequel OST](https://www.youtube.com/user/SonicATS13?disable_polymer=1)
- [Dual Ages : Time Twisted OST](https://www.youtube.com/playlist?list=PLKYFFo0H3JcxYdJy3MIdBZt4x3XukxXCt&disable_polymer=1)
- La série d'album Sonicesque, inspiré de la bandeson des jeux classiques, par Karl Brueggemann
  - [Sonicesque Vol. I](https://supermarcatobros.bandcamp.com/album/sonicesque-vol-i)
  - [Sonicesque Vol. II](https://supermarcatobros.bandcamp.com/album/sonicesque-vol-ii)
  - [Sonicesque Vol. III](https://supermarcatobros.bandcamp.com/album/sonicesque-vol-iii)
  - [Sonicesque Vol. VI](https://supermarcatobros.bandcamp.com/album/sonicesque-vol-iv)