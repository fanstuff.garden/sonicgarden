---
layout: layouts/base.njk
eleventyNavigation:
  key: La fan-area
  order: 3
---

# La fan-area

Les fans de Sonic sont connus pour leur créativités : nous avons eut de nombreux jeux, animations, et autres éléments créé autour de Sonic. Le but de cette catégorie est de célébrer un peu cela, et de faire connaitre les contenus cools de fans.

L'idée n'est pas de dire que tout doit être au niveaux de ces créations, juste d'en présenter quelques cools. De plus, il est important de comprendre qu'elles ne seront pas toujours parfaite, et auront souvent des défauts : elles sont souvent faits par des amateurs qui apprennent en faisant cela. Le but de cette section est de célébrer la créativité, pas de se dire que tout doit être parfait.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, false, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Règles d'ajout

- Il s'agit d'une création ou site basé sur la série Sonic the Hedgehog. Des hacks/mods qui rajoutent des personnages d'une autre franchise dans un jeu Sonic sont accessible. De la même manière, des jeux faisant du Sonic à partir d'autres jeux seront acceptés également.
- Les créations doivent être *suffisemment complètes* pour être une bonne expérience. Il peut s'agir de soit un petit projet (des hacks/fangame à un-niveau, des comics d'un numéro) ou des projets de proportion plus épique.
- Quelques exceptions à cette règle inclus :
  - Des jeux abandonnés qui ont une expérience suffisamment complète à la Sonic Fusion
  - Des séries de comics constamment en cours, qui ne seront jamais "terminées". Pour qu'un soit proposé, il doit avoir un numéro.
  - Des jeux basées sur le modding comme SRB2 ou Sonic World