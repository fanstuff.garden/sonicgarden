---
layout: layouts/base-toppages.njk
eleventyNavigation:
  key: Jeux non-sortis
  parent: Secrets et mystères
  order: 0
---

# Jeux non-sortis

De nombreux projets de jeux Sonic ont été lancé mais ne sont jamais sortis, le but de cette page est de permettre de les conserver et d'en découvrir plus sur elles. Certains projets assez grands auront des pages complètes pour eux. Des informations et certianes captures d'écran sont reprise du vieux site [Secret of Sonic Team](http://sost.emulationzone.org) ou du wiki de [Sonic Retro](https://sonicretro.org).

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## SegaSonic Bros.

SegaSonic Bros. est un puzzle game d'arcade conçu par Fukio Mitsuji, le créateur de Bubble Bobble, qui devait sortir en 1992. Le jeu est quasiment complet, mais n'a pas été déployé sur le japon entier, à cause d'une run de test non-satisfaisante. Il tournait sur un hardware très proche de la Mégadrive.

![Ecran titre de SEGASonic Bros.](/img/articles/prototypes/segasonicbros.png)

Le jeu consiste à faire tomber des Sonic colorés qui tombe par bloc de quatre afin de vider l'écran avant que les blocs atteignent le haut de l'écran. Le jeu possède trois couleurs les 30 premiers niveaux, puis quatre (avec une couleur blanche en plus) après. Tout les dix niveaux (les niveaux s'obtenant au score à la manière d'un tétris), le joueur obtient une chaos emerald et l'environnement de jeu change, passant par tout les niveaux de Sonic 1.

Une [rom du prototype](https://hiddenpalace.org/SegaSonic_Bros._(prototype)) est disponible, et a même [été converti sur Megadrive](https://mega.nz/#!eH5ghKhb!YlxdWnLpPPVNpjVE34au0Y9n-1Y_lptnXwTq_Ptrv70).

## SEGAPede

SEGAPede est un jeu qui a été pitché à SEGA of America après le développement de Sonic Spinball par Craig Stitt et Ken Rose. Ce jeu devait se produire originellement dans l'univers de Sonic, et on devait y jouer un badnik, Zip, qui avait un abdomen composé de pods avec différents power-ups qui devait pouvoir se disperser comme les anneaux de Sonic quand on prenait un coup. Le but du jeu dans cette première réflexion aurait été de sauver le Dr. Robotnik en récupérant nettoyant et récupérant une entité nommée "Chaos Dust".

![L'écran titre de segapede, avec une sorte de mille-patte robotique et le titre](/img/articles/prototypes/segapede1.png)

Le liens avec la série Sonic a rapidement été abandonné, mais le concept est resté similaire, et le jeu a finalement été abandonné après 14 mois, pour se concentrer sur des projets comme Comix Zone. La rom du prototype (qui utilise des assets d'Hidden Palace de Sonic 2) est disponible sur [Hidden Palace](https://hiddenpalace.org/Segapede_(Prototype)).

![Un niveau de segapede avec une sorte de mille-patte robotique dans un environnement de grotte magique (hidden palace zone de Sonic 2)](/img/articles/prototypes/segapede2.png)

## Sonic DS

Sonic DS était un prototype de jeu Sonic exploitant les capacités de la DS, ou on faisait courir Sonic de plus en plus vite en frottant l'écran de la DS. Le concept est proche de ce qui sera fait avec des mini-jeux dans Project Rub, mais ici centré sur le fait de faire courir Sonic.

<iframe class="youtube-video" src="https://www.youtube.com/embed/YogdnPhHRec?si=w3QezoO2DQmOuuU2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Sonic Extreme

> **Note :** À ne pas confondre avec [Sonic Xtreme](/secrets/xtreme/)

Sonic Extreme était un projet de jeux de skateboard Sonic. Il pourrait être à l'origine de Sonic Riders, mais son gameplay est plus proche d'un jeu de skate traditionnelle que du jeu de course à haute vitesse qu'est Sonic Riders.

<p><img width="50%" height="50%" src="/img/articles/prototypes/sonicextreme.png" alt="Ecran titre de Sonic Extreme" /></p>

Le jeu offrait des missions, et un système de tricks rappellant les jeux à la Tony Hawk et tout. Le jeu est très proche du jeu Tech Deck: Bare Knuckle Grind. Le prototype offrait comme prototype.

<iframe class="youtube-video" src="https://www.youtube.com/embed/XKgSOqS2g1I?si=W4yg2DI9QR0lspSW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Sonic Demo

Sonic Demo était une démo pour faire un jeu mobile Sonic proposé à Nitrome, visant à offrir un gameplay proche de leur jeu Super Leap Day (qui offre des challenges de plateforming quotidien). Le prototype n'a pas été accepté par SEGA et le projet n'a pas continué.

<div class="container"> 
  <iframe class="youtube-video" src="https://www.youtube.com/embed/s_AUhsbHhRA?si=3LVHRvTKEqwZc5_e" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

## Autres titres

- Des portages Mega-CD de Sonic 1 & et Sonic 2 ont été prévus à une époque
- **Sister Sonic** était un projet de rebranding Sonic du jeu *Popful Mail* qui devait remplacé le cast par des membres de la famille de Sonic, dont une soeur de Sonic. Le projet sera abandonné suite à de nombreuse lettre en colère et remplacé par un portage direct.
- **Sonic the Hedgehog 3 Limited Edition** était un projet de version standalone de Sonic 3 & Knuckles sur Megadrive.
- **Sonic Sports** était un projet de titre sportif ou il y aurait eut au moins Sonic, Knuckles et Ristar de jouable.
- Une version gba de **Sonic Riders** était prévue avec un gameplay à la Outrun.
- **Sonic the Hedgehog : Awakening** était un projet de potentielle suite à Sonic 2006, révélé uniquement via le CV de Pete Capella (voix de Silver dans 2006)