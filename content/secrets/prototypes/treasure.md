---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  key: Treasure Tails
  parent: Jeux non-sortis
  order: 2
---

# Treasure Tails

Treasure Tails était un projet de plateformer-puzzle qui aurait été centré autour de Tails, prévu pour le Noël 94 et développé de Décembre 92 à Avril 93. Il sera annulé sans cérémonie.

<p><img width="50%" height="50%" src="/img/articles/prototypes/treasuretails.png" alt="Capture d'écran d'un prototype ou mockup de Treasure Tails" /></p>

Aucun prototype n'a leaké et il n'existe pour l'instant aucune recréation du jeu où moyen d'y jouer, seulement des interviews et quelques images

## Gameplay

Le gameplay exacte du jeu n'est pas connu et ne peut être que spéculé.

<p><img width="50%" height="50%" src="/img/articles/prototypes/treasuretails2.png" alt="Capture d'écran d'un prototype ou mockup de Treasure Tails" /></p>


De ce qu'on peut voir, Tails se déplaçait dans un environnement en vue de côté, mais avec un peu de 3D isométrique, avec plusieurs types de tuiles. Certaines semblent montrer des trous, des tuiles pouvant s'effondrer sous nos pieds. La présence de grandes zones enflammée sur certaines captures d'écrans semblent indiquer que le vol de Tails était utilisé par moment.

## Historique du projet

Le jeu était visiblement prévu pour le Noël 94, et a été développé sous la forme d'un petit jeu qui aurait pu compléter le lineup de ce Noël. Nous avons plus d'informations dans une interview de l'artiste, Craig Stitt :

> Ah oui, SEGA of America avait demandé à STI un jeu Tails, avec une équipe majoritairement, voire totalement américaine. Il s'agissait d'un jeu d'aventure et de plateforme isométrique. J'étais le seul artiste et j'ai travaillé dessus de décembre 1992 à avril 1993. Tails Treasure devait être un jeu court et simple car SEGA le voulait pour le prochain Noël. Je dois encore consulter encore mes notes pour savoir qui a travaillé sur ce jeu, combien de temps il a duré et qui s'occupait de la programmation et de la conception du jeu. Le jeu a été "reporté" parce que ceux d'entre nous qui y travaillaient devaient se consacrer à d'autres projets. J'ai continué avec Sonic Spinball
>
> *--Craig Stitt, dans une interview pour [Genesis Temple](https://web.archive.org/web/20230603144815/https://genesistemple.com/interview-with-craig-stitt-from-sonic-to-spyro)* 