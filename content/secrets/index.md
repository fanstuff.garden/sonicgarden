---
layout: layouts/base.njk
eleventyNavigation:
  key: Secrets et mystères
  order: 2
---

# Secrets et mystères

Le but de cette catégorie est de parler des éléments de la franchise Sonic qui existent plus "du côté des fans". Il peut s'agir de toutes les discussions ou créations autour des glitchs de la franchise Sonic, d'éléments "perdu", de vieilles rumeurs, et d'éléments "perdus" de la franchises.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>