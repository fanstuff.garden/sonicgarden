# Personnages glitches

Sonic a eut de nombreux glitches dans les jeux, et s'ils sont moins célèbres que ceux de Pokémon, certains ont quand même eux une certaine célébrité dans le fandom. Le but n'est pas ici de parler de ceux "game-breaking", pas de trucs genre "j'ai traversé le sol à cet endroit" ou "ce script ne s'est pas déclenché", mais plus de certains qui ont cristalisée l'imagination des fans : les "personnages glitches".

Je vais aussi parler de personnages vu à partir "d'erreur de vision", tel qu'un sprite à l'envers qui donne l'impression de voir un autre personnage.

## Ashura (Glitch Sonic)

Ashura est le surnom donné à un bug de Sonic 2, permettant de faire apparaitre un Sonic de couleur différent dans les niveaux du jeu, notamment Emerald Hill. Il est devenu extrèmement célèbre, et est devenu un "fan-personnage" extrèmement connu et populaire.

### Scourge et Surge

## Wechnia (\*\*\*\*\*\*\*\*\*\*)

### Finitevus et Starline

## Red Sonic & Blue Knux

## Eggette / Omelette

Ce personnage n'est pas trop un glitch.

Chose amusante, Eggman recevra une fille quelques années après tout ça, en tant que Sage. Il n'y a à ce jour aucune confirmation ou information d'un rapport avec le personnage d'Omelette.