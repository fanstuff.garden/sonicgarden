---
layout: layouts/base-toppages.njk
eleventyNavigation:
  key: Sonic Xtreme
  parent: Secrets et mystères
  order: 1
---

# Sonic Xtreme

Sonic Xtreme est un projet de jeu qui devait être disponible sur SEGA Saturn et PC au milieu des années 90. A cause de complications dans le développement, de désaccords entre SEGA Japon et USA, et d'autres problèmes, il n'est jamais sorti. Il y a eu beaucoup de fascination pour ce jeu, et ici je vais essayer de montrer tout ce qui a été publié pour ce jeu, les fuites et les fan-remakes.

## Liste des pages

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Nouvelles capacités

Le jeu devait apporter selon [Secret of Sonic Team](http://sost.emulationzone.org/sonic_xtreme/whatis/index.htm) un certain nombre de pouvoirs à Sonic

- Le **SpinBash** - Une attaque rapide en avant, modifiée à partir du SpinDash.
- Le **SpinSlash** - Une attaque dans les airs attaquant tout autour.
- Le **lancé d'anneau** - Sacrifie un anneau pour le lancer en avant.
- La **Power Ball**  - Une attaque designé pour foncer sur les ennemis en dessous.
- Le **Super Bounce** - Un saut plus haut mais moins maniable.
- Le **Ring Shield** - Un bouclier qu'on obtient en sacrifiant des anneaux.
- Le **Sonic Boom** - Une attaque dans toute les directions en conjonction avec le Ring Shield (surement en le sacrifiant j'imagine ?)

## Liens et sources

- [Sonic Xtreme Compendium](http://scp.webulate.com/) - Version hébergée par Dav09
- [Sonic Xtreme Sprites](http://sost.emulationzone.org/sonic_xtreme/sprites/index.htm)
- [Sonic Xtreme TCRF Page](https://tcrf.net/Sonic_X-treme)

## Les prototypes leakés

Voici les différents prototypes et fuites que nous avons obtenus de Sonic Xtreme :

- [Sonic Xtreme v40 prototype](https://hiddenpalace.org/Sonic_X-treme_(V40_prototype))
- [Sonic Xtreme 714 prototype](https://hiddenpalace.org/Sonic_X-treme_(Jul_14,_1996_prototype)) - Un prototype par Point of View
- [Sonic Xtreme 718 prototype](https://hiddenpalace.org/Sonic_X-treme_(Jul_18,_1996_prototype)) - Un prototype utilisant le moteur de Boss de Chris Coffin
- [PC Restoration and assets](https://hiddenpalace.org/Sonic_X-treme_(PC_Restoration_and_Asset_Collection))
- [Sonic X-treme Package X Leak](https://archive.org/details/package-x)

## Remakes

De nombreux projets de fans ont eut pour but de tenter de recréer Sonic Xtreme, qui a pris un côté culte dans le fandom.

> **Note :** Pour l'instant, aucun projet n'a recréé Sonic Xtreme en tant que jeu complet, et tous sont inachevés.

- [Sonic Xtreme Remix](https://www.mediafire.com/file/u547usobiidx6jv/Sonic+Xtreme+Remix.zip/file) - Par [Dobermann Software](http://dobermannsoftware.net/)
- [Project AXSX](https://forums.sonicretro.org/index.php?threads/project-axsx-sonic-xtreme-and-sonic-cd-remix-a-2-5d-game.19713/)
- [Sonic Z-Treme](https://sonicfangameshq.com/forums/showcase/sonic-z-treme-saturn-sage-2018-demo.161/)
- [Sonic Z-Treme Metal Sonic Demo](https://sonicfangameshq.com/forums/showcase/sonic-z-treme-metal-sonic.851/) 
- [Project SXU](https://sonicfangameshq.com/forums/showcase/project-sxu.525/)
- [Sonic Xtreme](https://sonicfangameshq.com/forums/showcase/sonic-x-treme-sega-saturn.1383/) (qui est même sorti sur Saturn !)