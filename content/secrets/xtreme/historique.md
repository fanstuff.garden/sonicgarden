---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Sonic Xtreme
  key: Historique du projet
  order: 0
---

# Historique du projet

L'histoire de Sonic Xtreme est longue et complexe. Le but de cette page est d'en faire un résumé rapide, qui explique ce qui sont pour moi les causes de la perte du projet : une mauvaise maitrise du matériel, un environnement corporate compliqué, et des rivalités internes.

## Les tout débuts

En tout premier lieu, il faut savoir que Sonic Xtreme a été sur pas mal de support avant d'être sur la Saturn. Le projet a été prévu pour Megadrive (Le projet surnomé par des fans "Sonic-16", et un jeu en 3D isométrique), puis pour la 32X (Sonic Mars), pour un projet de console basé sur une carte NVidia de SEGA of America qui a été remplacé par la Saturn de SEGA of Japan, et finalement pour un combo Saturn-PC.

<!-- J'ai détaillé ces différentes premières périodes dans ma page "[Les tout débuts](/secrets/xtreme/debuts/)" -->

## Les deux moteurs

Le jeu était composé de deux moteur :

Le *moteur des niveaux* (ceux qu'on a retrouvé dans les différents release par Jollyjogger, les niveaux ayant déjà été leaké en 2009) qui utilise un système de bloc pour définir les niveaux, et à partir de certains build un système de fisheye afin d'offrir une meilleur visibilité sur les côtés. Il était également prévu que Sonic puisse courrir sur les murs et tout (grâce à un mode spécial en fait qui permettait ça). C'est également le moteur qu'on voit dans la pluparts des vidéos d'Xtreme (toutes celles montrant des niveaux) Ce moteur était développé par une équipe dirigée par Ofer Alon.

Le *moteur des boss*, qui est un moteur totalement différent, développé par Chris Coffin, et qui remonterait carrément à l'époque 32x du projet. On en possède qu'une démo, datant d'une époque bien ultérieur, c'est le fameux iso "718", où on retrouve aussi des assets des boss.

## Le développement

Pour ce qui s'est passé, en gros, le moteur d'Ofer Alon n'arrivait pas à tourner sous la Saturn, et avait une framerate très très basse. Du coup, il a fallut pas mal le retravailler. Le travail de programmation a alors été confié à une compagnie nommée Point of View, qui devait faire en sorte que le moteur des niveaux tourne sous la Saturn et tout. Si j'ai bien compris, le moteur est assez différent de celui d'Alon. On voit une partie de leur travail dans le post juste avant celui-ci. Résultat, on s'est limite retrouvé un peu avec deux équipes, Alon qui voulait continuer son moteur, et PoV, qui ont eut autant de mal qu'Alon. En Mars 1996, des représentatifs de SEGA sont arrivé pour voir le travail, et du coup on s'est retrouvé avec deux présentations, celle d'Alon et Chris Senn (le designer principal du jeu) qui représentait son moteur PC (je suppose que c'est de là que viennent toutes les vidéos "New World").

Quand le président Hayao Nakayama est arrivé, en gros il a vu le boulot de PoV, et il a été furieux (il était pas au courrant du coup que Alon avait un moteur sur PC qui était plus complet), parce que ça donnait l'impression qu'ils avaient pas du tout avancé. Il a du coup pointé le moteur des boss et à demandé à ce que tous le jeu soit comme ça. Si j'ai bien compris, quand Chris Senn est arrivé, non seulement c'était trop tard, mais il se serait énervé, un truc comme ça. Résultat, tout le boulot d'Alon est passé à la trappe. Ce qui me surprend un peu, c'est que le moteur PoV semble plus stable et plus fonctionnel que le moteur original (même si le moteur que l'on voit dans les vidéos semble encore plus fonctionnel). Je prends donc cette version avec un grain de sel.

## La fin du projet

Chris Coffin est devenu le programmeur principal à la place d'Alon, et ça a débuté la période ou Xtreme a été entièrement travaillé avec le moteur des boss (la période "Project Condor"). L'iso "718" viendrait de cette période là, et présente les débuts de cette conversion d'Xtreme au moteur des boss.

C'est à ce moment qu'ils auraient demandé le moteur de Night, parce qu'ils avaient besoin des outils de développements dedans. Cependant, ça aurait foiré, cette récupération ayant été fait dans le dos de Naka ce qui l'aurait énormément mis en colère. Il est cependant à noter que Yuji Naka a récemment déclaré que cela ne s'est jamais produit (et n'aurais jamais pu se produire car Sonic Xtreme a été écrit en C, et Nights en assembleur). 

Ensuite, Chris Coffin a choppé une pneumonie (il bossait comme un malade à cause de la deadline), et il a du quitter le projet. Et du coup sans développeur principal, le projet a fini par être abandonné.
