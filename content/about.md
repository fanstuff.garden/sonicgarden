---
layout: layouts/base.njk
eleventyNavigation:
  key: À propos
  parent: Accueil
  order: 0
---

# À propos

Sonic Garden est un website "oldschool" inspiré des shrines (des petits sites orienté sur un personnages ou un sujet) et des sites mid-2000 Sonic. Il est fait un peu "à l'ancienne" et vise à avoir un style un peu "documentation". Ce n'est pas un site très moderne ou y'aura des tas de trucs sociaux et tout, mais plus une sorte de concentration de pleins de petits trucs que j'ai fais durant les années. C'est un peu une sorte de gros "bloc-note" Sonic.

## C'est quoi ?

C'est une continuation d'un vieux projets nommé "project-mobius" où je voulait mettre toutes mes théories et points de vue sur Sonic the Hedgehog. Le vieux dépot a été retiré (de mon propre gré) dans un nettoyage de mon github entre 2016 et 2018, et du coup j'ai décidé recemment de transformer tout ça en un site web.

Globalement, c'est un braindump glorifié à propos d'un rat bleu qui court plutôt rapidement.

C'est vraiment aussi une sorte de référence par la vieille ère de fan-websites des années passées, avant que ces sites se solidifient en un petit set de site web plus large. Je n'ai pas pour but d'être en compétition avec ces sites avec mon petit site bizarre, mais plus de proposer quelque chose d'un peu différent, de plus "fait maison".

## Pourquoi ce site ?

Internet se concentre de plus en plus en quelques gros sites, laissant un peu de côté la diversité qu'on pouvait avoir à une époque. Je pense qu'une des bonnes choses sur internet est de permettre à des gens complètement amateur d'avoir leur propre site, et que ces sites peuvent prendre des formes très différentes.

De plus, de nombreux sites qui permettaient de découvrir des informations sur Sonic ont disparus (genre le Sonic Xtreme Compendium) ou risquant de disparaitre, ou les infos ne se trouvent que sur des wiki de fans qui sont cool mais sont souvent en anglais, et dans un format qui est plus prompt à contenir pas mal d'informations, mais où d'autres formats sont parfois plus prompt à la découverte (quitte à être moins complet que le wiki).

L'idée de ce site est donc d'être un site "espace de découverte" permettant de trouver des infos pour découvrirs certains aspect de Sonic, mais en étant :
- En français, pour les personnes ne lisant pas l'anglais
- Subjectif mais sans le cacher, sans tenter de fausse objectivité
- Gardant les informations et les contenus de certains vieux sites non-maintenus
- Référançant les autres sites et contenus en français quand pertinent.

C'est un peu un mélange entre une grosse awesome-list, de la documentation et un site retro à la manière des sites [neocities](https://neocities.org/). Il n'est pas en compétition avec les gros sites Sonic, mais est plus un petit espace complémentaire à côté.

## Y'a quoi ?

- Du Sonic (wow, qui s'y serait attendu ?)
- Des théories à propos du lore, de l'univers
- De la "rationalisation" de l'univers de Sonic
- Des listes, **pleins** de listes

## Crédits

- Site propulsé par [Eleventy](11ty.org)
- Le fond Station Square du header provient du vieux site Sonic Adventure sur Dreamcast
- Les artwork Sonic et Silver utilisé proviennent des fonds d'écran Sonic Channel