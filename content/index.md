---
layout: layouts/base.njk
eleventyNavigation:
  key: Accueil
  order: 0
---

# {{ metadata.title }}

Bienvenue sur {{ metadata.title }} !

Ce site web vise à être un site Sonic "oldschool", permettant de découvrir des trucs à propos de l'univers Sonic (ingame comme meta). Il va gagner de nouveaux sujets, suivant les choses dont j'ai envie de parler. Le but de ce site est d'être un peu plus orienté "documentation" (tandis qu'une expérience plus "discussion/blog" aurait besoin d'un site adaptés). Il est lié aussi à mes autres projets Sonic.

J'espère que vous aurez du fun sur {{ metadata.title }} !

## News Sonic

<iframe src="https://press.fanstuff.garden/iframe-sonic.php" height="180px" width="100%"></iframe>

## News SEGA

<iframe src="https://press.fanstuff.garden/iframe-sega.php" height="180px" width="100%"></iframe>
