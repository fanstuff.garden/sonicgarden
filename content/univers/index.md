---
layout: layouts/base.njk
eleventyNavigation:
  key: L'univers de Sonic
  order: 3
---

# L'univers de Sonic

L'univers de Sonic est très vaste, et comporte beaucoup d'histoire, surtout depuis que SEGA dans le début des années 2020 à décidé de créer un univers un peu plus transmédia, ayant à la fois des jeux, des dessins animés et des comics. Le but de cette catégorie est de présenter un peu ce qu'on peut trouver dans l'univers de Sonic, ainsi que quelques théories sur ledit-lore.

Le but est cependant pas d'être 100% "objectif", et va présenter des points de vues, des interprétations, etc.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>


## Le canon de Sonic

> Tout est canon (sauf Chronicles)

Les histoires de Sonic ont cette particularité qu'elles fonctionnent sur la base d'un "loose canon", c'est à dire d'un canon peu contraignant. Cela a un peu changé depuis les années 2020 puisqu'il y a maintenant une sorte de "comité canon et lore" dans la série Sonic, mais cela ne change pas ce fonctionnement principal de la série Sonic : le canon est créé par les différents jeux, et les jeux ne sont pas créés pour "faire avancer le canon". 

Chaque jeu Sonic (ou série, etc) part d'un concept, d'une idée et prend des éléments du "canon" pour créer son histoire, la plupart du temps en créant beaucoup d'éléments importants pour le jeu, puis en créant des liens. C'est pourquoi Iizuka a expliqué une fois que chaque jeu Sonic est dans une "bulle autonome" (self-contained bubble). Chaque jeu à pour objectif premier de faire son histoire et d'avancer ses concepts. Ensuite, parfois des rattachement peut se faire. Un exemple à cela est Frontiers : l'histoire des anciens est **d'abord** cette tragédie et cette esthétique d'être une forme de passeur d'âme, puis *après* on a eut une intégration des anciens qui ressemblent à Chaos et des Chao comme descendant des anciens.

Cela crée une situation où la notion de "canon" est moins restrictive que ce que nous avons souvent. Nous pourrions débattre de la version qui est la "bonne définition du canon", mais honnêtement il n'y a pas de bonne ou de mauvaise utilisation du canon car c'est un terme assez vague. C'est surtout une convention acceptée par les fans et parfois par les auteurs. Il signifie simplement "ce qui fait partie de l'histoire officielle". 

L'article de wikipedia en anglais est assez bien fait pour nuancer les différents usages du terme. C'est surtout parce que "canon" est souvent considéré comme quelque chose de binaire (soit quelque chose est canon, soit pas) alors que les choses sont souvent plus complexes. Starwars avait une façon intéressante de gérer cela avec ses différents niveaux de canonicité. Sonic a sa façon de faire, et ses éléments ont tendance à être canon les uns par rapport aux autres même si ça ne colle pas toujours parfaitement (maintenant, si c'est une bonne chose ou un gâchis, c'est à chacun de voir, j'en ai assez de discuter de ce genre de choses).

C'est aussi la raison pour laquelle nous avons des jeux comme Colors DS et Colors Wii qui ont des histoires différentes. Ce sont deux visions, deux points de vue de la même histoire.

## Contenus utilisés

Nous avons ici une vision en "une dimension" des séries "Classic Sonic" et "Modern Sonic". Le but est de voir leur séparation comme juste une séparation de "marques", mais toujours lié dans une seule continuitée.

Les différents éléments sont utilisés pour créer cette timeline :
- Jeux modernes comme classiques
- Les animations et webcomics officiel Sonic
- Les Sonic IDW
- Sonic Prime