---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Géographie
  key: Les îles et océans
  order: 2
---

# Les îles et océans

L'univers de Sonic comporte de nombreuses îles présentes dans l'océan. C'est là que vit principalement le Peuple Animal, alors que les humains vivent plutôt sur les continents. Leur organisation sociale est moins hiérarchisée et basée sur la domination que sur les continents. Elles ont souvent été attaqués par le Dr Eggman.

## South Island

L'un des lieux les plus importants de Sonic the Hedgehog, où Sonic et Eggman se sont beaucoup affrontés pour les légendaires Chaos Emeralds. Cette île est bien connue pour être l'endroit où se trouvent les Green Hill.

### Green Hill Zone

La zone la plus connue et la plus représenté de la planète de Sonic ! Elle a été très affectée par les différentes attaques d'Eggmans.

- Green Hill Zone
- Mecha Green Hill Zone
- Zone désertifiée et mécanisée créée par Eggman
  - Lost Valley
  - Guardian Rock
  - Arsenal Pyramid

### Autre zone naturelle

- Jungle Zone
  - Mystic Jungle Zone (zone ruinée transformée en casino)
    - Luminous Forest
    - Casino Forest Zone
    - Aqua Road
- Leaf Forest Zone
  - Floral Forest Village (IDW)
    - Vanilla's Home

### Villes

- Sunset City (nom par IDWSonic)
  - Ghost Town
  - Park Avenue
  - Sunset Heights
  - Red Gate Bridge
- Les ruines d'Emeraldville (IDW)
  - Le QG de la Restoration (IDW)

### All zones

- Tout les niveaux de Sonic 1 et Sonic 1 GG
- Tout les niveaux de Sonic 2 (GG/MS)
- Tout les niveaux de Sonic Chaos
- Tout les niveaux de Sonic Triple Trouble
- Tout les niveaux de Sonic Blast
- Le Grand Labyrinthe (sous l'île)
- Quelques niveaux de Forces
  - Mystic Jungle Zone
  - Metropolis Zone (Forces)
  - Eggman Empire Fortress
- La plupars des niveaux de Sonic Advance
- La plupars des niveaux de Sonic Advance 2

## Autres îles

- Westside Island
- Mirage Island ([nom fanon](https://sonicretro.org/2016/08/31/sonic-hedgehog-4-island-finally-name/))
  - Never Lake
  - Tout les niveaux de S4E1
  - Tout les niveaux de S4E2
- Flicky Island
  - Tout les niveaux de Sonic 3D Blast
- Newtrogic High Zone (A.K.A. The Island of Miracle)
  - Tout les niveaux de Knuckles Chaotix
- Eggmanland (Unleashed)
- Onyx Island (Angel Island Alternatif)
- Starfall Islands
  - Khronos Island
  - Ares Island
  - Chaos Island
  - Rhea Island
  - Ouranos Island
- Scrapnik Island (Ruines du Death Egg I)
- Northstar Islands

## Autres lieux

- Egg Carrier 1