---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Géographie
  key: Autres dimensions
  order: 4
---

# Autres dimensions

Le monde de Sonic est déjà un endroit avec beaucoup de lieux étranges et bizarres, mais notre hérisson bleu préféré a également visité beaucoup d'autres endroits en dehors de son propre monde ! Voici quelques autres dimensions que l'on peut voir dans la série.

## La Special Zone

La Special Zone est une dimension de poche étrange et compliquée où les Chaos Emerald peut se rendre de temps en temps, nécessitant que les personnages y accèdent pour les récupérer. Ce monde est en perpétuelle évolution et ne semble pas avoir de caractéristiques constantes, à l'exception de visuels trippants.

Elle semble cependant aimer faire courir les gens sur des demi-tuyaux.

## La Sol Dimension

- Southern Island
  - Windmill Village
  - Whale Point
  - Seagull Beach
- Plant Kingdom
- Machine Labyrinth
- Coral Cave
- Haunted Ship
- Sky Babylon
- Blizzard Peaks
- Pirates' Island
- Deep Core
- A lot of Hidden Islands
- Sol Empire Imperial Palace

## Autres lieux

- MaginaryWorld
- Chaotic Inferno
- The White World
- The Null Space
- The Cyberspace
- The EggNet
  - Mad Matrix
  - Game Land
- Storybooks dimensions
  - Arabian Nights World
  - Arthurian Legends World