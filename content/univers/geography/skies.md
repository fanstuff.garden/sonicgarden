---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Géographie
  key: Les cieux
  order: 3
---

# Les cieux

Il y a beaucoup de choses présentes dans le ciel de la Terre. La loi de la gravité ? Il semblerait qu'elle soit parfois davantage considérée comme une indication que comme une obligation.

## Îles et ruines flottantes

- Angel Island
  - Tout les niveaux dans S3&K
  - Red Mountain (SA)
  - Ice Cap (SA)
- Lost Hex
  - Tout les niveaux dans Sonic Lost World
- Les ruines des Sky Troops

## Flottes aériennes

- L'Egg Fleet
- L'Air Fleet (GUN)

## Autour de la Terre

- Little Planet
  - Tout les niveaux dans Sonic CD
- La station spatiale ARK
- Plein d'astéroïdes et planétoides
  - Meteor Herd
  - Mad Space
- La lune (a moitiée détruite)
- Potentiellement une seconde lune (visible dans quelques niveaux)
- Babylon Garden / Astral Babylon

## Stations spatiales

- Death Egg 1 (Crashé sur Terre, devenu Scrapnik Island)
- Death Egg 2 (Détruit dans Sonic the Fighter)
- Death Egg MkII (Toujours présent autour de Little Planet)
- Egg Utopia
- Death Egg (Sonic Battle, détruite)
- La station spatiale de l'intro d'Unleashed (détruite)
- Eggman Interstellar Amusement Park (détruit)

## Autres planétoïdes

- Black Comet (détruit)
- Sweet Mountains
- Aquarium Park
- Planet Wisp
- Asteroid Coaster