---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Géographie
  key: Les continents
  order: 0
---

# Les continents

Les continents sont la partie continentale de la Terre de Sonic. C'est là que vit la majorité de la population humaine, avec des cultures et des pratiques très différentes. Cette liste de continents est basée sur ce qui est montré dans *Sonic Unleashed* et essaie de respecter la position des éléments dans les différentes cultures. Là où nous pourrions avoir plusieurs positions différentes pour un élément, j'essaie de respecter la distance par rapport aux éléments principaux du jeu. Par exemple, le continent Shamar est plus proche de Chun-Nan et d'Empire City, tandis que le continent Mazuri est plus proche de celui d'Apotos/Spagonia.

Même si le Wikia Sonic a décidé d'utiliser les noms des continents terrestres en se basant sur l'adaptation de Gameloft Sonic Unleashed, j'ai décidé d'éviter ces noms, et de n'utiliser comme base que les informations des jeux officiels, et de réutiliser davantage la carte d'Unleashed.

Quelques autres notes :
- Les éléments inspirés de l'Amérique du Nord et de l'Amérique du Sud sont placés tous les deux sur le continent d'Empire City.
- Metal City se trouve sur le même continent que les Mystic Ruins, parce que les illustrations de Sonic Team suggèrent qu'elles ne sont pas si éloignées que ça.

## Les Fédérations Unies

Les Fédérations Unies sont un continent habité et fortement industrialisé qui est le centre de la plus grande puissance de l'univers de Sonic.

### Villes et parc

- Central City
  - Les collines qu'on dévale pendant City Escape
  - Mission Street
- Empire City
- Westopolis
  - Lethal Highway
- Casino Park
- Circus Park
- Station Square
  - Twinkle Park
  - Casinopolis
  - Speed Highway
  - Emerald Coast
- Grand Metropolis
- Metal City

### Système routier

- Route 101
- Route 208
- Radical Highway

### Bases de GUN

- Prison Island
  - Green Forest & White Jungle
  - Prison Lane
  - Metal Harbor
  - Security Hall
  - Weapons Bed
- Digital Circuit

### Canyon et ruines

- Canyons
  - Sky Rail 
  - Wild Canyon
  - Rail Canyon
  - Glyphic Canyon
- Mystic Ruins
  - Holy Summit
  - Echidna's Ruins
  - Final Egg
  - Windy Valley
  - Sand Hill
  - Ancient Angel Island site
- Death Jungle
- Pumpkin Hills
- Aquatic Mine (somehow connected to a gigantic sewer system)

### Autres lieux

- Chao World (a place to raise Chao)
  - Chao Kindergarten
- Seaside Hill
  - Ocean Palace
- Cryptic Castle

## Continent de Shamar

- Shamar
- Pyramid Cave / Hidden Base
  - Death Chamber
- Monopole (shown to be near Babylon Garden)
  - Megalo Station
  - MetorTech
  - Botanical Kingdom
  - Aquatic Capital
- Sand Ruins
  - Babylon Garden original location

## Apotos / Spagonia

### Nord et Centre

- Spagonia
- Hang Castle

### Sud

- Blue Coast Zone
  - Apotos
- Soleanna
  - Kingdom Valley
- White Acropolis

## Continent de Chun-Nan

- Chun-Nan
- Gigan Rocks

## Continent de Mazuri

- Dusty Desert
- Tropical Jungle
- Mazuri

## Île d'Adabat

- Adabat
- Frog Forest

## Continent d'Holoska  

- Holoska
- GlacierLand
  - Frozen Junkyard
  - Hidden Volcano
  - Ice Mountain