---
layout: layouts/base-toppages.njk
eleventyNavigation:
  parent: L'univers de Sonic
  key: Géographie
  order: 3
---

# La géographie Sonic

L'univers de Sonic est étrange, et il a subi de nombreux changements dans la façon dont il est géré. Pendant longtemps, les jeux de l'ère classique ont été séparés dans une autre ligne temporelle, et le monde a été divisé en deux univers : le monde des humains et le monde des animaux.

Le problème est maintenant résolu, et il n'y a qu'un seul monde, ce qui rend le fan plus heureux. C'est un remake d'un vieux projet que j'avais de "mettre tous les niveaux de Sonic dans une géographie". Il est basé sur la vision d'un seul monde du Sonic's World (d'autant plus que maintenant c'est ce qui est canon). Vous pouvez l'appeler "Terre" ou "Mobius" selon votre point de vue, ce n'est pas très important.

## Les lieux

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Règles choisies

J'essaie de suivre quelques règles sur ce projet :
- Je ne mets pas tous les niveaux de tous les jeux, mais seulement quelques uns pour montrer les "grands lieux importants"
- Je décide au cas par cas si une aventure se déroule sur une île ou sur les continents
- Je ne mets pas toutes les bases d'Eggman dans la liste, pour ne pas les encombrer (elles sont souvent détruites)
- Je n'ai pas mis les niveaux de Sonic Advance 3, car cela se passe dans une sorte de faille dimensionnelle/espace-temps.
- Je n'ai pas essayé de résoudre le problème des "deux Central City" qui peut se poser dans IDWSonic. Une solution serait de :
  - Voir l'utilisation de "Central City" dans Shadow the Hedgehog comme descriptive plus que comme donnant le nom de l'endroit (plutôt comme "c'est la ville et c'est central").
  - Séparer la ville montrée dans SA2 et Shadow 
  - Nommer la Central City continentale "Golden Bay", la zone inspirée de San Francisco dans Forces Speed Battle.
  - Cela déplacerait l'intrigue de Battle à South Island, et expliquerait la présence de GHZ.
- J'ai indiqué Sonic Advance et Sonic Advance 2 comme se déroulant principalement dans l'île du sud aussi, pour simplifier les choses et rendre les choses plus faciles avec IDWSonic.