---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Chronologie
  key: L'ère classique
  order: 1
---

# L'ère classique

Quelques infos et canevats :
- J'ai décidé de donner la priorité à Sonic Mania Adventure plutôt qu'au Mode Encore de Sonic Mania.
- Je considère le style moderne/classique comme quelque chose de plus stylistique qu'in-canon, et pour garder un arc Metal Sonic propre, j'ai mis Sonic 4 dans l'ère classique.

## Premières aventures

- Sonic the Hedgehog (1991) - Le premier jeu. Les sources sont contradictoires généralement sur si c'est ou non la première rencontre entre Sonic et Eggman.
- Sonic the Hedgehog CD (1993) - Sonic sauve Amy et Little Planet
- Sonic the Hedgehog 2 (1992) - Début de la première saga du Death Egg. Rencontre entre Sonic & Tails.
- Sonic the Hedgehog 3 & Knuckles (1994) - Fin de la seconde saga du Death Egg.
- Sonic the Hedgehog 2 GG (1992) - Eggman kidnappe Tails. D'après les manuels, Sonic et Tails se connaissent, donc cela doit se passer après S2 (donc après S3&K aussi).

## Le retour de Metal

- Sonic the Hedgehog 4 (2010) - Metal Sonic est de retour : nécessaire pour toute histoire avec Metal Sonic par la suite.
- Sonic Chaos (1993)
- Sonic Triple Trouble (1994)
- Sonic Spinball (1993) - S'est produit d'une certaine manière, et peut être considéré comme "canon" puisque la Veg-O-Fortress est mentionnée dans Sonic Origins.
- Sonic Labyrinth (1995) - Eggman tente de ralentir Sonic
- Tails' Skypatrol (1995) - Tails affronte une sorcière.
- Tails Adventure (1995) - Tails affronte des oiseaux (J'utilise l'interprétation américaine du jeu, car elle rend l'histoire plus convaincante.)
- Knuckles' Chaotix (1995) - Knuckles veut aussi sa part d'hérisson métallique.
- Sonic Blast (1996) 
- Sonic 3D Blast (1996) - Sonic & Co. vont sur Fliky Island
- Sonic the Fighters (1996) - Ne pensons pas trop aux 8 émeraudes, mais disons nous que c'était plutôt 7 émeraudes + la Master Emerald.
- Sonic R (1997)

## Nouvelles aventures

J'ai placé ici les histoires classiques les plus récentes, car il semble que les personnages se connaissent dans les comics.

- Sonic Generation (2011)
- Sonic Mania (2017)
- Sonic Forces (2017) - Histoire Classique
- Sonic Mania Adventure (Animation, 2017) - De retour à son époque, Sonic classique combat Eggman qui a fait quelques magouilles..
- Seasons of Chaos (IDW, 2021)
- Dr. Eggman's Birthday (IDW, 2021)
- Sonic Learns to Drive (IDW, 2021)
- Amy's New Hobby (IDW, 2021)
- Tails' 30th Anniversary Special : Flying Off the Rails (IDW, 2022)
- Amy's 30th Anniversary Special (IDW, 2023)
- Fang the Hunter minisérie (IDW, 2024)
- Sonic Superstars: Fang's Big Break (webcomic, 2023)
- Sonic Superstars : Trio of Trouble (Animation, 2023)
- Sonic Superstars (2023)