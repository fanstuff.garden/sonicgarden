---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Chronologie
  key: L'ère moderne
  order: 2
---

# L'ère moderne

## Eggman casse tout

- Sonic Unleashed (2008)
- Sonic Colours Prologue (webcomics, 2021)
- Sonic Colours (2010) — Introduction des Wisps dans la saga.
  - Sonic Colours : Rise of the Wisps (video, 2023) — Se produit pendant Sonic Colors Ultimate
- Sonic Generations (2011)
- Sonic Lost World (2013)

## La guerre d'Eggman

- Sonic Forces: Rise of Infinite, Stress Test et Looming Shadow (webcomic - 2017)
- Sonic Forces: Episode Shadow (DLC - 2017)
- *Sonic Forces : Sonic est vaincu par Infinite*
- Sonic Forces: Moment of Truth (webcomic - 2017)
- *Flashbacks de Tangle et Whisper*
- Sonic Forces (2017)

### Conséquences de la guerre d'Eggman

- Fallout (IDW01-IDW04, 2018)
- The Fate of Dr. Eggman (IDW05-IDW06, 2018)
- Meet the New Boss (IDW07, 2018)
- Silent Support (IDW08, 2018)
- The Battle for Angel Island (IDW09-IDW12, 2018-2019)
- Sonic the Hedgehog : Tangle & Whisper (comics, 2019)

*Cette période contient aussi les histoires du IDW Annual 2019*

## Le Metal Virus

- Infection (IDW03-IDW16, 2019)
- Crisis Cities (IDW17-IDW20, 2019)
- The Last Minute (IDW21-IDW24, 2019)
- All or Nothing (IDW25-IDW29, 2020)
- Out of the Blue (IDW30-IDW32, 2020)

*Cette période contient aussi les histoires du IDW Annual 2020*


## Zeti et Eggman véner

- Sonic the Hedgehog : Bad Guys (IDW, 2020)
- Chao Races and Badnik Bases (IDW33-IDW36, 2020-2021)
- Test Run! (IDW37-IDW40, 2020-2021)
- Team Sonic Racing (2019)
- Zeti Hunt! (IDW41-IDW44, 2021)

*Cette période contient aussi les histoires du IDW Annual 2022*


## Imposteurs et capitale

- Sonic the Hedgehog : Imposter Syndrome (IDW, 2021)
- Trial by Fire (IDW45-IDW47, 2021)
- The Imposter Attack (IDW48-IDW51, 2022)
- Overpowered (IDW52-IDW56)
- Urban Warfare (IDW57-IDW61)
- Misadventures (IDW62-IDW66)
- Sonic the Hedgehog : Scrapnik Island (IDW, 2022) - Se passe avant le IDW67, peut être vu comme simultané au 65~66 ou Sonic & Tails n'apparaissent pas

## Sonic Frontiers

- Sonic Frontiers Prologue : Convergence (webcomic, 2022)
- Sonic Frontiers Prologue : Divergence (animation, 2022)
- Sonic Frontiers (2022)

## Histoires actuelles

- The Murder of Sonic the Hegehog (2023) - Après Frontiers
- Sonic Prime (TV, 2023) - Placement difficile, car la série est censée être "canonique" mais présente quelques bizarreries d'écriture.
- Sonic the Hedgehog: The 900th Adventure (IDW, 2023) - Se passe après le Metal Virus, l'emplacement exact est à déterminer.
- Sonic the Hedgehog: Endless Summer (IDW, 2023)
- Sonic the Hedgehog: Halloween Special (IDW, 2023)
- Sonic the Hedgehog: Winter Jam (IDW, 2023)
- Sonic Dream Team (2023)