---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Chronologie
  key: L'ère aventure
  order: 2
---

# L'ère aventure

## Sonic Adventure

- Sonic Adventure (1998)
- Sonic Shuffle (2000)
- Sonic Advance (2001)

## L'héritage de Gerald

- Sonic Adventure 2 (2001) — L'ARK est désormais présent dans le canon, pareil pour Gerald et Shadow.
- Sonic Advance 2 (2002)
- Sonic Heroes (2003) – Le retour de Shadow
- Shadow the Hedgehog (2005) – Découverte des origines de Shadow
- Sonic Battle (2004) – Shadow s'interroge sur le fait d'être une arme. Découverte et mort d'Emerl.
- Sonic Advance 3 (2004) – Gemerl, l'héritier d'Emerl est désormais présent dans le canon.

## La timeline perdue

La ligne temporelle où Sonic 2006 était présent. Cette timeline a été effacée.

- Sonic the Hedgehog (2006)
- 200 ans plus tard : Silver combat Iblis dans une ère brisée

## La timeline restorée

La ligne temporelle créée par Sonic après avoir détruit la Flamme du Désastre.

- Sonic Rush (2005) — Blaze est désormais présente dans la timeline principale.
- Sonic Rush Adventure (2007)
- Sonic Rivals (2006) – Silver est désormais présent dans la timeline principale. Plus logique s'ils se passent après les Rush.
- Sonic Rivals 2 (2007)

## Histoires diverses

### Storybook

(Ces jeux peuvent se trouver partout dans la chronologie post-Shadow)

- Sonic & the Secret Rings (2007)
- Sonic & the Black Knight (2009)

### Sonic Riders

(Ces jeux peuvent se trouver partout dans la chronologie)

- Sonic Riders (2006)
- Sonic Riders - Zero Gravity (2008)
- Sonic Free Riders (2010)
