---
layout: layouts/base-toppages.njk
eleventyNavigation:
  key: Chronologie
  parent: L'univers de Sonic
  order: 2
---

# La chronologie de Sonic

Le but de ces pages est de créer une timeline des différents évenements et histoires se produisant dans l'univers de Sonic, et chercher comment elles sont liées. Le but de cette page est de présenter un peu une chronologie des jeux Sonic.

Cette chronologies comporte uniquement les jeux avec des scénario, et généralement pas les crossover style Mario & Sonic.

## Division et ères

La chronologie est divisée en trois ère, avec la division la plus commune :

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

L'ère classique comporte tout les jeux se produisant avant Sonic Adventure (dont Sonic 4), l'ère Adventure tout les jeux se produisant entre Sonic Adventure (inclu) et Unleashed (exclu) et l'ère Modern tout les jeux se produisant à partie d'Unleashed.

Il y aura sans doute une division à un moment des jeux se produisant à partir de Frontiers, j'ai cependant besoin de savoir exactement ou je vais placer les éléments.

## Sonic Chronicles

Sonic Chronicles n'est pas inclu dans cette chronologie, parce qu'il est l'un des rares jeux que la Sonic Team a déclaré non-canon. Cependant, il n'y a pas encore de (trop) grosse contradiction entre Sonic Chronicles et les jeux précédant.

Il reste cependant deux différences qui ont de l'impact selon moi :

- La première est que les technologies d'Angel Island étaient plus ou moins insinuée d'être liées aux Nocturnus dans Sonic Chronicles, là où elles sont maintenant insinuées d'être liées aux Anciens.
- Chronicles traite les Gizoids comme une série de robots là où dans Battle Gizoid était un robot unique

Si vous voulez voir Sonic Chronicles comme étant canon, vous pouvez positionner le jeu continuellement deux ans dans le futur du jeu Sonic le plus récent.