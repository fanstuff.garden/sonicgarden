---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Théories
  key: Les échidnés
  order: 0
---

# Théories à propos des Echidnas

Les Echidnas sont l'un des éléments les plus importants du lore de Sonic : les mystérieux ancêtres de Knuckles, habitants d'Angel Island, pleins de secrets et de mystères ! Ils sont apparus par la suite dans Sonic Adventure, et les Comics en ont usé et abusé (au point que la notion d'échidné est maintenant moins importante à cause de la Penderpocalypse).

Dans Sonic Chronicles, le clan Nocturnus était également un clan d'échidnés, rival du clan Knuckles.

## L'origine de Shadow

Cette hypothèse est assez répandue mais n'est pas encore confirmée. L'apparition de Shadow viendrait de la fresque que l'on peut voir dans Hidden Palace lors du duel entre Sonic et Knuckles, qui prophétise le combat final entre Sonic et Eggman dans l'espace. L'idée de cette théorie est que Shadow aurait été créé comme le héros prophétisé par Gerald Robotnik, qui aurait découvert le temple et ses secrets. Cette théorie explique un peu pourquoi Shadow ressemble non seulement à Sonic, mais aussi à la *super forme* de Sonic.

Cette théorie ne vient pas de rien et comporte déjà plusieurs éléments qui montrent que Gerald connaissait la culture d'Angel Island :
- Une réplique de l'autel de la Master Emerald est présente dans l'ARK, ce qui montre que Gerald connaissait l'autel, et donc l'île d'Angel.
- Dans Sonic Chronicles, les Gizoïd sont notés comme étant une création de la 4ème Grande Civilisation, dont Gerald a appris beaucoup de choses.

Cette théorie est également intéressante d'un point de vue thématique, car elle renforce le lien entre Shadow et Sonic en faisant encore plus de Shadow "l'ombre de Sonic". Comme la fresque prophétise une grande bataille entre Sonic et Eggman, elle fait de Shadow une copie de ce qui a été prophétisé pour Sonic.

## La technologie d'Angel Island

Le clan Knuckles que l'on voit dans Sonic Adventure a un niveau de technologie et de connaissance précomlombien. Ils n'ont tout simplement pas pu fabriquer la technologie que nous voyons dans Angel Island, qui comprend des choses comme des palais flottants, des téléporteurs, etc. Ceci est amplifié par Sonic Generation, qui ajoute un aspect technologique à certains éléments du Sky Sanctuary, montrant que ce n'est pas seulement la "vieille magie d'Echidna" qui fait fonctionner l'endroit.

Deux théories communes ont été élaborées à ce sujet :
- Les survivants ont créé les technologies. Je n'y crois pas vraiment, car les survivants étaient peu nombreux, et je ne vois pas de civilisation à grande échelle exister après l'attaque de Perfect Chaos.
- Les ruines d'Angel Island sont plus anciennes que le clan Knuckles.

La seconde a déjà été évoquée dans le reboot d'Archie Sonic, où les technologies d'Angel Island sont décrites comme "similaires à celles des Trolls" (qui sont la deuxième grande civilisation d'Archie). Si Archie Sonic n'est pas canonique au jeu, l'idée était déjà présente à l'époque.

**NOTE** : Cette théorie a été presque complètement confirmée par Sonic Frontiers, qui semble indiquer que les Anciens ont créé les technologies trouvées sur Angel Island, ou au moins créé des choses dont les technologies d'Angel Island sont dérivées.

## Le nom de Knuckles vient du clan

Le "clan Knuckles" étant le nom officiel de l'ancien clan des échidnés que l'on voit dans Sonic Adventure, il serait logique de penser que Knuckles a été nommé ainsi pour honorer son clan, surtout s'il est le dernier de son espèce.