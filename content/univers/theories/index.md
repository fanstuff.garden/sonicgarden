---
layout: layouts/base-toppages.njk
eleventyNavigation:
  key: Théories
  parent: L'univers de Sonic
  order: 6
---

# Théories

Le monde de Sonic est plein de mystères... et aussi d'incohérences. Le but de cette catégorie est de parler des différentes théories que l'on peut avoir sur l'univers Sonic, pour résoudre certains mystères, ou parce que c'est amusant.

Les théories sont classées par sujets.

## Sujets présents

Des théories sur les sujets suivants sont présentes sur ce site.

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventySubNavigation.order") -%}
    {%- if post.data.eleventySubNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Tout ça est vrai ?

Tout ce qui est décrit ici n'est pas forcément confirmé dans les jeux, voir sera peut-être infirmé dans un jeu futur (voir dans un jeu déjà produit). Cependant, l'idée est d'essayer d'avoir des théories qui peuvent marcher avec les informations que l'on a dans le jeu.

Le but n'est pas de décrire le "canon officiel", mais plus de s'amuser à essayer de deviner comment pourrait être l'univers. Le théoriecrafting doit être avant tout vu comme du fun, et non pas une science sérieuse ou whatever.