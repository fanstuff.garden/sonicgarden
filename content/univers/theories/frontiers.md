---
layout: layouts/base-subpages.njk
eleventySubNavigation:
  parent: Théories
  key: Sonic Frontiers
  order: 3
---

# Théories sur Sonic Frontiers

Quelques théories que j'ai sur Sonic Frontiers, que j'ai écrites ici pour me les enlever de la tête. Il s'agit principalement de théories sur l'histoire des anciens montrée dans Frontiers.

## Chao et Anciens

Dans Sonic Frontiers, il est révélé que les Anciens ressemblaient à Chaos, et que leurs derniers membres avaient muté en une "forme méconnaissable". On suppose (et c'est confirmé par le scénariste Ian Flynn) que les Chao sont la forme mutante. La théorie que j'ai sur leur relation exacte est que les Chao sont une forme de [néoténie](https://fr.wikipedia.org/wiki/N%C3%A9ot%C3%A9nie), où ils sont des anciens "coincés" dans leur forme larvaire (mais qui peuvent toujours se reproduire).

Cela signifierait que la forme (Neutral) *Chaos Chao* est une certaine résurgence de traits adultes, et qu'une partie des mutations du Chaos sont une résurgence de ces traits adultes.

Quelques questions que cela signifierait sont :
- Les chao Dark/Angel sont-ils une autre mutation sans rapport avec la néoténie ?
- Cela signifierait-il que nous pourrions obtenir des mutations qui créent un "Dark Chaos" et un "Angel Chaos" ? (ce serait cool)

## La première "grande civilisation" ?

Dans Sonic Battle, nous apprenons que les Gizoïdes ont été créés par un groupe appelé la "Quatrième Grande Civilisation". Cela tend à nous faire penser qu'il y a eu trois autres *grandes civilisations* auparavant, et ma théorie est que les Anciens sont la première.

Ma théorie est que les "Grandes Civilisations" racontées par Gerald sont celles qui ont un lien avec l'Émeraude du Chaos, et les Anciens serait la première. Le créateur du temple de Gaïa pourrait être la deuxième. D'après un bumblekast de Flynn, nous savons que les Anciens n'ont pas construit le temple de Gaïa, même s'il y a quelques similitudes au niveau de l'architecture (ce qui est logique : ils ont vécu il y a *des* dizaines de milliers d'années (soit > 20 000 ans, ce qui fait qu'ils sont autant distant des temples de gaïa que les temples de gaïa sont distant d'aujourd'hui)).

Il est possible que les créateurs des temples de Gaïa se soit inspiré de la conception des ruines des Anciens et ait créé sa propre façon de récolter le pouvoir des émeraudes du Chaos. Une autre théorie possible est que la civilisation de Gaïa serait la *première grande civilisation* et que les Anciens soient un prédécesseur.

## La Master Emerald

J'ai l'impression que même si la Master Emerald est montré comme étant sur terre lors de l'arrivée des Ancients, je ne pense pas non plus qu'il vienne de la Terre. On insinue qu'ils l'ont trouvée sur le sol, ce qui semble aller dans le sens d'une espèce qui n'était pas encore capable de la manipuler (voir mes théories sur le fait que les Anciens sont la première grande civilisation ou leur prédécesseur).

Mais il est possible que la Master Emerald soit arrivée sur Terre d'une origine inconnue, ce qui expliquerait pourquoi ils l'ont trouvée couchée sur le sol, sans qu'elle ait beaucoup utilisé son pouvoir. Une autre possibilité serait que les Chaos Emerald et la Master Emerald proviennent des mêmes origines et qu'elles aient été séparées à un moment donné.