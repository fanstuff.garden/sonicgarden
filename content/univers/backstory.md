---
layout: layouts/base.njk
eleventyNavigation:
  parent: L'univers de Sonic
  key: Backstory
  order: 0
---

# Backstory de Sonic

Si le personnage de Sonic n'a pas de backstory en lui-même (comparée à celles présentes dans d'autres média), l'univers de Sonic à eut beaucoup d'histoire avant le début des jeux (en particulier du à la propention de la série à mettre des civilisations anciennes partout).

Le but de cette page est de parler un peu des temps anciens de l'univers de Sonic.

## Temps anciens

- Il y a 6 milliards d'années : La terre se forme, et deux êtres se développent en son centre : Dark Gaia et Light Gaïa. Tous les 10 000 ans, ils se disputent pour la destruction ou la préservation de la planète.
- Il y a plus de 10 000 ans : Les Anciens arrivent sur Terre et s'installent sur les Starfall Island ainsi qu'Angel Island. Il pourrait s'agir ou non de la civilisation appelée par les érudits la "Première Grande Civilisation". Ils mutent à Chao en l'espace d'une génération en partie dû aux radiations du Maître Émeraude. Les plus anciennes ruines d'Angel Island date de cette époque.
- Il y a 10 000 ans : Dernier combat (pré Unleashed) entre Light Gaïa et Dark Gaïa. Cette fois, Light Gaïa est aidée par la civilisation présente à l'époque, qui a créé le Temple de Gaïa. Cette civilisation est une autre civilisation que les anciens
- Il y a plus de 4000 ans : Arrivée de tribues échidnées sur ce qui deviendra Angel Island, et occupent le territoire.
- Il y a 4000 ans : Extinction du clan Pachacamac suite à l'événement de Perfect Chaos. Angel Island se sépare de la Terre et flotte depuis dans le ciel.
  - Les échidnés restants commencent à garder la Master Emerald sur Angel Island.
- Il y a peut-être 3000 ans : Arrivée des Babyloniens sur Terre
  - Construction de Babylon Garden autour de l'Astral Babylon
  - Les babyloniens commencent à se livrer au vol et au banditisme, créant les Babylon Rogues.
  - Stolen, le 13ème chef du Babylon Rogue crée le Magic Carpet, prototype des Extreme Gear
    - Début de l'art de la fabrication des Extreme Gear
  - Le tapis volant est protégé par le Babylon Gardian
  - Une Arche du Cosmos est enterrée dans les Gigan Rock par les Babyloniens.
  - Babylon Garden est enfoui dans le sable du Dark Desert, selon les légendes par des "dieux" en punition des actions des Rogues.
- Il y a 2000 ans : La Black Comet arrive sur terre
  - Création de temples flottants Black Arms sur Terre.

**Note:** Comme nous n'avons pas de date pour l'époque babylonienne dans Sonic, j'ai utilisé la date terrestre entre 1800 av. J.-C. et 539 av. J.-C., donc entre 3800 et 2539 ans.

## Histoire récente

- Il y a 50 ans : L'incident de l'ARK
  - Gerald Robotnik explore des ruines échidnées et découvre la fresque représentant Super Sonic.
  - Gerald Robotnik découvre Gizoïd (qui deviendra Emerl).
  - La Black Commet passe près de la Terre et Black Doom contacte Gerald
  - Création de Shadow
  - L'incident de l'ARK se produit : mort de Maria, emprisonnement et condamnation à mort de Gerald.
  - Avant de mourir, Gerald reprogramme Shadow et l'ARK.
- Il y a 10 ans :
  - L'incident de Soleanna a lieu, le Duc de Soleanna meurt de ses blessures.
  - Dans une ligne temporelle disparue, Iblis est fusionné avec Elise (ÉVENEMENT EFFACÉ)